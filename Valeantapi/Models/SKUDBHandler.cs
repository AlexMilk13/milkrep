﻿using System;
using System.Collections.Generic;
using System.Data;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class SKUDBHandler
    {
        public DbClass dbc = new DbClass();
        public int count = 10;
        public int page;
        public string sort = "ID_Sku";
        public string order = "DESC";
        public string SkuName = "";
        public string Code = "";
        public string Diopter = "";
        public string Description = "";
        public string IsActive = "";
        public string SKU_ID = "";
        public string Store_Name = "";
        public string StoreCount = "";
        public string IsBausch = "";


        public List<SKUModel> GetItemList()
        {

            string SkuNamefilter = "";
            string Codefilter = "";
            string Diopterfilter = "";
            string Descriptionfilter = "";
            string IsActivefilter = "";
            string SKU_IDfilter = "";
            string Store_Namefilter = "";
            string StoreCountfilter = "";
            string IsBauschfilter = "";
            List<SKUModel> iList = new List<SKUModel>();

            if (StoreCount.Length > 0)
            {
                StoreCountfilter = " AND StoreCount IN (";

                int y = 0;
                foreach (string tt in StoreCount.Split('|'))
                {
                    if (y > 0)
                        StoreCountfilter += ", ";
                    StoreCountfilter += tt;
                    y++;
                }
                StoreCountfilter += ") ";
            }


            if (SkuName.Length > 0)
            {
                SkuNamefilter = " AND SkuName IN (";

                int y = 0;
                foreach (string tt in SkuName.Split('|'))
                {
                    if (y > 0)
                        SkuNamefilter += ", ";
                    SkuNamefilter += "'" + tt + "'";
                    y++;
                }
                SkuNamefilter += ") ";
            }

            if (Description.Length > 0)
            {
                Descriptionfilter = " AND Description IN (";

                int y = 0;
                foreach (string tt in Description.Split('|'))
                {
                    if (y > 0)
                        Descriptionfilter += ", ";
                    Descriptionfilter += "'" + tt + "'";
                    y++;
                }
                Descriptionfilter += ") ";
            }



            if (Code.Length > 0)
            {
                Codefilter = " AND Code IN (";

                int y = 0;
                foreach (string tt in Code.Split('|'))
                {
                    if (y > 0)
                        Codefilter += ", ";
                    Codefilter += "'" + tt + "'";
                    y++;
                }
                Codefilter += ") ";
            }

            if (Diopter.Length > 0)
            {
                Diopterfilter = " AND Diopter IN (";

                int y = 0;
                foreach (string tt in Diopter.Split('|'))
                {
                    if (y > 0)
                        Diopterfilter += ", ";
                    Diopterfilter += tt.ToString().Replace(",",".") ;
                    y++;
                }
                Diopterfilter += ") ";
            }
            /////////////////////////////
           

            if (SKU_ID.Length > 0)
            {
                SKU_IDfilter = " AND SKU_ID IN (";

                int y = 0;
                foreach (string tt in SKU_ID.Split('|'))
                {
                    if (y > 0)
                        SKU_IDfilter += ", ";
                    SKU_IDfilter += "'" + tt + "'";
                    y++;
                }
                SKU_IDfilter += ") ";
            }

            if (Store_Name.Length > 0)
            {
                Store_Namefilter = " AND Store_Name IN (";

                int y = 0;
                foreach (string tt in Store_Name.Split('|'))
                {
                    if (y > 0)
                        Store_Namefilter += ", ";
                    Store_Namefilter += "'" + tt + "'";
                    y++;
                }
                Store_Namefilter += ") ";
            }

            if (IsActive.Length > 0)
            {
                IsActivefilter = @" AND [IsActive] = " + IsActive;
            }
            if (IsBausch.Length > 0)
            {
                IsBauschfilter = @" AND [IsBausch] = " + IsBausch;
            }
            if (sort.ToLower() == "docname") sort = "[Name]";
            if (sort.ToLower() == "docsurname") sort = "[Surname]";
            if (sort.ToLower() == "docpatronymic") sort = "[Patronymic]";
            if (sort.ToLower() == "opticaddress") sort = "[Address]";
            string sorter = " ORDER BY ID_Sku ";
            if (sort != null && sort.Trim().Length > 0)
            {
                sorter = " ORDER BY " + sort + " ";
                if (order != null && order.Trim().Length > 0)
                    sorter += order;
            }
            string offset = "";

            offset = "  OFFSET " + ((page - 1) * count).ToString() + " ROWS FETCH NEXT " + count.ToString() + " ROWS ONLY";
            string query = @"SELECT 1  FROM [dbo].[SKU_VIEW]
   WHERE 1=1 " + SkuNamefilter + Codefilter + Diopterfilter + Descriptionfilter + IsActivefilter + SKU_IDfilter + Store_Namefilter + StoreCountfilter + Descriptionfilter + IsBauschfilter;
            DataTable dt = dbc.Make_Query(query);
            int yy = dt.Rows.Count;
            query = @"SELECT [ID_Sku]      ,[Code]      ,[SkuName]      ,[Diopter]      ,[Description]      ,[IsActive]      ,[SKU_ID]
     ,[id_StoringWay]      ,[Store_Name], [StoreCount], Picture, IsBausch FROM [dbo].[SKU_VIEW]
   WHERE 1=1 " + SkuNamefilter + Codefilter + Diopterfilter + Descriptionfilter + IsActivefilter + SKU_IDfilter + Store_Namefilter + StoreCountfilter + Descriptionfilter + IsBauschfilter + sorter + offset;
            dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new SKUModel
                    {
                        ID_Sku = Convert.ToInt32(dr["ID_Sku"]),
                        id_StoringWay = dr["id_StoringWay"] == Convert.DBNull ? -1: Convert.ToInt32(dr["id_StoringWay"]),
                        StoreCount = dr["StoreCount"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["StoreCount"]),
                        Code = Convert.ToString(dr["Code"]),
                        SkuName = Convert.ToString(dr["SkuName"]),
                        Description = Convert.ToString(dr["Description"]),
                        Store_Name = Convert.ToString(dr["Store_Name"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        Diopter = Convert.ToSingle(dr["Diopter"]),
                        SKU_ID = Convert.ToString(dr["SKU_ID"]),
                        IsBausch = Convert.ToBoolean(dr["IsBausch"]),
                        Picture = dr["Picture"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["Picture"]),
                        pagecount = yy
                    });


                    y++;
                }
            }
            return iList;
        }




        //////////////////--------------********************************************

        public List<SKUModel> GetItemListExcel()
        {

            string SkuNamefilter = "";
            string Codefilter = "";
            string Diopterfilter = "";
            string Descriptionfilter = "";
            string IsActivefilter = "";
            string SKU_IDfilter = "";
            string Store_Namefilter = "";
            string StoreCountfilter = "";
            string IsBauschfilter = "";
            List<SKUModel> iList = new List<SKUModel>();

            if (StoreCount.Length > 0)
            {
                StoreCountfilter = " AND StoreCount IN (";

                int y = 0;
                foreach (string tt in StoreCount.Split('|'))
                {
                    if (y > 0)
                        StoreCountfilter += ", ";
                    StoreCountfilter += tt;
                    y++;
                }
                StoreCountfilter += ") ";
            }


            if (SkuName.Length > 0)
            {
                SkuNamefilter = " AND SkuName IN (";

                int y = 0;
                foreach (string tt in SkuName.Split('|'))
                {
                    if (y > 0)
                        SkuNamefilter += ", ";
                    SkuNamefilter += "'" + tt + "'";
                    y++;
                }
                SkuNamefilter += ") ";
            }



            if (Description.Length > 0)
            {
                Descriptionfilter = " AND Description IN (";

                int y = 0;
                foreach (string tt in Description.Split('|'))
                {
                    if (y > 0)
                        Descriptionfilter += ", ";
                    Descriptionfilter += "'" + tt + "'";
                    y++;
                }
                Descriptionfilter += ") ";
            }


            if (Code.Length > 0)
            {
                Codefilter = " AND Code IN (";

                int y = 0;
                foreach (string tt in Code.Split('|'))
                {
                    if (y > 0)
                        Codefilter += ", ";
                    Codefilter += "'" + tt + "'";
                    y++;
                }
                Codefilter += ") ";
            }

            if (Diopter.Length > 0)
            {
                Diopterfilter = " AND Diopter IN (";

                int y = 0;
                foreach (string tt in Diopter.Split('|'))
                {
                    if (y > 0)
                        Diopterfilter += ", ";
                    Diopterfilter += tt.ToString().Replace(",", ".");
                    y++;
                }
                Diopterfilter += ") ";
            }
            /////////////////////////////


            if (SKU_ID.Length > 0)
            {
                SKU_IDfilter = " AND SKU_ID IN (";

                int y = 0;
                foreach (string tt in SKU_ID.Split('|'))
                {
                    if (y > 0)
                        SKU_IDfilter += ", ";
                    SKU_IDfilter += "'" + tt + "'";
                    y++;
                }
                SKU_IDfilter += ") ";
            }

            if (Store_Name.Length > 0)
            {
                Store_Namefilter = " AND Store_Name IN (";

                int y = 0;
                foreach (string tt in Store_Name.Split('|'))
                {
                    if (y > 0)
                        Store_Namefilter += ", ";
                    Store_Namefilter += "'" + tt + "'";
                    y++;
                }
                Store_Namefilter += ") ";
            }

            if (IsActive.Length > 0)
            {
                IsActivefilter = @" AND [IsActive] = " + IsActive;
            }

            if (IsBausch.Length > 0)
            {
                IsBauschfilter = @" AND [IsBausch] = " + IsBausch;
            }
            if (sort.ToLower() == "docname") sort = "[Name]";
            if (sort.ToLower() == "docsurname") sort = "[Surname]";
            if (sort.ToLower() == "docpatronymic") sort = "[Patronymic]";
            if (sort.ToLower() == "opticaddress") sort = "[Address]";
            string sorter = " ORDER BY ID_Sku ";
            if (sort != null && sort.Trim().Length > 0)
            {
                sorter = " ORDER BY " + sort + " ";
                if (order != null && order.Trim().Length > 0)
                    sorter += order;
            }


            string query = @"SELECT [ID_Sku]      ,[Code]      ,[SkuName]      ,[Diopter]      ,[Description]      ,[IsActive]      ,[SKU_ID]
     ,[id_StoringWay]      ,[Store_Name], [StoreCount], Picture,IsBausch  FROM [dbo].[SKU_VIEW]
   WHERE 1=1 " + SkuNamefilter + Codefilter + Diopterfilter + Descriptionfilter + IsActivefilter + SKU_IDfilter + Store_Namefilter + StoreCountfilter + Descriptionfilter + IsBauschfilter + sorter;
           DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new SKUModel
                    {
                        ID_Sku = Convert.ToInt32(dr["ID_Sku"]),
                        id_StoringWay = dr["id_StoringWay"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_StoringWay"]),
                        StoreCount = dr["StoreCount"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["StoreCount"]),
                        Code = Convert.ToString(dr["Code"]),
                        SkuName = Convert.ToString(dr["SkuName"]),
                        Description = Convert.ToString(dr["Description"]),
                        Store_Name = Convert.ToString(dr["Store_Name"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        Diopter = Convert.ToSingle(dr["Diopter"]),
                        SKU_ID = Convert.ToString(dr["SKU_ID"]),
                        IsBausch = Convert.ToBoolean(dr["IsBausch"]),
                        Picture = dr["Picture"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["Picture"]),
                        pagecount = 0
                    });


                    y++;
                }
            }
            return iList;
        }

        /////////////////////////////////////



        public List<string> GetDistinctList(string itemj)
        {
            List<string> iList = new List<string>();


            string IsBauschfilter = "";
            if (IsBausch.Length > 0)
            {
                IsBauschfilter = @" AND [IsBausch] = " + IsBausch;
            }
            string query = @"SELECT DISTINCT " + itemj + @" AS DD
FROM[dbo].[SKU_VIEW] WHERE 1=1
   " + IsBauschfilter;

            if (itemj == "Description")
                query = @" WITH AA AS
 (SELECT DISTINCT Description DD, sorter  FROM [dbo].[SKU_VIEW] sv WHERE 1=1
" + IsBauschfilter + @"
 )
 SELECT DD FROM AA
   ORDER BY sorter";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(
                   dr["DD"] == Convert.DBNull ? "" : Convert.ToString(dr["DD"]));
                }
            }
            return iList;
        }


        public List<string> GetDistinctDiopters(string itemj, string value)
        {
            List<string> iList = new List<string>();



            string query = @"SELECT DISTINCT [Diopter] DD
  FROM [dbo].[SKU_VIEW]
   WHERE " + itemj + " = '" + value + "' ORDER BY Diopter";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(
                   dr["DD"] == Convert.DBNull ? "" : Convert.ToString(dr["DD"]));
                }
            }
            return iList;
        }




        public SKUModel GetItem(string id)
        {


            SKUModel iList;


            string query = @"SELECT [ID_Sku]      ,[Code]      ,[SkuName]      ,[Diopter]      ,[Description]      ,[IsActive]      ,[SKU_ID]
      ,[id_StoringWay]      ,[Store_Name], [StoreCount], Picture,IsBausch  FROM [dbo].[SKU_VIEW]
  INNER JOIN [dbo].[fn_GetPermissions]('', 'optics') rts on rts.pmRead = 1 WHERE 1=1 AND  [ID_Sku] = " + id;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    iList = (new SKUModel
                    {
                        ID_Sku = Convert.ToInt32(dr["ID_Sku"]),
                        id_StoringWay = dr["id_StoringWay"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_StoringWay"]),
                        StoreCount = dr["StoreCount"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["StoreCount"]),
                        Code = Convert.ToString(dr["Code"]),
                        SkuName = Convert.ToString(dr["SkuName"]),
                        Description = Convert.ToString(dr["Description"]),
                        Store_Name = Convert.ToString(dr["Store_Name"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        IsBausch = Convert.ToBoolean(dr["IsBausch"]),
                        Diopter = Convert.ToSingle(dr["Diopter"]),
                        SKU_ID = Convert.ToString(dr["SKU_ID"]),
                        Picture = dr["Picture"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["Picture"]),
                        pagecount = 1
                    });
                    return iList;
                }
            }
            else return null;
            return null;
        }



        public bool insertItem(SKUModel id)
        {
            int did = 0;
           
            if (id.ID_Sku > 0)
                did = id.ID_Sku;
            int yy = id.IsActive ? 1 : 0;

            int isb = id.IsBausch ? 1 : 0;
            string query = @"INSERT INTO [dbo].[sku]
        ([Code]           ,[SkuName]           ,[Diopter]           ,[Description]           ,[IsActive]  ,[SKU_ID]           ,[StoreCount]           ,[id_StoringWay], IsBausch)
OUTPUT Inserted.ID_Sku
    VALUES
          ('" + id.Code + "' ,'" + id.SkuName + "' ," + id.Diopter.ToString().Replace(",", ".") + ", '" + id.Description + "', " + yy.ToString() + ",'" + id.SKU_ID + "'," + id.StoreCount + @",(SELECT [id_store]      
  FROM[dbo].[Storing] WHERE[Store_Name] = '" + id.Store_Name + @"'), " + isb.ToString() + "); ";
            if (did == null || did <= 0)
            {
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                    return true;

            }
            return false;

        }


        public bool UpdateItem(SKUModel id)
        {
            int isb = id.IsBausch ? 1 : 0;
            int yy = id.IsActive ? 1 : 0;
            string query = @"UPDATE [dbo].[sku]
   SET [IsActive] = " + yy.ToString() + @"
      ,[SkuName] = '" + id.SkuName + @"'
      ,[Description] = '" + id.Description + @"'
      ,[Code] = '" + id.Code + @"'
,IsBausch = " + isb.ToString() + @"
,[SKU_ID] = '" + id.SKU_ID + @"'
,Diopter = " + id.Diopter.ToString().Replace(",", ".") + @"
,[StoreCount]   =      " + id.StoreCount + @"  
,[id_StoringWay] = (SELECT [id_store]      
  FROM [dbo].[Storing] WHERE [Store_Name] = '" + id.Store_Name + @"')
 WHERE [ID_Sku]  = " + id.ID_Sku.ToString();
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Trim().Length == 0)
            {
                return true;
            }
            return false;
        }

        public bool DeleteItem(int id)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[sku]  WHERE [ID_Sku] = " + id;
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;
            }
            catch { return false; }
        }





        public bool DeleteItemCheck(string id)
        {
            try
            {
                OpticModel iList = new OpticModel();
                string query = @"SELECT 'true';";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr[0].ToString() == "true")
                            return true;
                        else
                            return false;
                    }
                }
                else return false;
            }
            catch { return false; }
            return false;
        }







}
}
