﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class RegionDBHandler
    {

        public DbClass dbc = new DbClass();
        public int count;
        public int page;
        public string sort = "ID_Town";
        public string order = "ASC";
        public bool insertItem(RegionModel id /*string sortOrder, bool asc, int pagenumber, int rows, ref int count, string searcher*/)
        {
            try
            {
                string query = @"INSERT INTO [dbo].[regions] ([Code],[RegionName],[RegionIndex])
OUTPUT Inserted.ID_Region
     VALUES
           ('" + id.Code + @"' , '" + id.RegionName + @"'," + id.RegionIndex + ")";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                else return false;
            }
            catch { return false; }
        }


        public List<RegionModel> GetRegions()
        {
            List<RegionModel> users = new List<RegionModel>();
            try
            {
                string query = @"SELECT [ID_Region],[Code],[RegionName],[RegionIndex]  FROM [dbo].[regions] ";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        users.Add(new RegionModel
                        {
                            ID_Region = Convert.ToInt32(dr["ID_Region"]),
                            RegionName = Convert.ToString(dr["RegionName"]),
                            Code = Convert.ToString(dr["Code"]),
                            RegionIndex = Convert.ToInt32(dr["RegionIndex"])
                        });
                    }
                }
            }
            catch { }
            return users;
        }

        public RegionModel GetOneRegion(string Code)
        {
            RegionModel users = new RegionModel();
            try
            {
                string query = @"SELECT [ID_Region],[Code],[RegionName],[RegionIndex]  FROM [dbo].[regions]
WHERE [ID_Region] = " + Code + "";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        users = (new RegionModel
                        {
                            ID_Region = Convert.ToInt32(dr["ID_Region"]),
                            RegionName = Convert.ToString(dr["RegionName"]),
                            Code = Convert.ToString(dr["Code"]),
                            RegionIndex = Convert.ToInt32(dr["RegionIndex"])
                        });
                    }
                }
            }
            catch { }
            return users;
        }


        public bool DeleteRegion(string id)
        {
            try
            {
                RegionModel iList = new RegionModel();
                string query = @"DELETE FROM [dbo].[regions] WHERE [ID_Region] = " + id + @";";
                                    

                dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                else return false;
            }
            catch { return false; }

        }


    }
}
