﻿using System;
using System.Collections.Generic;
using System.Data;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class OpticDBHandler
    {
        public DbClass dbc = new DbClass();
        public int count;
        public int page;        
        public string NetworkName = "";
        public string OpticTorgName = "";
        public string OpticJuraName = "";    
        public string INN = "";
        public string UserName = "";
        public string KASName = "";
        public string TownName = "";
        public string Phone = "";
        public string Email1 = "";
        public string Email2 = "";
        public string Email3 = "";
        public string OnlineLink = "";
        public string Kids1 = ""; 
        public string SMS = "";
        public string Digital = "";
        public string Latitude = "";
        public string Longitude = "";
        public string Doc1 = "";
        public string IsActive = "";
        public string sort = "ID_Optic";
        public string order = "DESC";
        public string restrict = "";
        public string Address = "";

        /*     
         *     [OnlineLink]      ,[Kids]      ,[SMS]
             ,[Digital]      ,[id_Doc1]      ,[id_Doc2]      ,[id_Doc3]      ,[Doc1]      ,[Doc2]      ,[Doc3]      ,[Latitude]      ,[Longitude]      ,[IsActive]      ,[AccessCode]
         *     · Сеть Оптик - Юридическое торговое название(Сеть Оптик) с возможностью выбора одного значения из справочника Сети Оптик
       · Торговое название Оптики
       · Юридическое название Оптики
       · ИНН Оптики
       · Ответственный менеджер
       · Ответственный КАМ/KAS
       · Адрес
       · Телефон - с возможностью редактирования в структурированном текстовом поле(+7 (ХХХ) ХХХ-ХХ-ХХ)
       · E-mail ответственного лица в Оптике
       · E- mail контактного лица в Оптике(дополнительный)
       · E-mail лица, ответственного за запись в Оптике
       · Ссылка на online запись
       · Ведется прием детей до 18 лет? – Да/Нет
       · SMS
       · Digital
       · Широта
       · Долгота
       · Врач основной
       · Статус*/
        public List<OpticModel> GetItemList()
        {
            List<OpticModel> iList = new List<OpticModel>();
            string NetworkNamefilter = "";
        string OpticTorgNamefilter = "";
        string OpticJuraNamefilter = "";
        string INNfilter = "";
        string UserNamefilter = "";
        string KASNamefilter = "";
        string TownNamefilter = "";
        string Phonefilter = "";
        string Email1filter = "";
        string Email2filter = "";
        string Email3filter = "";
        string OnlineLinkfilter = "";
        string Kidsfilter = "";
        string SMSfilter = "";
        string Digitalfilter = "";
        string Latitudefilter = "";
        string Longitudefilter = "";
        string Doc1filter = "";
        string IsActivefilter = "";
            string Addressfilter = "";


            if (Address.Length > 0)
            {
                Addressfilter = " AND Address IN (";

                int y = 0;
                foreach (string tt in Address.Split('|'))
                {
                    if (y > 0)
                        Addressfilter += ", ";
                    Addressfilter += "'" + tt + "'";
                    y++;
                }
                Addressfilter += ") ";
            }

            if (NetworkName.Length > 0)
            {
                NetworkNamefilter = " AND NetworkName IN (";

                    int y = 0;
                    foreach (string tt in NetworkName.Split('|'))
                    {
                        if (y > 0)
                        NetworkNamefilter += ", ";
                    NetworkNamefilter += "'" + tt + "'";
                        y++;
                    }
                NetworkNamefilter += ") ";
            }

            if (OpticTorgName.Length > 0)
            {
                OpticTorgNamefilter = " AND OpticTorgName IN (";

                int y = 0;
                foreach (string tt in OpticTorgName.Split('|'))
                {
                    if (y > 0)
                        OpticTorgNamefilter += ", ";
                    OpticTorgNamefilter += "'" + tt + "'";
                    y++;
                }
                OpticTorgNamefilter += ") ";
            }
            if (OpticJuraName.Length > 0)
            {
                OpticJuraNamefilter = " AND OpticJuraName IN (";

                int y = 0;
                foreach (string tt in OpticJuraName.Split('|'))
                {
                    if (y > 0)
                        OpticJuraNamefilter += ", ";
                    OpticJuraNamefilter += "'" + tt + "'";
                    y++;
                }
                OpticJuraNamefilter += ") ";
            }
            if (INN.Length > 0)
            {
                INNfilter = " AND OpticINN IN (";

                int y = 0;
                foreach (string tt in INN.Split('|'))
                {
                    if (y > 0)
                        INNfilter += ", ";
                    INNfilter += "'" + tt + "'";
                    y++;
                }
                INNfilter += ") ";
            }
            if (UserName.Length > 0)
            {
                UserNamefilter = " AND UserName IN (";

                int y = 0;
                foreach (string tt in UserName.Split('|'))
                {
                    if (y > 0)
                        UserNamefilter += ", ";
                    UserNamefilter += "'" + tt + "'";
                    y++;
                }
                UserNamefilter += ") ";
            }
            if (KASName.Length > 0)
            {
                KASNamefilter = " AND KASName IN (";

                int y = 0;
                foreach (string tt in KASName.Split('|'))
                {
                    if (y > 0)
                        KASNamefilter += ", ";
                    KASNamefilter += "'" + tt + "'";
                    y++;
                }
                KASNamefilter += ") ";
            }
            if (TownName.Length > 0)
            {
                TownNamefilter = " AND TownName IN (";

                int y = 0;
                foreach (string tt in TownName.Split('|'))
                {
                    if (y > 0)
                        TownNamefilter += ", ";
                    TownNamefilter += "'" + tt + "'";
                    y++;
                }
                TownNamefilter += ") ";
            }
            if (Phone.Length > 0)
            {
                Phonefilter = " AND Phone IN (";

                int y = 0;
                foreach (string tt in Phone.Split('|'))
                {
                    if (y > 0)
                        Phonefilter += ", ";
                    Phonefilter += "'" + tt + "'";
                    y++;
                }
                Phonefilter += ") ";
            }
            if (Email1.Length > 0)
            {
                Email1filter = " AND Email1 IN (";

                int y = 0;
                foreach (string tt in Email1.Split('|'))
                {
                    if (y > 0)
                        Email1filter += ", ";
                    Email1filter += "'" + tt + "'";
                    y++;
                }
                Email1filter += ") ";
            }
            if (Email2.Length > 0)
            {
                Email2filter = " AND Email2 IN (";

                int y = 0;
                foreach (string tt in Email2.Split('|'))
                {
                    if (y > 0)
                        Email2filter += ", ";
                    Email2filter += "'" + tt + "'";
                    y++;
                }
                Email2filter += ") ";
            }
            if (Email3.Length > 0)
            {
                Email3filter = " AND Email3 IN (";

                int y = 0;
                foreach (string tt in Email3.Split('|'))
                {
                    if (y > 0)
                        Email3filter += ", ";
                    Email3filter += "'" + tt + "'";
                    y++;
                }
                Email3filter += ") ";
            }
           
            if (OnlineLink.Length > 0)
            {
                OnlineLinkfilter = " AND OnlineLink IN (";

                int y = 0;
                foreach (string tt in OnlineLink.Split('|'))
                {
                    if (y > 0)
                        OnlineLinkfilter += ", ";
                    OnlineLinkfilter += "'" + tt + "'";
                    y++;
                }
                OnlineLinkfilter += ") ";
            }
            if (Kids1.Length > 0)
            {
                Kidsfilter = " AND Kids IN (";

                int y = 0;
                foreach (string tt in Kids1.Split('|'))
                {
                    if (y > 0)
                        Kidsfilter += ", ";
                    Kidsfilter += "'" + tt + "'";
                    y++;
                }
                Kidsfilter += ") ";
            }
            if (SMS.Length > 0)
            {
                SMSfilter = " AND SMS IN (";

                int y = 0;
                foreach (string tt in SMS.Split('|'))
                {
                    if (y > 0)
                        SMSfilter += ", ";
                    SMSfilter += "'" + tt + "'";
                    y++;
                }
                SMSfilter += ") ";
            }
            if (Digital.Length > 0)
            {
                Digitalfilter = " AND Digital IN (";

                int y = 0;
                foreach (string tt in Digital.Split('|'))
                {
                    if (y > 0)
                        Digitalfilter += ", ";
                    Digitalfilter += "'" + tt + "'";
                    y++;
                }
                Digitalfilter += ") ";
            }
            if (Latitude.Length > 0)
            {
                Latitudefilter = " AND Latitude IN (";

                int y = 0;
                foreach (string tt in Latitude.Split('|'))
                {
                    if (y > 0)
                        Latitudefilter += ", ";
                    Latitudefilter += "'" + tt + "'";
                    y++;
                }
                Latitudefilter += ") ";
            }
            if (Longitude.Length > 0)
            {
                Longitudefilter = " AND Longitude IN (";

                int y = 0;
                foreach (string tt in Longitude.Split('|'))
                {
                    if (y > 0)
                        Longitudefilter += ", ";
                    Longitudefilter += "'" + tt + "'";
                    y++;
                }
                Longitudefilter += ") ";
            }
            if (Doc1.Length > 0)
            {
                Doc1filter = " AND Doc1 IN (";

                int y = 0;
                foreach (string tt in Doc1.Split('|'))
                {
                    if (y > 0)
                        Doc1filter += ", ";
                    Doc1filter += "'" + tt + "'";
                    y++;
                }
                Doc1filter += ") ";
            }


              

            if (IsActive.Length > 0)
            {
                IsActivefilter = @" AND [IsActive] = " + IsActive;
            }

            string sorter = " ORDER BY ID_Optic ";
            if (sort != null && sort.Trim().Length > 0)
            {
                sorter = " ORDER BY " + sort + " ";
                if (order != null && order.Trim().Length > 0)
                    sorter += order;
            }
            string offset = "";
            offset = "  OFFSET " + ((page - 1) * count).ToString() + " ROWS FETCH NEXT " + count.ToString() + " ROWS ONLY";
            string query = @"SELECT 1
  FROM [dbo].[OpticsView]
  INNER JOIN [dbo].[fn_GetPermissions]('', 'optics') rts on rts.pmRead = 1 WHERE 1=1  " + restrict + NetworkNamefilter+              OpticTorgNamefilter+              OpticJuraNamefilter+              INNfilter+
              UserNamefilter+              KASNamefilter+              TownNamefilter+              Phonefilter+              Email1filter+              Email2filter+              Email3filter+
              OnlineLinkfilter+              Kidsfilter+              SMSfilter+              Digitalfilter+              Latitudefilter+              Longitudefilter+              Doc1filter+
              IsActivefilter + Addressfilter + sorter;
           DataTable dt = dbc.Make_Query(query);
            int co = dt.Rows.Count;
            query = @"SELECT [ID_Optic]      ,[ID_Network]      ,[NetworkName]      ,[NetworkINN]      ,[OpticTorgName]      ,[OpticJuraName]      ,[OpticINN]      ,[ID_User]      ,[UserName]
      ,[ID_KAS]      ,[KASName]      ,[ID_Region]      ,[RegionName]      ,[ID_Town]      ,[TownName]      ,[ID_TownBlock]      ,[TownBlockName]      ,[ID_SubTown]      ,[SubTownName]      ,[ID_SubSubTown]
      ,[SubSubTownName]      ,[ID_Street]      ,[StreetName]      ,[House]      ,[Korpus]      ,[Phone]      ,[Email1]      ,[Email2]      ,[Email3]      ,[OnlineLink]      ,[Kids]      ,[SMS]
      ,[Digital]      ,[id_Doc1]      ,[id_Doc2]      ,[id_Doc3]      ,[Doc1]      ,[Doc2]      ,[Doc3]      ,[Latitude]      ,[Longitude]      ,[IsActive]      ,[AccessCode], 
      [FiasAddress]   ,[ClientCode]      ,[BrickVC]      ,[ShowOnCard], Email, DeliverAddress  
,(SELECT COALESCE([Email],'')  FROM [dbo].[users] uu WHERE uu.ID_User = ov.[ID_User]) AS TMMAIL
,(SELECT COALESCE([Email],'')  FROM [dbo].[users] uu WHERE uu.ID_User = [ID_KAS]) AS KASMAIL, Metro FROM [dbo].[OpticsView] ov
  INNER JOIN [dbo].[fn_GetPermissions]('', 'optics') rts on rts.pmRead = 1 WHERE 1=1  " + restrict + NetworkNamefilter + OpticTorgNamefilter + OpticJuraNamefilter + INNfilter +
              UserNamefilter + KASNamefilter + TownNamefilter + Phonefilter + Email1filter + Email2filter + Email3filter +
              OnlineLinkfilter + Kidsfilter + SMSfilter + Digitalfilter + Latitudefilter + Longitudefilter + Doc1filter +
              IsActivefilter + Addressfilter + sorter + offset;
            dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new OpticModel
                    {
                        DeliverAddress = dr["DeliverAddress"] == Convert.DBNull ? "" : Convert.ToString(dr["DeliverAddress"]),
                        Email = dr["Email"] == Convert.DBNull ? "" : Convert.ToString(dr["Email"]),
                        ShowOnCard = dr["ShowOnCard"] == Convert.DBNull ? false : Convert.ToBoolean(dr["ShowOnCard"]),
                        ClientCode = dr["ClientCode"] == Convert.DBNull ? "" : Convert.ToString(dr["ClientCode"]),
                        BrickVC = dr["BrickVC"] == Convert.DBNull ? "" : Convert.ToString(dr["BrickVC"]),
                        FiasAddress = dr["FiasAddress"] == Convert.DBNull ? "" : Convert.ToString(dr["FiasAddress"]),
                        ID_optic = dr["ID_Optic"]== Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Optic"]),
                        ID_Network = dr["ID_Network"] == Convert.DBNull ?-1 : Convert.ToInt32(dr["ID_Network"]),
                        NetworkName = dr["NetworkName"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkName"]),
                        NetworkINN = dr["NetworkINN"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkINN"]),
                        OpticTorgName = dr["OpticTorgName"]== Convert.DBNull ? "" : Convert.ToString(dr["OpticTorgName"]),
                        OpticJuraName =  dr["OpticJuraName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticJuraName"]),
                        OpticINN =  dr["OpticINN"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticINN"]),
                        ID_User = dr["ID_User"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_User"]),
                        UserName =  dr["UserName"] == Convert.DBNull ? "" : Convert.ToString(dr["UserName"]),
                        ID_KAS = dr["ID_KAS"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_KAS"]),
                        KASName =  dr["KASName"] == Convert.DBNull ? "" : Convert.ToString(dr["KASName"]),
                        ID_Region = dr["ID_Region"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Region"]),
                        RegionName =  dr["RegionName"] == Convert.DBNull ? "" : Convert.ToString(dr["RegionName"]),
                        ID_Town = dr["ID_Town"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Town"]),
                        TownName =  dr["TownName"] == Convert.DBNull ? "" : Convert.ToString(dr["TownName"]),
                        ID_TownBlock =  dr["ID_TownBlock"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_TownBlock"]),
                        TownBlockName =  dr["TownBlockName"] == Convert.DBNull ? "" : Convert.ToString(dr["TownBlockName"]),
                        ID_SubTown =  dr["ID_SubTown"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_SubTown"]),
                        SubTownName =  dr["SubTownName"] == Convert.DBNull ? "" : Convert.ToString(dr["SubTownName"]),
                        ID_SubSubTown =  dr["ID_SubSubTown"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_SubSubTown"]),
                        SubSubTownName =  dr["SubSubTownName"] == Convert.DBNull ? "" : Convert.ToString(dr["SubSubTownName"]),
                        ID_Street =  dr["ID_Street"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Street"]),
                        StreetName =  dr["StreetName"] == Convert.DBNull ? "" : Convert.ToString(dr["StreetName"]),
                        House =  dr["House"] == Convert.DBNull ? "" : Convert.ToString(dr["House"]),
                        Korpus =  dr["Korpus"] == Convert.DBNull ? "" : Convert.ToString(dr["Korpus"]),
                        Phone =  dr["Phone"] == Convert.DBNull ? "" : Convert.ToString(dr["Phone"]),
                        Email1 =  dr["Email1"] == Convert.DBNull ? "" : Convert.ToString(dr["Email1"]),
                        Email2 =  dr["Email2"] == Convert.DBNull ? "" : Convert.ToString(dr["Email2"]),
                        Email3 =  dr["Email3"] == Convert.DBNull ? "" : Convert.ToString(dr["Email3"]),
                        OnlineLink =  dr["OnlineLink"] == Convert.DBNull ? "" : Convert.ToString(dr["OnlineLink"]),                        
                        Kids = dr["Kids"] == Convert.DBNull ? false : Convert.ToBoolean(dr["Kids"]),
                        Sms = dr["SMS"] == Convert.DBNull ? false : Convert.ToBoolean(dr["SMS"]),
                       Digital = dr["Digital"] == Convert.DBNull ? false : Convert.ToBoolean(dr["Digital"]),
                        id_Doc1 =  dr["id_Doc1"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc1"]),
                        Doc1 =  dr["Doc1"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc1"]),
                        id_Doc2 =  dr["id_Doc2"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc2"]),
                        Doc2 =  dr["Doc2"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc2"]),
                        id_Doc3 =  dr["id_Doc3"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc3"]),
                        Doc3 =  dr["Doc3"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc3"]),                        
                        Latitude = dr["Latitude"] == Convert.DBNull ? 0 : Convert.ToSingle(dr["Latitude"]),
                        Longitude = dr["Longitude"] == Convert.DBNull ? 0 : Convert.ToSingle(dr["Longitude"]),
                        IsActive = dr["IsActive"] == Convert.DBNull ? false : Convert.ToBoolean(dr["IsActive"]),
                        TMEmail = dr["TMMAIL"] == Convert.DBNull ? "" : Convert.ToString(dr["TMMAIL"]),
                        KASEmail = dr["KASMAIL"] == Convert.DBNull ? "" : Convert.ToString(dr["KASMAIL"]),
                        Metro = dr["Metro"] == Convert.DBNull ? "" : Convert.ToString(dr["Metro"]),
                        AccessCode = Convert.ToString(dr["AccessCode"]),
                        pagecount = co
                    }) ;
                    y++;
                }
            }
            return iList;
        }










        //-------------------------------------------------------------------------------excel
        public List<OpticModel> GetItemListExcel()
        {
            List<OpticModel> iList = new List<OpticModel>();
            string NetworkNamefilter = "";
            string OpticTorgNamefilter = "";
            string OpticJuraNamefilter = "";
            string INNfilter = "";
            string UserNamefilter = "";
            string KASNamefilter = "";
            string TownNamefilter = "";
            string Phonefilter = "";
            string Email1filter = "";
            string Email2filter = "";
            string Email3filter = "";
            string OnlineLinkfilter = "";
            string Kidsfilter = "";
            string SMSfilter = "";
            string Digitalfilter = "";
            string Latitudefilter = "";
            string Longitudefilter = "";
            string Doc1filter = "";
            string IsActivefilter = "";

            if (NetworkName.Length > 0)
            {
                NetworkNamefilter = " AND NetworkName IN (";

                int y = 0;
                foreach (string tt in NetworkName.Split('|'))
                {
                    if (y > 0)
                        NetworkNamefilter += ", ";
                    NetworkNamefilter += "'" + tt + "'";
                    y++;
                }
                NetworkNamefilter += ") ";
            }

            if (OpticTorgName.Length > 0)
            {
                OpticTorgNamefilter = " AND OpticTorgName IN (";

                int y = 0;
                foreach (string tt in OpticTorgName.Split('|'))
                {
                    if (y > 0)
                        OpticTorgNamefilter += ", ";
                    OpticTorgNamefilter += "'" + tt + "'";
                    y++;
                }
                OpticTorgNamefilter += ") ";
            }
            if (OpticJuraName.Length > 0)
            {
                OpticJuraNamefilter = " AND OpticJuraName IN (";

                int y = 0;
                foreach (string tt in OpticJuraName.Split('|'))
                {
                    if (y > 0)
                        OpticJuraNamefilter += ", ";
                    OpticJuraNamefilter += "'" + tt + "'";
                    y++;
                }
                OpticJuraNamefilter += ") ";
            }
            if (INN.Length > 0)
            {
                INNfilter = " AND INN IN (";

                int y = 0;
                foreach (string tt in INN.Split('|'))
                {
                    if (y > 0)
                        INNfilter += ", ";
                    INNfilter += "'" + tt + "'";
                    y++;
                }
                INNfilter += ") ";
            }
            if (UserName.Length > 0)
            {
                UserNamefilter = " AND UserName IN (";

                int y = 0;
                foreach (string tt in UserName.Split('|'))
                {
                    if (y > 0)
                        UserNamefilter += ", ";
                    UserNamefilter += "'" + tt + "'";
                    y++;
                }
                UserNamefilter += ") ";
            }
            if (KASName.Length > 0)
            {
                KASNamefilter = " AND KASName IN (";

                int y = 0;
                foreach (string tt in KASName.Split('|'))
                {
                    if (y > 0)
                        KASNamefilter += ", ";
                    KASNamefilter += "'" + tt + "'";
                    y++;
                }
                KASNamefilter += ") ";
            }
            if (TownName.Length > 0)
            {
                TownNamefilter = " AND TownName IN (";

                int y = 0;
                foreach (string tt in TownName.Split('|'))
                {
                    if (y > 0)
                        TownNamefilter += ", ";
                    TownNamefilter += "'" + tt + "'";
                    y++;
                }
                TownNamefilter += ") ";
            }
            if (Phone.Length > 0)
            {
                Phonefilter = " AND Phone IN (";

                int y = 0;
                foreach (string tt in Phone.Split('|'))
                {
                    if (y > 0)
                        Phonefilter += ", ";
                    Phonefilter += "'" + tt + "'";
                    y++;
                }
                Phonefilter += ") ";
            }
            if (Email1.Length > 0)
            {
                Email1filter = " AND Email1 IN (";

                int y = 0;
                foreach (string tt in Email1.Split('|'))
                {
                    if (y > 0)
                        Email1filter += ", ";
                    Email1filter += "'" + tt + "'";
                    y++;
                }
                Email1filter += ") ";
            }
            if (Email2.Length > 0)
            {
                Email2filter = " AND Email2 IN (";

                int y = 0;
                foreach (string tt in Email2.Split('|'))
                {
                    if (y > 0)
                        Email2filter += ", ";
                    Email2filter += "'" + tt + "'";
                    y++;
                }
                Email2filter += ") ";
            }
            if (Email3.Length > 0)
            {
                Email3filter = " AND Email3 IN (";

                int y = 0;
                foreach (string tt in Email3.Split('|'))
                {
                    if (y > 0)
                        Email3filter += ", ";
                    Email3filter += "'" + tt + "'";
                    y++;
                }
                Email3filter += ") ";
            }

            if (OnlineLink.Length > 0)
            {
                OnlineLinkfilter = " AND OnlineLink IN (";

                int y = 0;
                foreach (string tt in OnlineLink.Split('|'))
                {
                    if (y > 0)
                        OnlineLinkfilter += ", ";
                    OnlineLinkfilter += "'" + tt + "'";
                    y++;
                }
                OnlineLinkfilter += ") ";
            }
            if (Kids1.Length > 0)
            {
                Kidsfilter = " AND Kids IN (";

                int y = 0;
                foreach (string tt in Kids1.Split('|'))
                {
                    if (y > 0)
                        Kidsfilter += ", ";
                    Kidsfilter += "'" + tt + "'";
                    y++;
                }
                Kidsfilter += ") ";
            }
            if (SMS.Length > 0)
            {
                SMSfilter = " AND SMS IN (";

                int y = 0;
                foreach (string tt in SMS.Split('|'))
                {
                    if (y > 0)
                        SMSfilter += ", ";
                    SMSfilter += "'" + tt + "'";
                    y++;
                }
                SMSfilter += ") ";
            }
            if (Digital.Length > 0)
            {
                Digitalfilter = " AND Digital IN (";

                int y = 0;
                foreach (string tt in Digital.Split('|'))
                {
                    if (y > 0)
                        Digitalfilter += ", ";
                    Digitalfilter += "'" + tt + "'";
                    y++;
                }
                Digitalfilter += ") ";
            }
            if (Latitude.Length > 0)
            {
                Latitudefilter = " AND Latitude IN (";

                int y = 0;
                foreach (string tt in Latitude.Split('|'))
                {
                    if (y > 0)
                        Latitudefilter += ", ";
                    Latitudefilter += "'" + tt + "'";
                    y++;
                }
                Latitudefilter += ") ";
            }
            if (Longitude.Length > 0)
            {
                Longitudefilter = " AND Longitude IN (";

                int y = 0;
                foreach (string tt in Longitude.Split('|'))
                {
                    if (y > 0)
                        Longitudefilter += ", ";
                    Longitudefilter += "'" + tt + "'";
                    y++;
                }
                Longitudefilter += ") ";
            }
            if (Doc1.Length > 0)
            {
                Doc1filter = " AND Doc1 IN (";

                int y = 0;
                foreach (string tt in Doc1.Split('|'))
                {
                    if (y > 0)
                        Doc1filter += ", ";
                    Doc1filter += "'" + tt + "'";
                    y++;
                }
                Doc1filter += ") ";
            }
            if (IsActive.Length > 0)
            {
                IsActivefilter = @" AND [IsActive] = " + IsActive;
            }

            string sorter = " ORDER BY ID_Optic ";
            if (sort != null && sort.Trim().Length > 0)
            {
                sorter = " ORDER BY " + sort + " ";
                if (order != null && order.Trim().Length > 0)
                    sorter += order;
            }
            
            string query = @"SELECT [ID_Optic]      ,[ID_Network]      ,[NetworkName]      ,[NetworkINN]      ,[OpticTorgName]      ,[OpticJuraName]      ,[OpticINN]      ,[ID_User]      ,[UserName]
      ,[ID_KAS]      ,[KASName]      ,[ID_Region]      ,[RegionName]      ,[ID_Town]      ,[TownName]      ,[ID_TownBlock]      ,[TownBlockName]      ,[ID_SubTown]      ,[SubTownName]      ,[ID_SubSubTown]
      ,[SubSubTownName]      ,[ID_Street]      ,[StreetName]      ,[House]      ,[Korpus]      ,[Phone]      ,[Email1]      ,[Email2]      ,[Email3]      ,[OnlineLink]      ,[Kids]      ,[SMS]
      ,[Digital]      ,[id_Doc1]      ,[id_Doc2]      ,[id_Doc3]      ,[Doc1]      ,[Doc2]      ,[Doc3]      ,[Latitude]      ,[Longitude]      ,[IsActive]      ,[AccessCode]
,[FiasAddress]      ,[ClientCode]      ,[BrickVC]      ,[ShowOnCard], Email, DeliverAddress,(SELECT COALESCE([Email],'')  FROM [dbo].[users] uu WHERE uu.ID_User = ov.[ID_User]) AS TMMAIL
,(SELECT COALESCE([Email],'')  FROM [dbo].[users] uu WHERE uu.ID_User = [ID_KAS]) AS KASMAIL, Metro, [Address] 
FROM [dbo].[OpticsView] ov
  INNER JOIN [dbo].[fn_GetPermissions]('', 'optics') rts on rts.pmRead = 1 WHERE 1=1  " + restrict + NetworkNamefilter + OpticTorgNamefilter + OpticJuraNamefilter + INNfilter +
              UserNamefilter + KASNamefilter + TownNamefilter + Phonefilter + Email1filter + Email2filter + Email3filter +
              OnlineLinkfilter + Kidsfilter + SMSfilter + Digitalfilter + Latitudefilter + Longitudefilter + Doc1filter +
              IsActivefilter + sorter;
           DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new OpticModel
                    {
                        DeliverAddress = dr["DeliverAddress"] == Convert.DBNull ? "" : Convert.ToString(dr["DeliverAddress"]),
                        Email = dr["Email"] == Convert.DBNull ? "" : Convert.ToString(dr["Email"]),
                        ShowOnCard = dr["ShowOnCard"] == Convert.DBNull ? false : Convert.ToBoolean(dr["ShowOnCard"]),
                        ClientCode = dr["ClientCode"] == Convert.DBNull ? "" : Convert.ToString(dr["ClientCode"]),
                        BrickVC = dr["BrickVC"] == Convert.DBNull ? "" : Convert.ToString(dr["BrickVC"]),
                        FiasAddress = dr["FiasAddress"] == Convert.DBNull ? "" : Convert.ToString(dr["FiasAddress"]),
                        ID_optic = dr["ID_Optic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Optic"]),
                        ID_Network = dr["ID_Network"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Network"]),
                        NetworkName = dr["NetworkName"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkName"]),
                        NetworkINN = dr["NetworkINN"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkINN"]),
                        OpticTorgName = dr["OpticTorgName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticTorgName"]),
                        OpticJuraName = dr["OpticJuraName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticJuraName"]),
                        OpticINN = dr["OpticINN"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticINN"]),
                        ID_User = dr["ID_User"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_User"]),
                        UserName = dr["UserName"] == Convert.DBNull ? "" : Convert.ToString(dr["UserName"]),
                        ID_KAS = dr["ID_KAS"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_KAS"]),
                        KASName = dr["KASName"] == Convert.DBNull ? "" : Convert.ToString(dr["KASName"]),
                        ID_Region = dr["ID_Region"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Region"]),
                        RegionName = dr["RegionName"] == Convert.DBNull ? "" : Convert.ToString(dr["RegionName"]),
                        ID_Town = dr["ID_Town"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Town"]),
                        TownName = dr["TownName"] == Convert.DBNull ? "" : Convert.ToString(dr["TownName"]),
                        ID_TownBlock = dr["ID_TownBlock"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_TownBlock"]),
                        TownBlockName = dr["TownBlockName"] == Convert.DBNull ? "" : Convert.ToString(dr["TownBlockName"]),
                        ID_SubTown = dr["ID_SubTown"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_SubTown"]),
                        SubTownName = dr["SubTownName"] == Convert.DBNull ? "" : Convert.ToString(dr["SubTownName"]),
                        ID_SubSubTown = dr["ID_SubSubTown"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_SubSubTown"]),
                        SubSubTownName = dr["SubSubTownName"] == Convert.DBNull ? "" : Convert.ToString(dr["SubSubTownName"]),
                        ID_Street = dr["ID_Street"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Street"]),
                        StreetName = dr["StreetName"] == Convert.DBNull ? "" : Convert.ToString(dr["StreetName"]),
                        House = dr["House"] == Convert.DBNull ? "" : Convert.ToString(dr["House"]),
                        Korpus = dr["Korpus"] == Convert.DBNull ? "" : Convert.ToString(dr["Korpus"]),
                        Phone = dr["Phone"] == Convert.DBNull ? "" : Convert.ToString(dr["Phone"]),
                        Email1 = dr["Email1"] == Convert.DBNull ? "" : Convert.ToString(dr["Email1"]),
                        Email2 = dr["Email2"] == Convert.DBNull ? "" : Convert.ToString(dr["Email2"]),
                        Email3 = dr["Email3"] == Convert.DBNull ? "" : Convert.ToString(dr["Email3"]),
                        OnlineLink = dr["OnlineLink"] == Convert.DBNull ? "" : Convert.ToString(dr["OnlineLink"]),
                        Kids = dr["Kids"] == Convert.DBNull ? false : Convert.ToBoolean(dr["Kids"]),
                        Sms = dr["SMS"] == Convert.DBNull ? false : Convert.ToBoolean(dr["SMS"]),
                        Digital = dr["Digital"] == Convert.DBNull ? false : Convert.ToBoolean(dr["Digital"]),
                        id_Doc1 = dr["id_Doc1"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc1"]),
                        Doc1 = dr["Doc1"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc1"]),
                        id_Doc2 = dr["id_Doc2"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc2"]),
                        Doc2 = dr["Doc2"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc2"]),
                        id_Doc3 = dr["id_Doc3"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc3"]),
                        Doc3 = dr["Doc3"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc3"]),
                        Latitude = dr["Latitude"] == Convert.DBNull ? 0 : Convert.ToSingle(dr["Latitude"]),
                        Longitude = dr["Longitude"] == Convert.DBNull ? 0 : Convert.ToSingle(dr["Longitude"]),
                        IsActive = dr["IsActive"] == Convert.DBNull ? false : Convert.ToBoolean(dr["IsActive"]),
                        TMEmail = dr["TMMAIL"] == Convert.DBNull ? "" : Convert.ToString(dr["TMMAIL"]),
                        KASEmail = dr["KASMAIL"] == Convert.DBNull ? "" : Convert.ToString(dr["KASMAIL"]),
                        Metro = dr["Metro"] == Convert.DBNull ? "" : Convert.ToString(dr["Metro"]),
                        AccessCode = dr["AccessCode"] == Convert.DBNull ? "" : Convert.ToString(dr["AccessCode"])  ,
                        Address = dr["Address"] == Convert.DBNull ? "" : Convert.ToString(dr["Address"])
                    });
                    y++;
                }
            }
            return iList;
        }

        //////////----------------------------------------------------




        /// ////////////////////
        /// 

        public List<string> GetDistinctList(string itemj)
        {
            List<string> iList = new List<string>();


          
            string query = @"SELECT DISTINCT " + itemj + @" AS DD
  FROM [dbo].[OpticsView]";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(
                      Convert.ToString(dr["DD"]));


                    y++;
                }
            }
            return iList;
        }

        ///////////////////////////////////////////////////






        public OpticModel GetItem(string id /*string sortOrder, bool asc, int pagenumber, int rows, ref int count, string searcher*/)
        {
           OpticModel iList = new OpticModel();
           

            string query = @"SELECT [ID_Optic]      ,[ID_Network]      ,[NetworkName]      ,[NetworkINN]      ,[OpticTorgName]      ,[OpticJuraName]      ,[OpticINN]      ,[ID_User]      ,[UserName]
      ,[ID_KAS]      ,[KASName]      ,[ID_Region]      ,[RegionName]      ,[ID_Town]      ,[TownName]      ,[ID_TownBlock]      ,[TownBlockName]      ,[ID_SubTown]      ,[SubTownName]      ,[ID_SubSubTown]
      ,[SubSubTownName]      ,[ID_Street]      ,[StreetName]      ,[House]      ,[Korpus]      ,[Phone]      ,[Email1]      ,[Email2]      ,[Email3]      ,[OnlineLink]      ,[Kids]      ,[SMS]
      ,[Digital]      ,[id_Doc1]      ,[id_Doc2]      ,[id_Doc3]      ,[Doc1]      ,[Doc2]      ,[Doc3]      ,[Latitude]      ,[Longitude]      ,[IsActive]      ,[AccessCode]
,[FiasAddress]      ,[ClientCode]      ,[BrickVC]      ,[ShowOnCard], Email, DeliverAddress  
,(SELECT COALESCE([Email],'')  FROM [dbo].[users] uu WHERE uu.ID_User = ov.[ID_User]) AS TMMAIL
,(SELECT COALESCE([Email],'')  FROM [dbo].[users] uu WHERE uu.ID_User = [ID_KAS]) AS KASMAIL, Metro FROM [dbo].[OpticsView] ov
  INNER JOIN [dbo].[fn_GetPermissions]('', 'optics') rts on rts.pmRead = 1 WHERE  [ID_Optic] = " + id + restrict;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList = (new OpticModel
                    {
                        DeliverAddress = dr["DeliverAddress"] == Convert.DBNull ? "" : Convert.ToString(dr["DeliverAddress"]),
                        Email = dr["Email"] == Convert.DBNull ? "" : Convert.ToString(dr["Email"]),
                        ShowOnCard = dr["ShowOnCard"] == Convert.DBNull ? false : Convert.ToBoolean(dr["ShowOnCard"]),
                        ClientCode = dr["ClientCode"] == Convert.DBNull ? "" : Convert.ToString(dr["ClientCode"]),
                        BrickVC = dr["BrickVC"] == Convert.DBNull ? "" : Convert.ToString(dr["BrickVC"]),
                        FiasAddress = dr["FiasAddress"] == Convert.DBNull ? "" : Convert.ToString(dr["FiasAddress"]),
                        ID_optic = dr["ID_Optic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Optic"]),
                        ID_Network = dr["ID_Network"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Network"]),
                        NetworkName = dr["NetworkName"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkName"]),
                        NetworkINN = dr["NetworkINN"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkINN"]),
                        OpticTorgName = dr["OpticTorgName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticTorgName"]),
                        OpticJuraName = dr["OpticJuraName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticJuraName"]),
                        OpticINN = dr["OpticINN"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticINN"]),
                        ID_User = dr["ID_User"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_User"]),
                        UserName = dr["UserName"] == Convert.DBNull ? "" : Convert.ToString(dr["UserName"]),
                        ID_KAS = dr["ID_KAS"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_KAS"]),
                        KASName = dr["KASName"] == Convert.DBNull ? "" : Convert.ToString(dr["KASName"]),
                        ID_Region = dr["ID_Region"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Region"]),
                        RegionName = dr["RegionName"] == Convert.DBNull ? "" : Convert.ToString(dr["RegionName"]),
                        ID_Town = dr["ID_Town"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Town"]),
                        TownName = dr["TownName"] == Convert.DBNull ? "" : Convert.ToString(dr["TownName"]),
                        ID_TownBlock = dr["ID_TownBlock"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_TownBlock"]),
                        TownBlockName = dr["TownBlockName"] == Convert.DBNull ? "" : Convert.ToString(dr["TownBlockName"]),
                        ID_SubTown = dr["ID_SubTown"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_SubTown"]),
                        SubTownName = dr["SubTownName"] == Convert.DBNull ? "" : Convert.ToString(dr["SubTownName"]),
                        ID_SubSubTown = dr["ID_SubSubTown"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_SubSubTown"]),
                        SubSubTownName = dr["SubSubTownName"] == Convert.DBNull ? "" : Convert.ToString(dr["SubSubTownName"]),
                        ID_Street = dr["ID_Street"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Street"]),
                        StreetName = dr["StreetName"] == Convert.DBNull ? "" : Convert.ToString(dr["StreetName"]),
                        House = dr["House"] == Convert.DBNull ? "" : Convert.ToString(dr["House"]),
                        Korpus = dr["Korpus"] == Convert.DBNull ? "" : Convert.ToString(dr["Korpus"]),
                        Phone = dr["Phone"] == Convert.DBNull ? "" : Convert.ToString(dr["Phone"]),
                        Email1 = dr["Email1"] == Convert.DBNull ? "" : Convert.ToString(dr["Email1"]),
                        Email2 = dr["Email2"] == Convert.DBNull ? "" : Convert.ToString(dr["Email2"]),
                        Email3 = dr["Email3"] == Convert.DBNull ? "" : Convert.ToString(dr["Email3"]),
                        OnlineLink = dr["OnlineLink"] == Convert.DBNull ? "" : Convert.ToString(dr["OnlineLink"]),
                        Kids = dr["Kids"] == Convert.DBNull ? false : Convert.ToBoolean(dr["Kids"]),
                        Sms = dr["SMS"] == Convert.DBNull ? false : Convert.ToBoolean(dr["SMS"]),
                        Digital = dr["Digital"] == Convert.DBNull ? false : Convert.ToBoolean(dr["Digital"]),
                        id_Doc1 = dr["id_Doc1"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc1"]),
                        Doc1 = dr["Doc1"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc1"]),
                        id_Doc2 = dr["id_Doc2"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc2"]),
                        Doc2 = dr["Doc2"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc2"]),
                        id_Doc3 = dr["id_Doc3"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc3"]),
                        Doc3 = dr["Doc3"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc3"]),
                        Latitude = dr["Latitude"] == Convert.DBNull ? 0 : Convert.ToSingle(dr["Latitude"]),
                        Longitude = dr["Longitude"] == Convert.DBNull ? 0 : Convert.ToSingle(dr["Longitude"]),
                        IsActive = dr["IsActive"] == Convert.DBNull ? false : Convert.ToBoolean(dr["IsActive"]),
                        TMEmail = dr["TMMAIL"] == Convert.DBNull ? "" : Convert.ToString(dr["TMMAIL"]),
                        KASEmail = dr["KASMAIL"] == Convert.DBNull ? "" : Convert.ToString(dr["KASMAIL"]),
                        Metro = dr["Metro"] == Convert.DBNull ? "" : Convert.ToString(dr["Metro"]),
                        AccessCode = Convert.ToString(dr["AccessCode"]),
                        pagecount = 1
                    });


                    y++;
                }
            }
            return iList;
        }




        public bool insertItem(OpticModel id /*string sortOrder, bool asc, int pagenumber, int rows, ref int count, string searcher*/)
        {
            try
            {
                OpticModel iList = new OpticModel();

                string hh = @"
EXEC    [dbo].[INSERT_towns_short]		@p_TownName = N'" + id.TownName + "',		@p_ID_Region = " + id.ID_Region.ToString() + @";
EXEC	[dbo].[INSERT_address_Object] @Streetname = N'" + id.StreetName + @"', @p_Code = 'street';
EXEC	[dbo].[INSERT_address_Object] @Streetname = N'" + id.TownBlockName + @"', @p_Code = 'TownBlock';
EXEC	[dbo].[INSERT_address_Object] @Streetname = N'" + id.SubTownName + @"', @p_Code = 'SubTown';
EXEC	[dbo].[INSERT_address_Object] @Streetname = N'" + id.SubSubTownName + @"', @p_Code = 'SubSubTown';
";
                string query = @"
INSERT INTO [dbo].[optics]
           ([ID_Network]           ,[OpticName]           ,[ID_User]           ,[ID_Town]           ,[Phone]           
           ,[IsActive] ,[Digital]           ,[SMS]           
,[Latitude]           ,[Longitude]                     ,[Optic_INN]           
 ,[Email_Resp]           ,[Email_Contact]           ,[Email_Registry]           ,[Online_string]           ,[Kids18less]           ,[DocMain]           ,[DocAdd1]
           ,[DocAdd2]           ,[AccessCode]           ,[MerchantName] ,[ID_Region]           ,[ID_KAS]           ,[building]
           ,[korpus]           ,[ID_TownBlock]           ,[ID_SubTown]           ,[ID_SubSubTown]           ,[ID_Street] ,[FiasAddress]
           ,[ClientCode]           ,[BrickVC]           ,[ShowOnCard]           ,[DeliverAddress], Metro) OUTPUT Inserted.ID_Optic
     VALUES (" + id.ID_Network.ToString() + ",'" + id.OpticJuraName + "', " + id.ID_User.ToString() +


     @", (SELECT TOP (1) [ID_Address]
  FROM [dbo].[FiasAddresses]
  WHERE id_Level = 1 AND Address_Name = '" + id.TownName + @"'
  AND ID_Parent = " + id.ID_Region + @"), '"

+ id.Phone + 
     @"',";
                if (id.IsActive)
                    query += "1";
                else
                    query += "0";
                query += ", ";
                if (id.Digital)
                    query += "1";
                else
                    query += "0";
                query += ",";
                if (id.Sms)
                    query += "1";
                else
                    query += "0";


                query += ", " + id.Latitude.ToString().Replace(",", ".") + ", " + id.Longitude.ToString().Replace(",", ".") + ", '";
                long hh1 = 0;
                if (id.OpticINN == null || id.OpticINN.Trim().Length == 0 || !long.TryParse(id.OpticINN,out hh1))
                    query += "-1";
                else
                    query += id.OpticINN;
                  query+=  "', '" + id.Email1 + "', '" + id.Email2 + "', '" + id.Email3 + "', '" + id.OnlineLink + "',";
                if (id.Kids)
                    query += "1";
                else
                    query += "0";
                query += ", " + id.id_Doc1 + ", " + id.id_Doc2 + ", " + id.id_Doc3  + ", '" + id.AccessCode + "','" + id.OpticTorgName + "'," + id.ID_Region + "," + id.ID_KAS +
                     ",'" + id.House + "','" + id.Korpus + @"', (SELECT TOP (1) [ID_Address]
FROM[dbo].[FiasAddresses] fa WHERE fa.ID_Parent = 10000001 AND [Address_Name] = '" + id.TownBlockName + @"'), (SELECT TOP (1) [ID_Address]
FROM[dbo].[FiasAddresses] fa WHERE fa.ID_Parent = 10000002 AND [Address_Name] = '" + id.SubTownName + @"'), (SELECT TOP (1) [ID_Address]
FROM[dbo].[FiasAddresses] fa WHERE fa.ID_Parent = 10000003 AND [Address_Name] = '" + id.SubSubTownName + @"'), (SELECT TOP (1) [ID_Address]
FROM[dbo].[FiasAddresses] fa WHERE fa.ID_Parent = 10000000 AND [Address_Name] = '" + id.StreetName + "')";
                query += ",'" + id.FiasAddress + "','" + id.ClientCode + "','" + id.BrickVC + "',";
                    if (id.ShowOnCard)
                    query += "1";
                else
                    query += "0";
                query += ", '" + id.DeliverAddress + "','" + id.Metro + "')";
                if (id.ID_optic > 0)
                {
                    query = @"UPDATE [dbo].[optics] 
SET [ID_Network] = " + id.ID_Network + @"
      ,[OpticName] = '" + id.OpticJuraName + @"'      ,[ID_User] = " + id.ID_User.ToString() + @"      ,[ID_Town] =  (SELECT TOP (1) [ID_Address]
  FROM [dbo].[FiasAddresses]
  WHERE id_Level = 1 AND Address_Name = '" + id.TownName + @"'
  AND ID_Parent = " + id.ID_Region + @") 
      ,[Phone] = '" + id.Phone + @"'";
                    if (id.IsActive)
                        query += ",[IsActive] = 1";
                    else
                        query += ",[IsActive] = 0";
                    query += ", ";
                    if (id.Digital)
                        query += "[Digital] = 1";
                    else
                        query += "[Digital] = 0";
                    query += ",";
                    if (id.Sms)
                        query += "[SMS] = 1";
                    else
                        query += "[SMS] = 0";
                    query += ", ";
                    if (id.Kids)
                        query += "[Kids18less] = 1";
                    else
                        query += "[Kids18less] = 0";


                    query += ", [Latitude] = " + id.Latitude.ToString().Replace(",", ".") + ", [Longitude] = " + id.Longitude.ToString().Replace(",", ".");
                    if (id.OpticINN == null || id.OpticINN.Trim().Length == 0 || !long.TryParse(id.OpticINN, out hh1))
                        query += ", [Optic_INN] = -1";
                    else
                        query += ", [Optic_INN] = " + id.OpticINN;
                    //", [Optic_INN] = '" + id.OpticINN +
                      query +=  ", [Email_Resp] = '" + id.Email1 + "', [Email_Contact] = '" + id.Email2 + "', [Email_Registry] = '" + id.Email3 + "', [Online_string] = '" + id.OnlineLink;
                    query += "', [DocMain] = " + id.id_Doc1 + ", [DocAdd1] = " + id.id_Doc2 + ", [DocAdd2] = " + id.id_Doc3 + ", [AccessCode] = '" + id.AccessCode + "',[MerchantName] = '"
                        + id.OpticTorgName + "' ,[LastDate] = CURRENT_TIMESTAMP, [ID_Region] = " + id.ID_Region + ", [ID_KAS] = " + id.ID_KAS +
                      ", [building] = '" + id.House + "', [korpus] = '" + id.Korpus + @"', [ID_TownBlock] =  (SELECT TOP (1) [ID_Address]
FROM[dbo].[FiasAddresses] fa WHERE fa.ID_Parent = 10000001 AND [Address_Name] = '" + id.TownBlockName + @"'), [ID_SubTown] = (SELECT TOP (1) [ID_Address]
FROM[dbo].[FiasAddresses] fa WHERE fa.ID_Parent = 10000002 AND [Address_Name] = '" + id.SubTownName + @"'), [ID_SubSubTown] = (SELECT TOP (1) [ID_Address]
FROM[dbo].[FiasAddresses] fa WHERE fa.ID_Parent = 10000003 AND [Address_Name] = '" + id.SubSubTownName + @"'), [ID_Street] =  (SELECT TOP (1) [ID_Address]
FROM [dbo].[FiasAddresses] fa WHERE fa.ID_Parent = 10000000 AND[Address_Name] = '" + id.StreetName + @"')";
                    query += ", FiasAddress='" + id.FiasAddress + "',ClientCode = '" + id.ClientCode + "', BrickVC = '" + id.BrickVC + "', ShowOnCard = ";
                    if (id.ShowOnCard)
                        query += "1";
                    else
                        query += "0";
                    query += ", DeliverAddress = '" + id.DeliverAddress + "',Metro = '" + id.Metro + "'";
                    query += @" WHERE ID_Optic = " + id.ID_optic;

                }
                query = hh + query;


                
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    if (id.ID_optic == null || id.ID_optic <= 0)
                    {
                        string yy = dt.Rows[0][0].ToString();
                        DeleteAllOpticDoctors(yy);

                        if (id.id_Doc1 != null && id.id_Doc1 > 0) AddOpticDoctor(id.id_Doc1.ToString(), yy);
                        if (id.id_Doc2 != null && id.id_Doc2 > 0) AddOpticDoctor(id.id_Doc2.ToString(), yy);
                        if (id.id_Doc3 != null && id.id_Doc3 > 0) AddOpticDoctor(id.id_Doc3.ToString(), yy);
                        return AddOpticMasterDoctor(yy);
                    }
                    else
                    {
                        DeleteAllOpticDoctors(id.ID_optic.ToString());

                        if (id.id_Doc1 != null && id.id_Doc1 > 0) AddOpticDoctor(id.id_Doc1.ToString(), id.ID_optic.ToString());
                        if (id.id_Doc2 != null && id.id_Doc2 > 0) AddOpticDoctor(id.id_Doc2.ToString(), id.ID_optic.ToString());
                        if (id.id_Doc3 != null && id.id_Doc3 > 0) AddOpticDoctor(id.id_Doc3.ToString(), id.ID_optic.ToString());
                        return AddOpticMasterDoctor(id.ID_optic.ToString());
                    }
                }
                else return false;
            }
            catch { return false; }
        }

        public bool DeleteItem(string id /*string sortOrder, bool asc, int pagenumber, int rows, ref int count, string searcher*/)
        {
            try
            {
                OpticModel iList = new OpticModel();


                string query = @"
DELETE
  FROM [dbo].[optic_users]
 WHERE [ID_Optic] = " + id + @";
IF NOT EXISTS(SELECT 1
  FROM [dbo].[OpticsUsersView_Api]
  WHERE [ID_Optic] = " + id + @" )
  BEGIN
DELETE FROM [dbo].[optics]  WHERE ID_Optic = " + id +
                
                @";
                END";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                else return false;
            }
            catch { return false; }
        }


        public bool DeleteItemCheck(string id /*string sortOrder, bool asc, int pagenumber, int rows, ref int count, string searcher*/)
        {
            try
            {
                OpticModel iList = new OpticModel();
                string query = @"
IF NOT EXISTS
(
--SELECT 1  FROM [dbo].[OpticsUsersView_Api]  
SELECT 1  FROM [dbo].[diagnostics]
  WHERE ID_Sertificat IN
  (SELECT [ID_Sertificat] FROM [dbo].[MindBox_Certificates] WHERE ID_Optic IN (" + id + @"))
--WHERE [ID_Optic] = " + id + @" 
)
  BEGIN
  SELECT 'true';
  END
  ELSE
  BEGIN
  SELECT 'false';
  END";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {                    
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr[0].ToString() == "true")
                            return true;
                        else
                            return false;
                    }
                }
                else return false;
            }
            catch { return false; }
            return false;
        }






        public List<Doctors> GetAllDoctors()
        {
            List<Doctors> iList = new List<Doctors>();

            string query = @"SELECT [ID_User], COALESCE([Surname],'') + ' ' + COALESCE([Name],'') + ' ' + COALESCE([Patronymic],'') AS GF FROM  [dbo].[users]
  WHERE [isDoc] = 1";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new Doctors
                    {
                        ID_Doc = Convert.ToInt32(dr["ID_User"]),
                        DocName = Convert.ToString(dr["GF"])
                    });
                    y++;
                }
            }
            return iList;
        }

        public List<OpticDoctors> GetAllOpticDoctors(int id)
        {
            List<OpticDoctors> iList = new List<OpticDoctors>();
            string IsActivefilter = "";
            if (IsActive.Length > 0)
            {
                IsActivefilter = @" AND ou.ID_User IN (SELECT [ID_User]
  FROM [dbo].[users] WHERE [IsActive] = " + IsActive + ")";
            }
            string query = @"SELECT ou.ID_Optic, ou.[ID_User], COALESCE([Name],'') + ' ' + COALESCE([Surname],'') + ' ' + COALESCE([Patronymic],'') AS GF 
  FROM  [dbo].[optic_users] ou 
  LEFT OUTER JOIN [dbo].[users] uu ON uu.ID_User = ou.ID_User
  WHERE ID_Optic = " + id.ToString() + IsActivefilter;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new OpticDoctors
                    {
                        id_Optic = Convert.ToInt32(dr["ID_Optic"]),
                        ID_Doc = Convert.ToInt32(dr["ID_User"]),
                        DocName = Convert.ToString(dr["GF"])
                    });
                    y++;
                }
            }
            return iList;
        }

        public List<NetworksDoctors> GetAllNetworkDoctors(int id)
        {
            List<NetworksDoctors> iList = new List<NetworksDoctors>();
            string IsActivefilter = "";
            if (IsActive.Length > 0)
            {
                if (IsActive == "true" || IsActive == "1") IsActive = "1";
                else if (IsActive == "false" || IsActive == "0") IsActive = "0";

                IsActivefilter = @" AND ou.ID_User IN (SELECT [ID_User]
  FROM [dbo].[users] WHERE [IsActive] = " + IsActive + ")";
            }
            string query = @"SELECT ou.ID_Network, ou.[ID_User], COALESCE([Name],'') + ' ' + COALESCE([Surname],'') + ' ' + COALESCE([Patronymic],'') AS GF 
  FROM  [dbo].[networks_users] ou 
  LEFT OUTER JOIN [dbo].[users] uu ON uu.ID_User = ou.ID_User
  WHERE ID_Network = " + id.ToString() + IsActivefilter;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new NetworksDoctors
                    {
                        id_Network = Convert.ToInt32(dr["ID_Network"]),
                        ID_Doc = Convert.ToInt32(dr["ID_User"]),
                        DocName = Convert.ToString(dr["GF"])
                    });
                    y++;
                }
            }
            return iList;
        }


        public List<OpticDoctors> GetDoctors(string id )
        {
            List<OpticDoctors> iList = new List<OpticDoctors>();
            string IsActivefilter = "";
            if (IsActive.Length > 0)
            {
                IsActivefilter = @" AND ou.ID_User IN (SELECT [ID_User]
  FROM [dbo].[users] WHERE [IsActive] = '" + IsActive + "')";
            }

            string query = @"SELECT ou.ID_Optic, ou.[ID_User], COALESCE([Surname],'') + ' ' + COALESCE([Name],'') + ' ' + COALESCE([Patronymic],'') AS GF
  FROM [dbo].[optic_users] ou
  LEFT OUTER JOIN dbo.users uu ON uu.ID_User = ou.ID_User
  WHERE ID_Optic = " + id + IsActivefilter;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new OpticDoctors
                    {
                        id_Optic = Convert.ToInt32(dr["ID_Optic"]),
                        ID_Doc = Convert.ToInt32(dr["ID_User"]),
                        DocName = Convert.ToString(dr["GF"])
                    });
                    y++;
                }
            }
            return iList;
        }



        public List<Doctors> GetOnRole(string id)
        {
            List<Doctors> iList = new List<Doctors>();

            string query = @"SELECT uu.[ID_User], COALESCE([Name],'') + ' ' + COALESCE([Surname],'') + ' ' + COALESCE([Patronymic],'') AS GF
  FROM dbo.users uu WHERE uu.ID_User IN
  (SELECT [ID_User] FROM [dbo].[user_roles] WHERE ID_Role IN
  (SELECT [ID_Role] FROM [dbo].[roles]
WHERE Code = '" + id + "'))";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new Doctors
                    {
                        ID_Doc = Convert.ToInt32(dr["ID_User"]),
                        DocName = Convert.ToString(dr["GF"])
                    });
                    y++;
                }
            }
            return iList;
        }


        public bool DeleteOpticDoctor(string idd, string idop)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[optic_users] WHERE [ID_Optic] = " + idop + " AND ID_User = " + idd;
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;
            }
            catch { return false; }
        }

        public bool DeleteNetworkDoctor(string idd, string idop)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[networks_users] WHERE [ID_Network] = " + idop + " AND ID_User = " + idd;
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;
            }
            catch { return false; }
        }

        public bool DeleteAllOpticDoctors(string idop)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[optic_users] WHERE [ID_Optic] = " + idop;
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;
            }
            catch { return false; }
        }


        public bool AddOpticDoctor(string idd, string ido)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[optic_users] WHERE [ID_Optic] = " + ido + " AND [ID_User] = " + idd + @";  INSERT INTO [dbo].[optic_users] ([ID_Optic] ,[ID_User])
     VALUES (" + ido + " , " + idd + ") " ;
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;
            }
            catch { return false; }
        }

        public bool AddNetworkDoctor(string idd, string ido)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[networks_users] WHERE [ID_Network] = " + ido + " AND [ID_User] = " + idd + @";  INSERT INTO [dbo].[networks_users] ([ID_Network] ,[ID_User])
     VALUES (" + ido + " , " + idd + ") ";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;
            }
            catch { return false; }
        }



        public bool AddOpticMasterDoctor(string ido)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[optic_users] WHERE [ID_Optic] = " + ido + @" AND [ID_User] = (SELECT [ID_User]
  FROM[dbo].[users]
  WHERE CODE = 'Мастер врач');  INSERT INTO [dbo].[optic_users] ([ID_Optic] ,[ID_User])
     VALUES (" + ido + @" , (SELECT [ID_User]
  FROM[dbo].[users]
  WHERE CODE = 'Мастер врач')) ";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;
            }
            catch { return false; }
        }




        public List<OpticModel> GetItemListForPost(OpticModelPost id)
        {
            List<OpticModel> iList = new List<OpticModel>();
            string NetworkNamefilter = "";
            string OpticTorgNamefilter = "";
            string OpticJuraNamefilter = "";
            string INNfilter = "";
            string UserNamefilter = "";
            string KASNamefilter = "";
            string TownNamefilter = "";
            string Phonefilter = "";
            string Email1filter = "";
            string Email2filter = "";
            string Email3filter = "";
            string OnlineLinkfilter = "";
            string Kidsfilter = "";
            string SMSfilter = "";
            string Digitalfilter = "";
            string Latitudefilter = "";
            string Longitudefilter = "";
            string Doc1filter = "";
            string IsActivefilter = "";
            string Addressfilter = "";


            if (id.Address != null && id.Address.Length > 0)
            {
                Addressfilter = " AND Address IN (";

                int y = 0;
                foreach (string tt in id.Address.Split('|'))
                {
                    if (y > 0)
                        Addressfilter += ", ";
                    Addressfilter += "'" + tt + "'";
                    y++;
                }
                Addressfilter += ") ";
            }

            if (id.NetworkName!=null && id.NetworkName.Length > 0)
            {
                NetworkNamefilter = " AND NetworkName IN (";

                int y = 0;
                foreach (string tt in id.NetworkName.Split('|'))
                {
                    if (y > 0)
                        NetworkNamefilter += ", ";
                    NetworkNamefilter += "'" + tt + "'";
                    y++;
                }
                NetworkNamefilter += ") ";
            }

            if (id.OpticTorgName != null && id.OpticTorgName.Length > 0)
            {
                OpticTorgNamefilter = " AND OpticTorgName IN (";

                int y = 0;
                foreach (string tt in id.OpticTorgName.Split('|'))
                {
                    if (y > 0)
                        OpticTorgNamefilter += ", ";
                    OpticTorgNamefilter += "'" + tt + "'";
                    y++;
                }
                OpticTorgNamefilter += ") ";
            }
            if (id.OpticJuraName != null && id.OpticJuraName.Length > 0)
            {
                OpticJuraNamefilter = " AND OpticJuraName IN (";

                int y = 0;
                foreach (string tt in id.OpticJuraName.Split('|'))
                {
                    if (y > 0)
                        OpticJuraNamefilter += ", ";
                    OpticJuraNamefilter += "'" + tt + "'";
                    y++;
                }
                OpticJuraNamefilter += ") ";
            }
            if (id.OpticINN != null && id.OpticINN.Length > 0)
            {
                INNfilter = " AND OpticINN IN (";

                int y = 0;
                foreach (string tt in id.OpticINN.Split('|'))
                {
                    if (y > 0)
                        INNfilter += ", ";
                    INNfilter += "'" + tt + "'";
                    y++;
                }
                INNfilter += ") ";
            }
            if (id.UserName != null && id.UserName.Length > 0)
            {
                UserNamefilter = " AND UserName IN (";

                int y = 0;
                foreach (string tt in id.UserName.Split('|'))
                {
                    if (y > 0)
                        UserNamefilter += ", ";
                    UserNamefilter += "'" + tt + "'";
                    y++;
                }
                UserNamefilter += ") ";
            }
            if (id.KASName != null && id.KASName.Length > 0)
            {
                KASNamefilter = " AND KASName IN (";

                int y = 0;
                foreach (string tt in id.KASName.Split('|'))
                {
                    if (y > 0)
                        KASNamefilter += ", ";
                    KASNamefilter += "'" + tt + "'";
                    y++;
                }
                KASNamefilter += ") ";
            }
            if (id.TownName != null && id.TownName.Length > 0)
            {
                TownNamefilter = " AND TownName IN (";

                int y = 0;
                foreach (string tt in id.TownName.Split('|'))
                {
                    if (y > 0)
                        TownNamefilter += ", ";
                    TownNamefilter += "'" + tt + "'";
                    y++;
                }
                TownNamefilter += ") ";
            }
            if (id.Phone != null && id.Phone.Length > 0)
            {
                Phonefilter = " AND Phone IN (";

                int y = 0;
                foreach (string tt in id.Phone.Split('|'))
                {
                    if (y > 0)
                        Phonefilter += ", ";
                    Phonefilter += "'" + tt + "'";
                    y++;
                }
                Phonefilter += ") ";
            }
            if (id.Email1 != null && id.Email1.Length > 0)
            {
                Email1filter = " AND Email1 IN (";

                int y = 0;
                foreach (string tt in id.Email1.Split('|'))
                {
                    if (y > 0)
                        Email1filter += ", ";
                    Email1filter += "'" + tt + "'";
                    y++;
                }
                Email1filter += ") ";
            }
            if (id.Email2 != null && id.Email2.Length > 0)
            {
                Email2filter = " AND Email2 IN (";

                int y = 0;
                foreach (string tt in id.Email2.Split('|'))
                {
                    if (y > 0)
                        Email2filter += ", ";
                    Email2filter += "'" + tt + "'";
                    y++;
                }
                Email2filter += ") ";
            }
            if (id.Email3 != null && id.Email3.Length > 0)
            {
                Email3filter = " AND Email3 IN (";

                int y = 0;
                foreach (string tt in id.Email3.Split('|'))
                {
                    if (y > 0)
                        Email3filter += ", ";
                    Email3filter += "'" + tt + "'";
                    y++;
                }
                Email3filter += ") ";
            }

            if (id.OnlineLink != null && id.OnlineLink.Length > 0)
            {
                OnlineLinkfilter = " AND OnlineLink IN (";

                int y = 0;
                foreach (string tt in id.OnlineLink.Split('|'))
                {
                    if (y > 0)
                        OnlineLinkfilter += ", ";
                    OnlineLinkfilter += "'" + tt + "'";
                    y++;
                }
                OnlineLinkfilter += ") ";
            }
            if (id.Kids != null)
            {
                Kidsfilter = " AND Kids IN (" + id.Kids.ToString();


                Kidsfilter += ") ";
            }
            if (id.Sms != null)
            {
                SMSfilter = " AND SMS IN (" + id.Sms.ToString();

                SMSfilter += ") ";
            }
            if (id.Digital != null)
            {
                Digitalfilter = " AND Digital IN (" + id.Digital.ToString();
               
                Digitalfilter += ") ";
            }
            if (id.IsActive != null && id.IsActive != null)
            {
                IsActivefilter = @" AND [IsActive] = " + id.IsActive.ToString();
            }
            if (id.Latitude != null && id.Latitude > 0)
            {
                Latitudefilter = " AND Latitude IN (" + id.Latitude.ToString().Replace(",",".");               
                Latitudefilter += ") ";
            }
            if (id.Longitude != null && id.Longitude > 0)
            {
                Longitudefilter = " AND Longitude IN (" + Longitude.ToString().Replace(",", ".");
                
                Longitudefilter += ") ";
            }
            if (id.Doc1 != null && id.Doc1.Length > 0)
            {
                Doc1filter = " AND Doc1 IN (";

                int y = 0;
                foreach (string tt in id.Doc1.Split('|'))
                {
                    if (y > 0)
                        Doc1filter += ", ";
                    Doc1filter += "'" + tt + "'";
                    y++;
                }
                Doc1filter += ") ";
            }




            

            string sorter = " ORDER BY ID_Optic ";
            if (id.sort != null && id.sort.Trim().Length > 0)
            {
                sorter = " ORDER BY " + id.sort + " ";
                if (id.order != null && id.order.Trim().Length > 0)
                    sorter += id.order;
            }
            string offset = "";
            offset = "  OFFSET " + ((id.page - 1) * id.count).ToString() + " ROWS FETCH NEXT " + id.count.ToString() + " ROWS ONLY";
            string query = @"SELECT 1
  FROM [dbo].[OpticsView]
  INNER JOIN [dbo].[fn_GetPermissions]('', 'optics') rts on rts.pmRead = 1 WHERE 1=1  " + restrict + NetworkNamefilter + OpticTorgNamefilter + OpticJuraNamefilter + INNfilter +
              UserNamefilter + KASNamefilter + TownNamefilter + Phonefilter + Email1filter + Email2filter + Email3filter +
              OnlineLinkfilter + Kidsfilter + SMSfilter + Digitalfilter + Latitudefilter + Longitudefilter + Doc1filter +
              IsActivefilter + Addressfilter + sorter;
            DataTable dt = dbc.Make_Query(query);
            int co = dt.Rows.Count;
            query = @"SELECT [ID_Optic]      ,[ID_Network]      ,[NetworkName]      ,[NetworkINN]      ,[OpticTorgName]      ,[OpticJuraName]      ,[OpticINN]      ,[ID_User]      ,[UserName]
      ,[ID_KAS]      ,[KASName]      ,[ID_Region]      ,[RegionName]      ,[ID_Town]      ,[TownName]      ,[ID_TownBlock]      ,[TownBlockName]      ,[ID_SubTown]      ,[SubTownName]      ,[ID_SubSubTown]
      ,[SubSubTownName]      ,[ID_Street]      ,[StreetName]      ,[House]      ,[Korpus]      ,[Phone]      ,[Email1]      ,[Email2]      ,[Email3]      ,[OnlineLink]      ,[Kids]      ,[SMS]
      ,[Digital]      ,[id_Doc1]      ,[id_Doc2]      ,[id_Doc3]      ,[Doc1]      ,[Doc2]      ,[Doc3]      ,[Latitude]      ,[Longitude]      ,[IsActive]      ,[AccessCode], 
      [FiasAddress]   ,[ClientCode]      ,[BrickVC]      ,[ShowOnCard], Email, DeliverAddress  , [Address]
,(SELECT COALESCE([Email],'')  FROM [dbo].[users] uu WHERE uu.ID_User = ov.[ID_User]) AS TMMAIL
,(SELECT COALESCE([Email],'')  FROM [dbo].[users] uu WHERE uu.ID_User = [ID_KAS]) AS KASMAIL, Metro FROM [dbo].[OpticsView] ov
  INNER JOIN [dbo].[fn_GetPermissions]('', 'optics') rts on rts.pmRead = 1 WHERE 1=1  " + restrict + NetworkNamefilter + OpticTorgNamefilter + OpticJuraNamefilter + INNfilter +
              UserNamefilter + KASNamefilter + TownNamefilter + Phonefilter + Email1filter + Email2filter + Email3filter +
              OnlineLinkfilter + Kidsfilter + SMSfilter + Digitalfilter + Latitudefilter + Longitudefilter + Doc1filter +
              IsActivefilter + Addressfilter + sorter + offset;
            dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new OpticModel
                    {
                        Address =  dr["Address"] == Convert.DBNull ? "" : Convert.ToString(dr["Address"]),
                        DeliverAddress = dr["DeliverAddress"] == Convert.DBNull ? "" : Convert.ToString(dr["DeliverAddress"]),
                        Email = dr["Email"] == Convert.DBNull ? "" : Convert.ToString(dr["Email"]),
                        ShowOnCard = dr["ShowOnCard"] == Convert.DBNull ? false : Convert.ToBoolean(dr["ShowOnCard"]),
                        ClientCode = dr["ClientCode"] == Convert.DBNull ? "" : Convert.ToString(dr["ClientCode"]),
                        BrickVC = dr["BrickVC"] == Convert.DBNull ? "" : Convert.ToString(dr["BrickVC"]),
                        FiasAddress = dr["FiasAddress"] == Convert.DBNull ? "" : Convert.ToString(dr["FiasAddress"]),
                        ID_optic = dr["ID_Optic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Optic"]),
                        ID_Network = dr["ID_Network"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Network"]),
                        NetworkName = dr["NetworkName"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkName"]),
                        NetworkINN = dr["NetworkINN"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkINN"]),
                        OpticTorgName = dr["OpticTorgName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticTorgName"]),
                        OpticJuraName = dr["OpticJuraName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticJuraName"]),
                        OpticINN = dr["OpticINN"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticINN"]),
                        ID_User = dr["ID_User"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_User"]),
                        UserName = dr["UserName"] == Convert.DBNull ? "" : Convert.ToString(dr["UserName"]),
                        ID_KAS = dr["ID_KAS"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_KAS"]),
                        KASName = dr["KASName"] == Convert.DBNull ? "" : Convert.ToString(dr["KASName"]),
                        ID_Region = dr["ID_Region"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Region"]),
                        RegionName = dr["RegionName"] == Convert.DBNull ? "" : Convert.ToString(dr["RegionName"]),
                        ID_Town = dr["ID_Town"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Town"]),
                        TownName = dr["TownName"] == Convert.DBNull ? "" : Convert.ToString(dr["TownName"]),
                        ID_TownBlock = dr["ID_TownBlock"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_TownBlock"]),
                        TownBlockName = dr["TownBlockName"] == Convert.DBNull ? "" : Convert.ToString(dr["TownBlockName"]),
                        ID_SubTown = dr["ID_SubTown"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_SubTown"]),
                        SubTownName = dr["SubTownName"] == Convert.DBNull ? "" : Convert.ToString(dr["SubTownName"]),
                        ID_SubSubTown = dr["ID_SubSubTown"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_SubSubTown"]),
                        SubSubTownName = dr["SubSubTownName"] == Convert.DBNull ? "" : Convert.ToString(dr["SubSubTownName"]),
                        ID_Street = dr["ID_Street"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Street"]),
                        StreetName = dr["StreetName"] == Convert.DBNull ? "" : Convert.ToString(dr["StreetName"]),
                        House = dr["House"] == Convert.DBNull ? "" : Convert.ToString(dr["House"]),
                        Korpus = dr["Korpus"] == Convert.DBNull ? "" : Convert.ToString(dr["Korpus"]),
                        Phone = dr["Phone"] == Convert.DBNull ? "" : Convert.ToString(dr["Phone"]),
                        Email1 = dr["Email1"] == Convert.DBNull ? "" : Convert.ToString(dr["Email1"]),
                        Email2 = dr["Email2"] == Convert.DBNull ? "" : Convert.ToString(dr["Email2"]),
                        Email3 = dr["Email3"] == Convert.DBNull ? "" : Convert.ToString(dr["Email3"]),
                        OnlineLink = dr["OnlineLink"] == Convert.DBNull ? "" : Convert.ToString(dr["OnlineLink"]),
                        Kids = dr["Kids"] == Convert.DBNull ? false : Convert.ToBoolean(dr["Kids"]),
                        Sms = dr["SMS"] == Convert.DBNull ? false : Convert.ToBoolean(dr["SMS"]),
                        Digital = dr["Digital"] == Convert.DBNull ? false : Convert.ToBoolean(dr["Digital"]),
                        id_Doc1 = dr["id_Doc1"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc1"]),
                        Doc1 = dr["Doc1"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc1"]),
                        id_Doc2 = dr["id_Doc2"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc2"]),
                        Doc2 = dr["Doc2"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc2"]),
                        id_Doc3 = dr["id_Doc3"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_Doc3"]),
                        Doc3 = dr["Doc3"] == Convert.DBNull ? "" : Convert.ToString(dr["Doc3"]),
                        Latitude = dr["Latitude"] == Convert.DBNull ? 0 : Convert.ToSingle(dr["Latitude"]),
                        Longitude = dr["Longitude"] == Convert.DBNull ? 0 : Convert.ToSingle(dr["Longitude"]),
                        IsActive = dr["IsActive"] == Convert.DBNull ? false : Convert.ToBoolean(dr["IsActive"]),
                        TMEmail = dr["TMMAIL"] == Convert.DBNull ? "" : Convert.ToString(dr["TMMAIL"]),
                        KASEmail = dr["KASMAIL"] == Convert.DBNull ? "" : Convert.ToString(dr["KASMAIL"]),
                        Metro = dr["Metro"] == Convert.DBNull ? "" : Convert.ToString(dr["Metro"]),
                        AccessCode = Convert.ToString(dr["AccessCode"]),
                        pagecount = co
                    });
                    y++;
                }
            }
            return iList;
        }


























    }
}
