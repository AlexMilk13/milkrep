﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class CertificateModel
    {
        public int ID_Sertificat { get; set; }
        public int ID_Mindbox { get; set; }
        public string CodeInt { get; set; }
        public string CodeMindBox { get; set; }
        public int ID_Client_Int { get; set; }
        public int ID_Client_Mindbox { get; set; }
        public string Status { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateActivate { get; set; }
        public DateTime DateExpired { get; set; }
        public int ID_Optic { get; set; }
        public int ID_User { get; set; }
    }
}
