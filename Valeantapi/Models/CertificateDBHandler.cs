﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class CertificateDBHandler
    {

        public DbClass dbc = new DbClass();
        public int count;
        public int page;
        public string sort = "ID_Certificate";
        public string order = "ASC";
        public string CertCode = "";
        public string ErrorMessage = "";

        public bool CheckCertificate(string CertCode)
        {
            string query = @"DECLARE	@Code_Sertificate_MindBox varchar(255),
		@ErrorMessage varchar(max)

SELECT	@Code_Sertificate_MindBox = N' '
SELECT	@ErrorMessage = N' '

EXEC	[dbo].[MB_GetSertificateByCode]
		@session_hash = N'  ',
		@p_SertificateCode = N'Ae45Rty',
		@Code_Sertificate_MindBox = @Code_Sertificate_MindBox OUTPUT,
		@ErrorMessage = @ErrorMessage OUTPUT

SELECT	@Code_Sertificate_MindBox as N'Code',
		@ErrorMessage as N'Message'";

            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {

                return true;
            }
            else return false;
        }


        public bool InsertRequest(string mm, int userid, string Code)
        {
            string query = @"INSERT INTO [dbo].[Mindbox_Reqs]
           ([ID_User]           ,[LastDate]           ,[Request_Code]           ,[Response_Text])
     VALUES
           (" + userid.ToString() + @", CURRENT_TIMESTAMP, '" + Code + @"','" + mm + "')";

            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {

                return true;
            }
            else return false;
        }


        /*  public bool insertItem(CertificateModel id )
          {
              try
              {
                  string query = @"INSERT INTO [dbo].[dbo].[MindBox_Certificates] ([Code], [CertificateName], [ID_Region])

       VALUES
             ('" + id.Code + @"' , '" + id.CertificateName + @"'," + id.ID_Region.ToString() + ")";

                  DataTable dt = dbc.Make_Query(query);
                  if (dbc.last_error.Length == 0)
                  {
                      return true;
                  }
                  else return false;
              }
              catch { return false; }
          }


          public List<CertificateModel> GetCertificates()
          {
              List<CertificateModel> users = new List<CertificateModel>();
              try
              {
                  string query = @"SELECT [ID_Sertificat]
        ,[ID_Mindbox]
        ,[CodeInt]
        ,[CodeMindBox]
        ,[ID_Client_Int]
        ,[ID_Client_Mindbox]
        ,[Status]
        ,[DateCreate]
        ,[DateActivate]
        ,[DateExpired]
        ,[ID_Optic]
        ,[ID_User]
    FROM [dbo].[MindBox_Certificates] ";

                  DataTable dt = dbc.Make_Query(query);
                  if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                  {
                      foreach (DataRow dr in dt.Rows)
                      {
                          users.Add(new CertificateModel
                          {
                              ID_Certificate = Convert.ToInt32(dr["ID_Certificate"]),
                              ID_Region = Convert.ToInt32(dr["ID_Region"]),
                              RegionName = Convert.ToString(dr["RegionName"]),
                              Code = Convert.ToString(dr["Code"]),
                              CertificateName = Convert.ToString(dr["CertificateName"])
                          });
                      }
                  }
              }
              catch { }
              return users;
          }

          public CertificateModel GetOneCertificate(string Code)
          {
              CertificateModel users = new CertificateModel();
              try
              {
                  string query = @"SELECT [ID_Certificate]      ,tt.[Code]      ,[CertificateName]      ,tt.[ID_Region]	  ,rr.RegionName
    FROM [dbo].[Certificates] tt LEFT OUTER JOIN dbo.regions rr ON rr.ID_Region = tt.ID_Region
  WHERE [ID_Certificate] = " + Code + "";

                  DataTable dt = dbc.Make_Query(query);
                  if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                  {
                      foreach (DataRow dr in dt.Rows)
                      {
                          users = (new CertificateModel
                          {
                              ID_Certificate = Convert.ToInt32(dr["ID_Certificate"]),
                              ID_Region = Convert.ToInt32(dr["ID_Region"]),
                              RegionName = Convert.ToString(dr["RegionName"]),
                              Code = Convert.ToString(dr["Code"]),
                              CertificateName = Convert.ToString(dr["CertificateName"])
                          });
                      }
                  }
              }
              catch { }
              return users;
          }


          public bool DeleteCertificate(string id)
          {
              try
              {
                  string query = @"DELETE FROM FROM [dbo].[MindBox_Certificates] WHERE [ID_Sertificat] = " + id + @";";

                  dbc.Make_Query(query);
                  if (dbc.last_error.Length == 0)
                  {
                      return true;
                  }
                  else return false;
              }
              catch { return false; }

          }*/
    }
}
