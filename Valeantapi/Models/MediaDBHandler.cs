﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class MediaDBHandler
    {
        public DbClass dbc = new DbClass();

        public MediaFileModel GetItem(string id)
        {


            MediaFileModel iList = new MediaFileModel();


            string query = @"SELECT [mediafile]      ,[Code]      ,[FileType]      ,[FileId]  FROM [dbo].[MediaFiles]
 WHERE 1=1 AND  [FileId] = " + id;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    iList = (new MediaFileModel
                    {
                        mediafile = (byte[])dr["mediafile"],
                        Code = Convert.ToString(dr["Code"]),
                        FileType = Convert.ToString(dr["FileType"]),
                        FileId = Convert.ToInt32(dr["FileId"])
                    });

                }
            }
            return iList;
        }

        public MediaFileModel GetItem_byte(string id)
        {


            MediaFileModel iList = new MediaFileModel();


            string query = @"SELECT [mediafile]      ,[Code]      ,[FileType]      ,[FileId]  FROM [dbo].[MediaFiles]
 WHERE 1=1 AND  [FileId] IN (SELECT TOP (1)
[id_picture]
  FROM [dbo].[sku] WHERE [Description] = '" + id + "')";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    iList = (new MediaFileModel
                    {
                        mediafile = (byte[])dr["mediafile"],
                        Code = Convert.ToString(dr["Code"]),
                        FileType = Convert.ToString(dr["FileType"]),
                        FileId = Convert.ToInt32(dr["FileId"])
                    });

                }
            }
            return iList;
        }
    }
}
