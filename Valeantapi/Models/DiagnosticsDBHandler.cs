﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class DiagnosticsDBHandler
    {
        public string restrict = "";
        public DbClass dbc = new DbClass();
        public int count;
        public int page;
        public string sort = "ID_Optic";
        public string order = "ASC";

        public string CodeMindBox = "";//Код Сертификата,
        public string Opt_Net_Name = "";//Оптика Net,
        public string Opt_Name = "";//Оптика,
        public string ID_Optic = "";//Оптика,
        public string Client_FIO = "";//Имя Клиента,
        public string DoctorName = "";//Имя Врача,
        public DateTime DateBegin;//Дата начала,Время начала,
        public DateTime DateEnd;//Дата окончания,Время окончания
        public List<DiagnosticsModel> GetItemList()
        {
            List<DiagnosticsModel> iList = new List<DiagnosticsModel>();
        string CodeMindBoxFilter = "";//Код Сертификата,
        string Opt_Net_NameFilter = "";//Оптика Net,
        string Opt_NameFilter = "";//Оптика,
        string Client_FIOFilter = "";//Имя Клиента,
        string DoctorNameFilter = "";//Имя Врача,
        string DateBeginFilter = "";//Дата начала,Время начала,
        string DateEndFilter = "";//Дата окончания,Время окончания
            if (CodeMindBox.Length > 0)
            {
                CodeMindBoxFilter = " AND [CodeMindBox] IN (";

                int y = 0;
                foreach (string tt in CodeMindBox.Split('|'))
                {
                    if (y > 0)
                        CodeMindBoxFilter += ", ";
                    CodeMindBoxFilter += "'" + tt + "'";
                    y++;
                }
                CodeMindBoxFilter += ") ";
            }
            if (Opt_Net_Name.Length > 0)
            {
                Opt_Net_NameFilter = " AND Opt_Net_Name IN (";

                int y = 0;
                foreach (string tt in Opt_Net_Name.Split('|'))
                {
                    if (y > 0)
                        Opt_Net_NameFilter += ", ";
                    Opt_Net_NameFilter += "'" + tt + "'";
                    y++;
                }
                Opt_Net_NameFilter += ") ";
            }

            if (Opt_Name.Length > 0)
            {
                Opt_NameFilter = " AND OpticName IN (";

                int y = 0;
                foreach (string tt in Opt_Name.Split('|'))
                {
                    if (y > 0)
                        Opt_NameFilter += ", ";
                    Opt_NameFilter += "'" + tt + "'";
                    y++;
                }
                Opt_NameFilter += ") ";
            }

            if (Client_FIO.Length > 0)
            {
                Client_FIOFilter = " AND Client_FIO IN (";

                int y = 0;
                foreach (string tt in Client_FIO.Split('|'))
                {
                    if (y > 0)
                        Client_FIOFilter += ", ";
                    Client_FIOFilter += "'" + tt + "'";
                    y++;
                }
                Client_FIOFilter += ") ";
            }

            if (DoctorName.Length > 0)
            {
                DoctorNameFilter = " AND DoctorName IN (";

                int y = 0;
                foreach (string tt in DoctorName.Split('|'))
                {
                    if (y > 0)
                        DoctorNameFilter += ", ";
                    DoctorNameFilter += "'" + tt + "'";
                    y++;
                }
                DoctorNameFilter += ") ";
            }

            if (DateBegin != null && DateBegin!=new DateTime(1,1,1))
            {
                DateBeginFilter = " AND DateBegin >= ";
                
                    DateBeginFilter += "'" + DateBegin.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                 
                DateBeginFilter += " ";
            }

            if (DateEnd!=null && DateEnd != new DateTime(1, 1, 1))
            {
                DateEndFilter = " AND CAST([DateEnd] AS DATE) <= ";               
                    DateEndFilter += "'" + DateEnd.ToString("yyyy-MM-dd") + "'";
                DateEndFilter += " ";
            }
            string sorter = " ORDER BY ID_Optic ";
            if (sort != null && sort.Trim().Length > 0)
            {
                if (sort == "Opt_Name") sort = "OpticName";
                sorter = " ORDER BY " + sort + " ";
                if (order != null && order.Trim().Length > 0)
                    sorter += order;
            }
            string offset = "";
            offset = "  OFFSET " + ((page - 1) * count).ToString() + " ROWS FETCH NEXT " + count.ToString() + " ROWS ONLY";
            string query = @"SELECT [ID_Diagnostic]      ,[ID_Sertificat]      ,[CodeMindBox]      ,[id_opt_net]      ,[Opt_Net_Name]      ,[ID_Client_Int]      ,[Client_FIO]
                  ,[ID_Doctor]      ,[DoctorName]      ,[DateBegin]      ,[DateEnd], ID_Optic, OpticName ,dodo,cr.pmUpdate  FROM [dbo].[diagnostictsView] dv
            INNER JOIN [dbo].[fn_GetCanRead] ('', 'diagnostics') cr on ID_Diagnostic = cr.id  WHERE pmRead = 1  " + restrict + CodeMindBoxFilter + Opt_Net_NameFilter + Opt_NameFilter + Client_FIOFilter + DoctorNameFilter + DateBeginFilter + DateEndFilter;
            DataTable dt = dbc.Make_Query(query);
            count = dt.Rows.Count;
            query = @"SELECT [ID_Diagnostic]      ,[ID_Sertificat]      ,[CodeMindBox]      ,[id_opt_net]      ,[Opt_Net_Name]      ,[ID_Client_Int]      ,[Client_FIO]
      ,[ID_Doctor]      ,[DoctorName]      ,[DateBegin]      ,[DateEnd], ID_Optic, OpticName ,dodo,cr.pmUpdate  FROM [dbo].[diagnostictsView] dv
INNER JOIN [dbo].[fn_GetCanRead] ('', 'diagnostics') cr on ID_Diagnostic = cr.id  WHERE pmRead = 1  " + restrict + CodeMindBoxFilter + Opt_Net_NameFilter + Opt_NameFilter + Client_FIOFilter + DoctorNameFilter + DateBeginFilter + DateEndFilter + sorter + offset;
            dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new DiagnosticsModel
                    {
                        ID_Diagnostic = dr["ID_Diagnostic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Diagnostic"]),
                        ID_Sertificat = dr["ID_Sertificat"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Sertificat"]),
                        CodeMindBox = dr["CodeMindBox"] == Convert.DBNull ? "" : Convert.ToString(dr["CodeMindBox"]),//Код Сертификата,
                        id_opt_net = dr["id_opt_net"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_opt_net"]),//Сеть Оптик,
                        Opt_Net_Name = dr["Opt_Net_Name"] == Convert.DBNull ? "" : Convert.ToString(dr["Opt_Net_Name"]),//Оптика,
                        id_opt = dr["ID_Optic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Optic"]),//Сеть Оптик,
                        Opt_Name = dr["OpticName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticName"]),//Оптика,
                        ID_Client_Int = dr["ID_Client_Int"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Client_Int"]),
                        Client_FIO = dr["Client_FIO"] == Convert.DBNull ? "" : Convert.ToString(dr["Client_FIO"]),//Имя Клиента,
                        ID_Doctor = dr["ID_Doctor"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Doctor"]),
                        DoctorName = dr["DoctorName"] == Convert.DBNull ? "" : Convert.ToString(dr["DoctorName"]),//Имя Врача,
                        DateBegin = dr["DateBegin"] == Convert.DBNull ? new DateTime(1,1,1) : Convert.ToDateTime(dr["DateBegin"]),//Дата начала,Время начала,
                        DateEnd = dr["DateEnd"] == Convert.DBNull ? new DateTime(1, 1, 1) : Convert.ToDateTime(dr["DateEnd"]),//Дата окончания,Время окончания
                        pmRead = dr["dodo"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["dodo"]),
                        pmUpdate = dr["dodo"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["dodo"]),
                        pagecount = count
                    });


                    y++;
                }
            }
            return iList;
        }







        public List<DiagnosticsOrder> GetItemListOrders(DateTime DateBegin, DateTime DateEnd, string Opticname)
        {
            List<DiagnosticsOrder> iList = new List<DiagnosticsOrder>();
            string Opt_NameFilter = "";//Оптика,
            string DateBeginFilter = "";//Дата начала,Время начала,
            string DateEndFilter = "";//Дата окончания,Время окончания


           
            if (Opticname!=null && Opticname.Length > 0)
            {
                Opt_NameFilter = " AND OpticName IN (";

                int y = 0;
                foreach (string tt in Opticname.Split('|'))
                {
                    if (y > 0)
                        Opt_NameFilter += ", ";
                    Opt_NameFilter += "'" + tt + "'";
                    y++;
                }
                Opt_NameFilter += ") ";
            }



                if (DateBegin != null && DateBegin != new DateTime(1, 1, 1))
                {
                    DateBeginFilter = " AND DateBegin >= ";

                    DateBeginFilter += "'" + DateBegin.ToString("yyyy-MM-dd HH:mm:ss") + "'";

                }
                else
            {
                DateBeginFilter = "";
            }

                if (DateEnd != null && DateEnd != new DateTime(1, 1, 1))
                {
                DateTime gg = new DateTime(DateEnd.Year, DateEnd.Month, DateEnd.Day).AddDays(1).AddMilliseconds(-1);
                    DateEndFilter = " AND DATEEND <= ";
                    DateEndFilter += "'" + gg.ToString("yyyy-MM-dd HH:mm:ss") + "'";

                }
                else
                DateEndFilter = "";




            string query = @"SELECT [RegionName] ,[TMNAME] ,[KASName] ,[Client_INN] ,[Client_FIO] ,[ID_Optic] ,[OpticName] ,[TownName] ,[FiasAddress] ,[DeliverAddress]
 ,[CodeMindBox] ,[Certif] ,[SKUName] ,[SKU_ID] ,[Nomenkl] ,[SKUAmount] ,[ResultName] ,[Reason] ,[DateBegin] ,[DateEnd] ,[monthpart] ,[quoterpart] ,[yearpart] ,[mobilePhone], Address, AccessCode
  FROM [dbo].[ORDER_REPORT_VIEW]  WHERE 1 = 1  " + restrict + Opt_NameFilter + DateBeginFilter + DateEndFilter;
            DataTable dt = dbc.Make_Query(query);
            count = dt.Rows.Count;
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new DiagnosticsOrder
                    {
                        RegionName = dr["RegionName"] == Convert.DBNull ? "" : Convert.ToString(dr["RegionName"]),//Код Сертификата,
                        TMNAME = dr["TMNAME"] == Convert.DBNull ? "" : Convert.ToString(dr["TMNAME"]),//Код Сертификата,
                        KASName = dr["KASName"] == Convert.DBNull ? "" : Convert.ToString(dr["KASName"]),//Код Сертификата,
                        Client_INN = dr["Client_INN"] == Convert.DBNull ? "" : Convert.ToString(dr["Client_INN"]),//Код Сертификата,
                        Client_FIO = dr["Client_FIO"] == Convert.DBNull ? "" : Convert.ToString(dr["Client_FIO"]),//Код Сертификата,
                        OpticName = dr["OpticName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticName"]),//Код Сертификата,
                        TownName = dr["TownName"] == Convert.DBNull ? "" : Convert.ToString(dr["TownName"]),//Код Сертификата,
                        FiasAddress = dr["Address"] == Convert.DBNull ? "" : Convert.ToString(dr["Address"]),//Код Сертификата,
                        DeliverAddress = dr["DeliverAddress"] == Convert.DBNull ? "" : Convert.ToString(dr["DeliverAddress"]),//Код Сертификата,
                        Certif = dr["Certif"] == Convert.DBNull ? "" : Convert.ToString(dr["Certif"]),//Код Сертификата,
                        SKUName = dr["SKUName"] == Convert.DBNull ? "" : Convert.ToString(dr["SKUName"]),//Код Сертификата,
                        SKU_ID = dr["SKU_ID"] == Convert.DBNull ? "" : Convert.ToString(dr["SKU_ID"]),//Код Сертификата,
                        Nomenkl = dr["Nomenkl"] == Convert.DBNull ? "" : Convert.ToString(dr["Nomenkl"]),//Код Сертификата,
                        SKUAmount = dr["SKUAmount"] == Convert.DBNull ? -1 : Convert.ToSingle(dr["SKUAmount"]),
                        ResultName = dr["ResultName"] == Convert.DBNull ? "" : Convert.ToString(dr["ResultName"]),//Код Сертификата,
                        Reason = dr["Reason"] == Convert.DBNull ? "" : Convert.ToString(dr["Reason"]),//Код Сертификата,
                        mobilePhone = dr["mobilePhone"] == Convert.DBNull ? "" : Convert.ToString(dr["mobilePhone"]),//Код Сертификата,
                        DateEnd = dr["DateEnd"] == Convert.DBNull ? new DateTime(1, 1, 1) : Convert.ToDateTime(dr["DateEnd"]),//Дата окончания,Время окончания
                        monthpart = dr["monthpart"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["monthpart"]),
                        quoterpart = dr["quoterpart"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["quoterpart"]),
                        yearpart = dr["yearpart"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["yearpart"]),
                        CodeMindBox = dr["CodeMindBox"] == Convert.DBNull ? "" : Convert.ToString(dr["CodeMindBox"]),//Код Сертификата,
                        AccessCode = dr["AccessCode"] == Convert.DBNull ? "" : Convert.ToString(dr["AccessCode"]),
                        pagecount = count
                    });


                    y++;
                }
            }
            return iList;
        }































        public DiagnosticsModel GetItem(string id)
        {
            List<DiagnosticsModel> iList = new List<DiagnosticsModel>();
            string query = @"SELECT [ID_Diagnostic]      ,[ID_Sertificat]      ,[CodeMindBox]      ,[id_opt_net]      ,[Opt_Net_Name]      ,[ID_Client_Int]      ,[Client_FIO]
      ,[ID_Doctor]      ,[DoctorName]      ,[DateBegin]      ,[DateEnd], ID_Optic, OpticName ,dodo,cr.pmUpdate  FROM [dbo].[diagnostictsView] dv
INNER JOIN [dbo].[fn_GetCanRead] ('', 'diagnostics') cr on ID_Diagnostic = cr.id  WHERE pmRead = 1 " + restrict + " AND [ID_Diagnostic] = " + id;
           DataTable dt = dbc.Make_Query(query);
            count = dt.Rows.Count;
            foreach (DataRow dr in dt.Rows)
            {
                iList.Add(new DiagnosticsModel
                {
                    ID_Diagnostic = dr["ID_Diagnostic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Diagnostic"]),
                    ID_Sertificat = dr["ID_Sertificat"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Sertificat"]),
                    CodeMindBox = dr["CodeMindBox"] == Convert.DBNull ? "" : Convert.ToString(dr["CodeMindBox"]),//Код Сертификата,
                    id_opt_net = dr["id_opt_net"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["id_opt_net"]),//Сеть Оптик,
                    Opt_Net_Name = dr["Opt_Net_Name"] == Convert.DBNull ? "" : Convert.ToString(dr["Opt_Net_Name"]),//Оптика,
                    id_opt = dr["ID_Optic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Optic"]),//Сеть Оптик,
                    Opt_Name = dr["OpticName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticName"]),//Оптика,
                    ID_Client_Int = dr["ID_Client_Int"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Client_Int"]),
                    Client_FIO = dr["Client_FIO"] == Convert.DBNull ? "" : Convert.ToString(dr["Client_FIO"]),//Имя Клиента,
                    ID_Doctor = dr["ID_Doctor"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Doctor"]),
                    DoctorName = dr["DoctorName"] == Convert.DBNull ? "" : Convert.ToString(dr["DoctorName"]),//Имя Врача,
                    DateBegin = dr["DateBegin"] == Convert.DBNull ? new DateTime(1, 1, 1) : Convert.ToDateTime(dr["DateBegin"]),//Дата начала,Время начала,
                    DateEnd = dr["DateEnd"] == Convert.DBNull ? new DateTime(1, 1, 1) : Convert.ToDateTime(dr["DateEnd"]),//Дата окончания,Время окончания
                    pmRead = dr["dodo"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["dodo"]),
                    pmUpdate = dr["dodo"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["dodo"]),
                    pagecount = count
                });
                

            }
            if (iList.Count > 0)
                return iList[0];
            else
                return null;
        }

        public List<string> GetDistinctList(string itemj)
        {
            List<string> iList = new List<string>();

            if (itemj.ToLower() == "opt_name")
                itemj = "OpticName";

            string query = @"SELECT DISTINCT " + itemj + @" DD  FROM [dbo].[diagnostictsView] dv
INNER JOIN [dbo].[fn_GetCanRead] ('', 'diagnostics') cr on ID_Diagnostic = cr.id  WHERE pmRead = 1  ";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(
                      Convert.ToString(dr["DD"]));


                    y++;
                }
            }
            return iList;
        }

        public List<Reasons> GetDistinctReasonsList()
        {
            List<Reasons> iList = new List<Reasons>();



            string query = @"SELECT [ID],so.ObjectName
  FROM [dbo].[SimpleObjects] so
  WHERE ObjectCat IN
  (SELECT [ID]  FROM [dbo].[categories] WHERE CatName = 'OutgoingReason') ";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new Reasons {
                        ID = Convert.ToInt32(dr["ID"]),
                        ObjectName =   Convert.ToString(dr["ObjectName"])
                    });


                    y++;
                }
            }
            return iList;
        }


        public bool DeleteItem(string id)
        {
            try
            {
                List<DiagnosticsModel> iList = new List<DiagnosticsModel>();
                string query = @"DELETE  FROM [dbo].[diagnosticts] WHERE [ID_Diagnostic] = " + id;
                DataTable dt = dbc.Make_Query(query);
                if(dbc.last_error.Trim().Length == 0)
                return true;
                else
                    return false;
            }
            catch 
            { 
                return false; 
            }
        }




        public bool InsertItem(DiagnosticsModel mod)
        {
            try
            {
                List<DiagnosticsModel> iList = new List<DiagnosticsModel>();
                DateTime DateBegin1 = mod.DateBegin;
                DateTime DateEnd1 = mod.DateEnd;
                string query = @"INSERT INTO [dbo].[diagnostics]
           ([ID_Sertificat], [Code], [DateBegin], [DateEnd])
     VALUES
           (" + mod.ID_Sertificat + @",'" + mod.CodeMindBox;
                if (DateBegin1 == null || DateBegin1 == new DateTime(1, 1, 1))
                    query += @"', CURRENT_TIMESTAMP,";
                else 
                    query += @"', '" + mod.DateBegin.ToString("yyyy-MM-dd HH:mm:ss") + "',";

                if (DateEnd1 == null || DateEnd1 == new DateTime(1,1,1))
                    query += "CURRENT_TIMESTAMP)";
                else
                    query += "'" + mod.DateEnd.ToString("yyyy-MM-dd HH:mm:ss") + "')";
                if (mod.id_opt > 0 && mod.ID_Sertificat>0)
                {
                    query += @";
UPDATE [dbo].[MindBox_Certificates]
   SET [ID_Optic] = " + mod.id_opt.ToString();
                    if (mod.ID_Doctor > 0)
                        query += " , ID_User = " + mod.ID_Doctor.ToString();
                    query += " WHERE ID_Sertificat = " + mod.ID_Sertificat.ToString() + ";"; 
                }

                DataTable dt = dbc.Make_Query(query);

                return true;
            }
            catch
            {
                return false;
            }
        }


        public bool UpdateItem(DiagnosticsModel mod)
        {
            try
            {
                List<DiagnosticsModel> iList = new List<DiagnosticsModel>();
                string query = @"UPDATE [dbo].[diagnostics]
           SET [ID_Sertificat] = " + mod.ID_Sertificat + @",
 [Code] = '" + mod.CodeMindBox + @"'";
                if(mod.DateBegin != null && mod.DateBegin != new DateTime(1,1,1))
                query += ",[DateBegin] = '" + mod.DateBegin.ToString("yyyy-MM-dd HH:mm:ss") + @"'";
                if (mod.DateEnd != null && mod.DateEnd != new DateTime(1, 1, 1))
                    query += ", [DateEnd] = '" + mod.DateEnd.ToString("yyyy-MM-dd HH:mm:ss") + @"'
   WHERE ID_Diagnostic = " + mod.ID_Diagnostic.ToString();
                DataTable dt = dbc.Make_Query(query);
                if(dbc.last_error.Trim().Length == 0) 
                    return true;
            }
            catch
            {
                return false;
            }
            return false;
        }
















    }
}
