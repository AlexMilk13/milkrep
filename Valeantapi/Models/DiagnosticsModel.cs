﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Valeantapi.Models
{
    public class DiagnosticsModel
    {
        public int ID_Diagnostic{ get; set; }
        public int ID_Sertificat{ get; set; }
        public string CodeMindBox{ get; set; }//Код Сертификата,
        public int id_opt_net{ get; set; }//Сеть Оптик,
        public string Opt_Net_Name{ get; set; }//Оптика Net,
        public int id_opt{ get; set; }//Оптикa,
        public string Opt_Name{ get; set; }//Оптика,
        public int ID_Client_Int{ get; set; }
        public string Client_FIO{ get; set; }//Имя Клиента,
        public int ID_Doctor{ get; set; }
        public string DoctorName{ get; set; }//Имя Врача,
        public DateTime DateBegin{ get; set; }//Дата начала,Время начала,
        public DateTime DateEnd{ get; set; }//Дата окончания,Время окончания
        public int pmUpdate{ get; set; }
        public int pmRead{ get; set; }
        public int pagecount { get; set; }
    }





    public class DiagnosticsOrder
    {
      public string RegionName { get; set; }
      public string TMNAME { get; set; }
      public string KASName { get; set; }
      public string Client_INN { get; set; }
      public string Client_FIO { get; set; }
      public string OpticName { get; set; }
      public string TownName { get; set; }
      public string FiasAddress { get; set; }
      public string DeliverAddress { get; set; }
      public string CodeMindBox { get; set; }
      public string Certif { get; set; }
      public string SKUName { get; set; }
      public string SKU_ID { get; set; }
      public string Nomenkl { get; set; }
     public float SKUAmount { get; set; }
      public string ResultName { get; set; }
      public string Reason { get; set; }
      public DateTime DateEnd { get; set; }
      public int monthpart { get; set; }
      public int quoterpart { get; set; }
      public int yearpart { get; set; }
      public string mobilePhone { get; set; }
    public string AccessCode { get; set; }
        public int pagecount { get; set; }
    }
}
