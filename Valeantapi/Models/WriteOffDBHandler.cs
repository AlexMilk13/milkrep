﻿using System;
using System.Collections.Generic;
using System.Data;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class WriteOffDBHandler
    {

        public DbClass dbc = new DbClass();
        public int count = 10;
        public int page;
        public string sort = "Outgoing_Date";
        public string order = "DESC";
        public string OpticName = "";
        public string NetworkName = "";
        public string DocName = "";
        public string ObjectName = "";//причина списания
        public string Outgoing_Date = "";
        public string SKU = "";
        public string Diopter = "";
        public string Amount = "";
        public string ID_Diagnostic = "";
        public UserInfoModel ui = new UserInfoModel();
        public int userrole = 0;
        

   


        public List<WriteOffModel> GetItemList()
        {

            string OpticNamefilter = "";
            string NetworkNamefilter = "";
            string ObjectNamefilter = "";
            string DocNamefilter = "";
            string Outgoing_Datefilter = "";
            string SKUfilter = "";
            string Diopterfilter = "";
            string Amountfilter = "";
            string ID_Diagnosticfilter = "";
        //string IsActivefilter = "";
        List<WriteOffModel> iList = new List<WriteOffModel>();

            if (ID_Diagnostic.Length > 0)
            {
                ID_Diagnosticfilter = " AND ID_Diagnostic IN (";

                int y = 0;
                foreach (string tt in OpticName.Split('|'))
                {
                    if (y > 0)
                        ID_Diagnosticfilter += ", ";
                    ID_Diagnosticfilter += tt;
                    y++;
                }
                ID_Diagnosticfilter += ") ";
            }

            if (OpticName.Length > 0)
            {
                OpticNamefilter = " AND OpticName IN (";

                int y = 0;
                foreach (string tt in OpticName.Split('|'))
                {
                    if (y > 0)
                        OpticNamefilter += ", ";
                    OpticNamefilter += "'" + tt + "'";
                    y++;
                }
                OpticNamefilter += ") ";
            }


            if (DocName.Length > 0)
            {
                DocNamefilter = " AND DocName IN (";

                int y = 0;
                foreach (string tt in DocName.Split('|'))
                {
                    if (y > 0)
                        DocNamefilter += ", ";
                    DocNamefilter += "'" + tt + "'";
                    y++;
                }
                DocNamefilter += ") ";
            }


            if (NetworkName.Length > 0)
            {
                NetworkNamefilter = " AND NetworkName IN (";

                int y = 0;
                foreach (string tt in NetworkName.Split('|'))
                {
                    if (y > 0)
                        NetworkNamefilter += ", ";
                    NetworkNamefilter += "'" + tt + "'";
                    y++;
                }
                NetworkNamefilter += ") ";
            }

            if (ObjectName.Length > 0)
            {
                ObjectNamefilter = " AND ObjectName IN (";

                int y = 0;
                foreach (string tt in ObjectName.Split('|'))
                {
                    if (y > 0)
                        ObjectNamefilter += ", ";
                    ObjectNamefilter += "'" + tt + "'";
                    y++;
                }
                ObjectNamefilter += ") ";
            }
            /////////////////////////////
            if (Outgoing_Date.Length > 0)
            {
                Outgoing_Datefilter = " AND CAST(Outgoing_Date AS DATE) IN (";

                int y = 0;
                foreach (string tt in Outgoing_Date.Split('|'))
                {
                    DateTime tmpdate;
                    if (DateTime.TryParse(tt, out tmpdate))
                    {
                        DateTime.TryParse(tt, out tmpdate);
                        if (y > 0)
                            Outgoing_Datefilter += ", ";
                        Outgoing_Datefilter += "CAST('" + tmpdate.ToString("yyyy-MM-dd HH:mm:ss") + "' AS DATE)";
                        //
                    y++;
                    }
                }
                Outgoing_Datefilter += ") ";
            }

            if (SKU.Length > 0)
            {
                SKUfilter = @" AND ID_Outgoing IN (SELECT ID_Outgoing FROM dbo.outgoin_details WHERE ID_Sku IN
  (SELECT ID_Sku FROM dbo.sku WHERE [Description] IN(";

                int y = 0;
                foreach (string tt in SKU.Split('|'))
                {
                    if (y > 0)
                        SKUfilter += ", ";
                    SKUfilter += "'" + tt + "'";
                    y++;
                }
                SKUfilter += "))) ";
            }

            if (Diopter.Length > 0)
            {
                Diopterfilter = " AND ID_Outgoing IN (SELECT ID_Outgoing FROM dbo.outgoin_details WHERE Diopter IN (";

                int y = 0;
                foreach (string tt in Diopter.Split('|'))
                {
                    if (y > 0)
                        Diopterfilter += ", ";
                    Diopterfilter +=  tt.Replace(",",".") ;
                    y++;
                }
                Diopterfilter += ")) ";
            }

            if (Amount.Length > 0)
            {
                Amountfilter = " Amount IN  (";

                int y = 0;
                foreach (string tt in Amount.Split('|'))
                {
                    if (y > 0)
                        Amountfilter += ", ";
                    Amountfilter += tt;
                    y++;
                }
                Amountfilter += ") ";
            }

            string sorter = " ORDER BY ID_Outgoing ";
            if (sort != null && sort.Trim().Length > 0)
            {
                sorter = " ORDER BY " + sort + " ";
                if (order != null && order.Trim().Length > 0)
                    sorter += order;
            }
            string offset = "";
            string idoptic = "";
            if (ui.id_Optic > 0)
                idoptic = " AND [ID_Optic] = " +  ui.id_Optic.ToString() + " ";
            offset = "  OFFSET " + ((page - 1) * count).ToString() + " ROWS FETCH NEXT " + count.ToString() + " ROWS ONLY";
            string query = @"SELECT 1  FROM [dbo].[OutgoingsView]  WHERE 1=1 
 " + idoptic + OpticNamefilter + NetworkNamefilter + Amountfilter + Diopterfilter + SKUfilter + Outgoing_Datefilter + ObjectNamefilter + ID_Diagnosticfilter;
            DataTable dt = dbc.Make_Query(query);
            count = dt.Rows.Count;
            query = @" SELECT [ID_Outgoing]      ,[ID_Optic]      ,[OpticName]      ,[ID_Network]      ,[NetworkName]      ,[ID_OutgoinType]      ,[ID_Diagnostic]      ,[Amount]
      ,[Outgoing_Date]      ,[Description]      ,[WriteOffReason]      ,[ObjectName]      ,[id_Doc], DocName, dodo  FROM [dbo].[OutgoingsView] WHERE 1=1 
 " + idoptic + ID_Diagnosticfilter + OpticNamefilter + NetworkNamefilter + Amountfilter + Diopterfilter + SKUfilter + Outgoing_Datefilter + ObjectNamefilter + DocNamefilter + sorter + offset;
            dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new WriteOffModel
                    {
                        ID_Outgoing = dr["ID_Outgoing"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Outgoing"]),
                        ID_Optic = dr["ID_Optic"] ==  Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Optic"]),
                        OpticName = dr["OpticName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticName"]),
                        ID_Network = dr["ID_Network"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Network"]),
                        NetworkName = dr["NetworkName"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkName"]),
                        Description = dr["Description"] == Convert.DBNull ? "" : Convert.ToString(dr["Description"]),
                        ObjectName = dr["ObjectName"] == Convert.DBNull ? "" : Convert.ToString(dr["ObjectName"]),
                        WriteOffReason = dr["WriteOffReason"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["WriteOffReason"]),
                        Outgoing_Date = dr["Outgoing_Date"] == Convert.DBNull ? DateTime.Now : Convert.ToDateTime(dr["Outgoing_Date"]),
                        IDDoc = dr["ID_Outgoing"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Outgoing"]),
                        DocName = dr["DocName"] == Convert.DBNull ? "" : Convert.ToString(dr["DocName"]),
                        pmRead = dr["dodo"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["dodo"]),
                        pmUpdate = dr["dodo"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["dodo"]),
                        pagecount = count
                    });
                    

                }
            }
            return iList;
        }






        ////////////////////////////////////////////////
        public List<WriteOfDiagfModel> GetItemDiagList()
        {

            string OpticNamefilter = "";
            string NetworkNamefilter = "";
            string ObjectNamefilter = "";
            string Outgoing_Datefilter = "";
            string SKUfilter = "";
            string Diopterfilter = "";
            string Amountfilter = "";
            string ID_Diagnosticfilter = "";
            List<WriteOfDiagfModel> iList = new List<WriteOfDiagfModel>();
            if (ID_Diagnostic.Length > 0)
            {
                ID_Diagnosticfilter = " AND ID_Diagnostic IN (";

                int y = 0;
                foreach (string tt in ID_Diagnostic.Split('|'))
                {
                    if (y > 0)
                        ID_Diagnosticfilter += ", ";
                    ID_Diagnosticfilter += tt;
                    y++;
                }
                ID_Diagnosticfilter += ") ";
            }
            if (OpticName.Length > 0)
            {
                OpticNamefilter = " AND OpticName IN (";

                int y = 0;
                foreach (string tt in OpticName.Split('|'))
                {
                    if (y > 0)
                        OpticNamefilter += ", ";
                    OpticNamefilter += "'" + tt + "'";
                    y++;
                }
                OpticNamefilter += ") ";
            }

            if (NetworkName.Length > 0)
            {
                NetworkNamefilter = " AND NetworkName IN (";

                int y = 0;
                foreach (string tt in NetworkName.Split('|'))
                {
                    if (y > 0)
                        NetworkNamefilter += ", ";
                    NetworkNamefilter += "'" + tt + "'";
                    y++;
                }
                NetworkNamefilter += ") ";
            }

            if (ObjectName.Length > 0)
            {
                ObjectNamefilter = " AND ObjectName IN (";

                int y = 0;
                foreach (string tt in ObjectName.Split('|'))
                {
                    if (y > 0)
                        ObjectNamefilter += ", ";
                    ObjectNamefilter += "'" + tt + "'";
                    y++;
                }
                ObjectNamefilter += ") ";
            }
            /////////////////////////////
            if (Outgoing_Date.Length > 0)
            {
                Outgoing_Datefilter = " AND Outgoing_Date IN (";

                int y = 0;
                foreach (string tt in Outgoing_Date.Split('|'))
                {
                    DateTime tmpdate;
                    if (DateTime.TryParse(tt, out tmpdate))
                    {
                        DateTime.TryParse(tt, out tmpdate);
                        if (y > 0)
                            Outgoing_Datefilter += ", ";
                        Outgoing_Datefilter += "'" + tmpdate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                        //
                        y++;
                    }
                }
                Outgoing_Datefilter += ") ";
            }

            if (SKU.Length > 0)
            {
                SKUfilter = @" AND ID_Outgoing IN (SELECT ID_Outgoing FROM dbo.outgoin_details WHERE ID_Sku IN
  (SELECT ID_Sku FROM dbo.sku WHERE SkuName IN(";

                int y = 0;
                foreach (string tt in SKU.Split('|'))
                {
                    if (y > 0)
                        SKUfilter += ", ";
                    SKUfilter += "'" + tt + "'";
                    y++;
                }
                SKUfilter += "))) ";
            }

            if (Diopter.Length > 0)
            {
                Diopterfilter = " AND ID_Outgoing IN (SELECT ID_Outgoing FROM dbo.outgoin_details WHERE Diopter IN (";

                int y = 0;
                foreach (string tt in Diopter.Split('|'))
                {
                    if (y > 0)
                        Diopterfilter += ", ";
                    Diopterfilter += tt.Replace(",", ".");
                    y++;
                }
                Diopterfilter += ")) ";
            }

            if (Amount.Length > 0)
            {
                Amountfilter = " Amount IN  (";

                int y = 0;
                foreach (string tt in Amount.Split('|'))
                {
                    if (y > 0)
                        Amountfilter += ", ";
                    Amountfilter += tt;
                    y++;
                }
                Amountfilter += ") ";
            }



            string sorter = " ORDER BY ID_Outgoing ";
            if (sort != null && sort.Trim().Length > 0)
            {
                sorter = " ORDER BY " + sort + " ";
                if (order != null && order.Trim().Length > 0)
                    sorter += order;
            }
            string offset = "";
            string idoptic = "";
            if (ui.id_Optic > 0)
                idoptic = " AND [ID_Optic] = " + ui.id_Optic.ToString() + " ";
            offset = "  OFFSET " + ((page - 1) * count).ToString() + " ROWS FETCH NEXT " + count.ToString() + " ROWS ONLY";
            string query = @"SELECT 1  FROM [dbo].OutgoingsViewDiag  WHERE 1=1 
 " + idoptic + OpticNamefilter + NetworkNamefilter + Amountfilter + Diopterfilter + SKUfilter + Outgoing_Datefilter + ObjectNamefilter + ID_Diagnosticfilter;
            DataTable dt = dbc.Make_Query(query);
            count = dt.Rows.Count;
            query = @" SELECT [ID_Outgoing]      ,[ID_Optic]      ,[OpticName]      ,[ID_Network]      ,[NetworkName]      ,[ID_OutgoinType]      ,[ID_Diagnostic]      ,[Amount]
      ,[Outgoing_Date]      ,[Description]      ,[WriteOffReason]      ,[ObjectName]      ,[id_Doc], DocName  FROM [dbo].[OutgoingsViewDiag] WHERE 1=1 
 " + idoptic + ID_Diagnosticfilter + OpticNamefilter + NetworkNamefilter + Amountfilter + Diopterfilter + SKUfilter + Outgoing_Datefilter + ObjectNamefilter + sorter + offset;
            dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new WriteOfDiagfModel
                    {
                        ID_Outgoing = dr["ID_Outgoing"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Outgoing"]),
                        ID_Optic = dr["ID_Optic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Optic"]),
                        OpticName = dr["OpticName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticName"]),
                        ID_Network = dr["ID_Network"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Network"]),
                        NetworkName = dr["NetworkName"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkName"]),
                        Description = dr["Description"] == Convert.DBNull ? "" : Convert.ToString(dr["Description"]),
                        ObjectName = dr["ObjectName"] == Convert.DBNull ? "" : Convert.ToString(dr["ObjectName"]),
                        WriteOffReason = dr["WriteOffReason"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["WriteOffReason"]),
                        Outgoing_Date = dr["Outgoing_Date"] == Convert.DBNull ? DateTime.Now : Convert.ToDateTime(dr["Outgoing_Date"]),
                        IDDoc = dr["ID_Outgoing"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Outgoing"]),
                        DocName = dr["DocName"] == Convert.DBNull ? "" : Convert.ToString(dr["DocName"]),
                        ID_Diagnostic = dr["ID_Diagnostic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Diagnostic"]),
                        pagecount = count
                    });

                    iList[iList.Count - 1].WriteOffSKU = new List<SKUItemDiag>();
                    iList[iList.Count - 1].WriteOffSKU = GetSKUListDiag(iList[iList.Count - 1].ID_Outgoing);
                    y++;
                }
            }
            return iList;
        }





        public List<SKUItem> GetSKUList(int id)
        {
            List<SKUItem> iList = new List<SKUItem>();

            string query = @"SELECT [IDSKU]      ,[SKUName]      ,[SKUAmount]      ,[Diopter]
  FROM [dbo].[OutgoingSKUView] WHERE [ID_Outgoing] = " + id.ToString();
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new SKUItem
                    {
                        IDSKU = dr["IDSKU"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["IDSKU"]),
                        SKUName = dr["SKUName"] == Convert.DBNull ? "" : Convert.ToString(dr["SKUName"]),
                        SKUAmount = dr["SKUAmount"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["SKUAmount"]),
                        Diopter = dr["Diopter"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["Diopter"])
                        
                    });
                    y++;
                } 
            }
            return iList;
        }

        public List<SKUItemDiag> GetSKUListDiag(int id)
        {
            List<SKUItemDiag> iList = new List<SKUItemDiag>();

            string query = @"SELECT [ID_Outgoing]      ,[IDSKU]      ,[SKUName]      ,[SKUAmount]      ,[Diopter]      ,[ID_Result]      ,[ResultName]
  FROM [dbo].[OutgoingSKUView] WHERE [ID_Outgoing] = " + id.ToString();
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new SKUItemDiag
                    {
                        ID_Outgoing = dr["ID_Outgoing"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Outgoing"]),
                        IDSKU = dr["IDSKU"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["IDSKU"]),
                        SKUName = dr["SKUName"] == Convert.DBNull ? "" : Convert.ToString(dr["SKUName"]),
                        SKUAmount = dr["SKUAmount"] == Convert.DBNull ? -1 : Convert.ToSingle(dr["SKUAmount"]),
                        Diopter = dr["Diopter"] == Convert.DBNull ? -1 : Convert.ToSingle(dr["Diopter"]),
                        ID_Result = dr["ID_Result"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Result"]),
                        ResultName = dr["ResultName"] == Convert.DBNull ? "" : Convert.ToString(dr["ResultName"])
                    });
                    y++;
                }
            }
            return iList;
        }



        public List<string> GetDistinctList(string itemj)
        {
            List<string> iList = new List<string>();

           
            if (itemj.ToUpper() == "AMOUNT") itemj = "Amount";
            if (itemj.ToUpper() == "DIOPTER") itemj = "Diopter";
            string query = @"SELECT DISTINCT " + itemj + @" AS DD  FROM [dbo].[OutgoingsCommonView] ";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(
                      Convert.ToString(dr["DD"]));


                    y++;
                }
            }
            return iList;
        }


        public List<Reasons> GetReasonsList()
        {
            List<Reasons> iList = new List<Reasons>();

           
            
            string query = @"SELECT [ID]
      ,[ObjectName]     
  FROM [dbo].[SimpleObjects]
  WHERE ObjectCat IN
  (SELECT [ID]
  FROM [dbo].[categories] WHERE [CatName] = 'OutgoingReason')";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new Reasons
                    {
                        ID = dr["ID"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID"]),
                        ObjectName =  dr["ObjectName"] == Convert.DBNull ? "" : Convert.ToString(dr["ObjectName"])
                    })
                        ;
                }
            }
            return iList;
        }

        public List<Results> GetResultsList()
        {
            List<Results> iList = new List<Results>();



            string query = @"SELECT [ID]
      ,[ObjectName]     
  FROM [dbo].[SimpleObjects]
  WHERE ObjectCat IN
  (SELECT [ID]
  FROM [dbo].[categories] WHERE [CatName] = 'Результат подбора')";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new Results
                    {
                        ID = dr["ID"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID"]),
                        ObjectName = dr["ObjectName"] == Convert.DBNull ? "" : Convert.ToString(dr["ObjectName"])
                    })
                        ;
                }
            }
            return iList;
        }




        public WriteOffModel GetItem(string id)
        {


            WriteOffModel iList = new WriteOffModel();


            string query = @"SELECT [ID_Outgoing]      ,[ID_Optic]      ,[OpticName]      ,[ID_Network]      ,[NetworkName]      ,[ID_OutgoinType]      ,[ID_Diagnostic]      ,[Amount]
      ,[Outgoing_Date]      ,[Description]      ,[WriteOffReason]      ,[ObjectName]      ,[id_Doc], DocName, dodo  FROM [dbo].[OutgoingsView] WHERE 1=1  AND [ID_Outgoing] = " + id;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    iList = (new WriteOffModel
                    {
                        ID_Outgoing = dr["ID_Outgoing"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Outgoing"]),
                        ID_Optic = dr["ID_Optic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Optic"]),
                        OpticName = dr["OpticName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticName"]),
                        ID_Network = dr["ID_Network"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Network"]),
                        NetworkName = dr["NetworkName"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkName"]),
                        Description = dr["Description"] == Convert.DBNull ? "" : Convert.ToString(dr["Description"]),
                        ObjectName = dr["ObjectName"] == Convert.DBNull ? "" : Convert.ToString(dr["ObjectName"]),
                        WriteOffReason = dr["WriteOffReason"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["WriteOffReason"]),
                        Outgoing_Date = dr["Outgoing_Date"] == Convert.DBNull ? DateTime.Now : Convert.ToDateTime(dr["Outgoing_Date"]),
                        IDDoc = dr["ID_Outgoing"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Outgoing"]),
                        DocName = dr["DocName"] == Convert.DBNull ? "" : Convert.ToString(dr["DocName"]),
                        pmRead = dr["dodo"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["dodo"]),
                        pmUpdate = dr["dodo"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["dodo"]),
                        pagecount = dt.Rows.Count
                    });

                }

            }


            query = @"SELECT *  FROM [dbo].[OutgoingSKUView] WHERE 1=1  AND [ID_Outgoing] = " + id;
            dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0 && dt.Rows.Count>0)
            {
                iList.WriteOffSKU = new List<SKUItem>();
                foreach (DataRow dr in dt.Rows)
                {
                    iList.WriteOffSKU.Add (new SKUItem
                    {
                      IDSKU = dr["IDSKU"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["IDSKU"]),
                      SKUName = dr["SKUName"] == Convert.DBNull ? "" : Convert.ToString(dr["SKUName"]),
                      SKUAmount = dr["SKUAmount"] == Convert.DBNull ? -1 : Convert.ToSingle(dr["SKUAmount"]),
                      Diopter = dr["Diopter"] == Convert.DBNull ? -1 : Convert.ToSingle(dr["Diopter"])
                    });

                }
            }
            return iList;
        }


        public WriteOfDiagfModel GetItemDiag(string id)
        {


            WriteOfDiagfModel iList = new WriteOfDiagfModel();


            string query = @"SELECT [ID_Outgoing]      ,[ID_Optic]      ,[OpticName]      ,[ID_Network]      ,[NetworkName]      ,[ID_OutgoinType]      ,[ID_Diagnostic]      ,[Amount]
      ,[Outgoing_Date]      ,[Description]      ,[WriteOffReason]      ,[ObjectName]      ,[id_Doc], DocName  FROM [dbo].[OutgoingsViewDiag] WHERE 1=1  AND [ID_Outgoing] = " + id;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    iList = (new WriteOfDiagfModel
                    {
                        ID_Outgoing = dr["ID_Outgoing"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Outgoing"]),
                        ID_Optic = dr["ID_Optic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Optic"]),
                        OpticName = dr["OpticName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticName"]),
                        ID_Network = dr["ID_Network"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Network"]),
                        NetworkName = dr["NetworkName"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkName"]),
                        Description = dr["Description"] == Convert.DBNull ? "" : Convert.ToString(dr["Description"]),
                        ObjectName = dr["ObjectName"] == Convert.DBNull ? "" : Convert.ToString(dr["ObjectName"]),
                        WriteOffReason = dr["WriteOffReason"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["WriteOffReason"]),
                        Outgoing_Date = dr["Outgoing_Date"] == Convert.DBNull ? DateTime.Now : Convert.ToDateTime(dr["Outgoing_Date"]),
                        IDDoc = dr["ID_Outgoing"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Outgoing"]),
                        DocName = dr["DocName"] == Convert.DBNull ? "" : Convert.ToString(dr["DocName"]),
                        ID_Diagnostic = dr["[ID_Diagnostic]"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["[ID_Diagnostic]"]),
                        pagecount = dt.Rows.Count
                    });

                }
            }


            query = @"SELECT *  FROM [dbo].[OutgoingSKUView] WHERE 1=1  AND [ID_Outgoing] = " + id;
            dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0 && dt.Rows.Count > 0)
            {
                iList.WriteOffSKU = new List<SKUItemDiag>();
                foreach (DataRow dr in dt.Rows)
                {
                    iList.WriteOffSKU.Add(new SKUItemDiag
                    {
                        ID_Outgoing = dr["ID_Outgoing"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Outgoing"]),
                        IDSKU = dr["IDSKU"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["IDSKU"]),
                        SKUName = dr["SKUName"] == Convert.DBNull ? "" : Convert.ToString(dr["SKUName"]),
                        SKUAmount = dr["SKUAmount"] == Convert.DBNull ? -1 : Convert.ToSingle(dr["SKUAmount"]),
                        Diopter = dr["Diopter"] == Convert.DBNull ? -1 : Convert.ToSingle(dr["Diopter"]),
                        ID_Result = dr["ID_Result"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Result"]),
                        ResultName = dr["ResultName"] == Convert.DBNull ? "" : Convert.ToString(dr["ResultName"])
                    });

                }
            }
            return iList;
        }


        public bool InsertItem(WriteOffModel id)
        {
            try
            {
                DateTime huy = DateTime.Now;
                if (id.Outgoing_Date != null && id.Outgoing_Date != new DateTime(1, 1, 1))
                    huy = id.Outgoing_Date;
                string query = @"INSERT INTO [dbo].[outgoings] ([ID_Optic] ,[ID_OutgoinType],[Outgoing_Date] ,[WriteOffReason] ,[id_Doc]) 
OUTPUT Inserted.ID_Outgoing
SELECT " + id.ID_Optic + ",(SELECT [ID] FROM [dbo].[SimpleObjects] WHERE OBJECTNAME = 'Списание без диагностики'), '" + huy.ToString("yyyy-MM-dd HH:mm:ss") + "', " + id.WriteOffReason + "," + id.IDDoc;
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    if (id.ID_Outgoing == null || id.ID_Outgoing <= 0)
                    {
                        string yy = dt.Rows[0][0].ToString();
                        CleanSKUItems(yy);
                        foreach (SKUItem it in id.WriteOffSKU)
                    { InsertSKUItem(yy, it); 
                        }
                        return true;
                    }
                   
                    return true;
                }
                return false;
            }
            catch { return false; }
        }


        public bool InsertItemDiag(WriteOfDiagfModel id)
        {
            try
            {
                DateTime huy = DateTime.Now;
                if (id.Outgoing_Date != null && id.Outgoing_Date != new DateTime(1, 1, 1))
                    huy = id.Outgoing_Date;
                string query = @"INSERT INTO [dbo].[outgoings] ([ID_OutgoinType],[Outgoing_Date],WriteOffReason ,ID_Diagnostic, ID_Optic) 
OUTPUT Inserted.ID_Outgoing
SELECT (SELECT [ID] FROM [dbo].[SimpleObjects] WHERE OBJECTNAME = 'Списание с диагностикой' AND ObjectCat IN (SELECT [ID]
  FROM [dbo].[categories]
  WHERE CatName = 'Вид диагностики')), '" + huy.ToString("yyyy-MM-dd HH:mm:ss") + "', null," + id.ID_Diagnostic;
                query += @" , (SELECT TOP 1 [ID_Optic]    
  FROM [dbo].[MindBox_Certificates]
  WHERE ID_Sertificat IN
  (SELECT [ID_Sertificat]      
  FROM [dbo].[diagnostics] WHERE ID_Diagnostic = " + id.ID_Diagnostic + ") )";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    if (id.ID_Outgoing == null || id.ID_Outgoing <= 0)
                    {
                        string yy = dt.Rows[0][0].ToString();
                        CleanSKUItems(yy);
                        foreach (SKUItemDiag it in id.WriteOffSKU)
                        {
                            InsertSKUItemDiag(yy, it);
                        }
                        return true;
                    }

                    return true;
                }
                return false;
            }
            catch { return false; }
        }



        public bool UpdateItem(WriteOffModel id)
        {
            try
            {
                DateTime huy = DateTime.Now;
                if (id.Outgoing_Date != null && id.Outgoing_Date != new DateTime(1, 1, 1))
                    huy = id.Outgoing_Date;
                string query = @"UPDATE [dbo].[outgoings] SET [ID_Optic] = " + id.ID_Optic + " ,[ID_OutgoinType] = (SELECT [ID] FROM [dbo].[SimpleObjects] WHERE OBJECTNAME = 'Списание без диагностики'),[Outgoing_Date] = '" + huy.ToString("yyyy-MM-dd HH:mm:ss") + @"' ,[WriteOffReason] = "
                    + id.WriteOffReason + " ,[id_Doc] = " + id.IDDoc + " WHERE [ID_Outgoing] = " + id.ID_Outgoing.ToString();
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    CleanSKUItems(id.ID_Outgoing.ToString());
                    foreach (SKUItem it in id.WriteOffSKU)
                    {
                        InsertSKUItem(id.ID_Outgoing.ToString(), it);
                    }
                        return true;
                }
                return false;
            }
            catch { return false; }
        }

        public bool UpdateItemDiag(WriteOfDiagfModel id)
        {
            try
            {
                //[ID_OutgoinType],[Outgoing_Date],WriteOffReason ,ID_Diagnostic
                DateTime huy = DateTime.Now;
                if (id.Outgoing_Date != null && id.Outgoing_Date != new DateTime(1, 1, 1))
                    huy = id.Outgoing_Date;
                string query = @"UPDATE [dbo].[outgoings] SET [ID_Optic] = 
(SELECT TOP 1 [ID_Optic]    
  FROM [dbo].[MindBox_Certificates]
  WHERE ID_Sertificat IN
  (SELECT [ID_Sertificat]      
  FROM [dbo].[diagnostics] WHERE ID_Diagnostic = " + id.ID_Diagnostic + @") )
,[ID_OutgoinType] = (SELECT [ID] FROM [dbo].[SimpleObjects] WHERE OBJECTNAME = 'Списание с диагностикой' AND ObjectCat IN (SELECT [ID]
  FROM[dbo].[categories]
  WHERE CatName = 'Вид диагностики')),[Outgoing_Date] = '" + huy.ToString("yyyy-MM-dd HH:mm:ss") + "' ,[WriteOffReason] = NULL ,[id_Doc] = " + id.IDDoc + " WHERE [ID_Outgoing] = " + id.ID_Outgoing.ToString();
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    CleanSKUItems(id.ID_Outgoing.ToString());
                    foreach (SKUItemDiag it in id.WriteOffSKU)
                    {
                        InsertSKUItemDiag(id.ID_Outgoing.ToString(), it);
                    }
                    return true;
                }
                return false;
            }
            catch { return false; }
        }

        public bool CleanSKUItems(string ido)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[outgoin_details] WHERE [ID_Outgoing] = " + ido.ToString();
                
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;

            }
            catch { return false; }
        }


        public bool InsertSKUItem(string ido, SKUItem id)
        {
            try
            {
                string query = @";
INSERT INTO [dbo].[outgoin_details]
           ([ID_Outgoing] ,[ID_Sku] ,[Amount] ,[Diopter])
     VALUES
           (" + ido.ToString() + @"," + id.IDSKU.ToString() + ", " + id.SKUAmount.ToString().Replace(",",".") + ", " + id.Diopter.ToString().Replace(",", ".") + ")";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;
            }
            catch { return false; }
        }


        public bool InsertSKUItemDiag(string ido, SKUItemDiag id)
        {
            try
            {
                string query = @";
INSERT INTO [dbo].[outgoin_details]
           ([ID_Outgoing] ,[ID_Sku] ,[Amount] ,[Diopter],ID_Result)
     VALUES
           (" + ido.ToString() + @"," + id.IDSKU.ToString() + ", " + id.SKUAmount.ToString().Replace(",", ".") + ", " + id.Diopter.ToString().Replace(",", ".");
               query += "," + id.ID_Result.ToString() + ")";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;
            }
            catch { return false; }
        }


        public bool DeleteItem(int id)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[outgoin_details]
      WHERE [ID_Outgoing] = " + id.ToString() + "; DELETE FROM [dbo].[outgoings] WHERE [ID_Outgoing] = " + id.ToString();
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;
            }
            catch { return false; }
        }
        




    }
}
