﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class WriteOffModel
    {

        public int ID_Outgoing { get; set; }
       public int ID_Optic { get; set; }
	  public string OpticName { get; set; }
      public int ID_Network { get; set; }
      public string NetworkName { get; set; }
      public DateTime Outgoing_Date { get; set; }
      public string Description { get; set; }
      public int WriteOffReason { get; set; }
        public string ObjectName { get; set; }
        public int pagecount { get; set; }
        public int IDDoc { get; set; }
        public string DocName { get; set; }
        public List<SKUItem> WriteOffSKU { get; set; }
        public int pmUpdate { get; set; }
        public int pmRead { get; set; }
    }

    public class WriteOfDiagfModel
    {

        public int ID_Outgoing { get; set; }
        public int ID_Optic { get; set; }
        public string OpticName { get; set; }
        public int ID_Network { get; set; }
        public string NetworkName { get; set; }
        public DateTime Outgoing_Date { get; set; }
        public string Description { get; set; }
        public int WriteOffReason { get; set; }
        public string ObjectName { get; set; }
        public int pagecount { get; set; }
        public int IDDoc { get; set; }
        public string DocName { get; set; }
        public List<SKUItemDiag> WriteOffSKU { get; set; }
        public int ID_Diagnostic { get; set; }
    }

    public class SKUItemDiag
    {
        public int IDSKU { get; set; }
        public string SKUName { get; set; }
        public float SKUAmount { get; set; }
        public Single Diopter { get; set; }
        public int ID_Outgoing { get; set; }
        public int ID_Result { get; set; }
        public string ResultName { get; set; }
    }

    public class Reasons
    {
        public int ID { get; set; }
        public string ObjectName { get; set; }
    }

    public class Results
    {
        public int ID { get; set; }
        public string ObjectName { get; set; }
    }

    public class SKUItem
    {
        public int IDSKU { get; set; }
        public string SKUName { get; set; }
        public float SKUAmount { get; set; }
        public Single Diopter { get; set; }
    }
}
