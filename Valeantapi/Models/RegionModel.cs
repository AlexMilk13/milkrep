﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class RegionModel
    {
        public int ID_Region { get; set; }
        public string Code { get; set; }
        public string RegionName { get; set; }
        public int RegionIndex { get; set; }
    }
}
