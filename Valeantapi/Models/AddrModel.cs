﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class AddrModel
    {
        public int ID_Address { get; set; }
        public string Address_Name { get; set; }
        public string Fias_Code { get; set; }
    }
}
