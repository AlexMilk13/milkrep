﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class NetworksDBHandler
    {

        public DbClass dbc = new DbClass();
        public int count = 0;
        public int page = 0;
        public string INN = "";
        public string NetworkName = "";
        public string sort = "ID_Network ";
        public string order = "DESC";
        public string IsActive = "";
        public string UserID = "";
        public int level = 0;
        public bool insertItem(NetworksModel id)
        {
            try
            {
                string ff = id.IsActive ? "1" : "0";
                string query = @"INSERT INTO [dbo].[networks] ([Code]           ,[NetworkName]           ,[IsActive])
OUTPUT Inserted.ID_Network
     VALUES
           ('" + id.Code + @"' , '" + id.NetworkName + @"'," + ff + ")";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                else return false;
            }
            catch { return false; }
        }




        public string [] GetDistinctNetworks(string itemj)
        {
            string[] networks = new string[0];
            try { 
            string query = @"SELECT DISTINCT " + itemj + "   FROM [dbo].[networks] WHERE 1=1 ";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                    Array.Resize(ref networks, networks.Length + 1);
                    networks[networks.Length - 1] = dr[0].ToString();
                    }
                }
            }
            catch { }
            return networks;
        }




        public List<NetworksModel> GetNetworks()
        {
            string NetworkNamefilter = "";
            string INNfilter = "";
            string IsActivefilter = "";
            List<NetworksModel> users = new List<NetworksModel>();

            if (NetworkName.Length > 0)
            {
                NetworkNamefilter = " AND NetworkName IN (";

                int y = 0;
                foreach (string tt in NetworkName.Split('|'))
                {
                    if (y > 0)
                        NetworkNamefilter += ", ";
                    NetworkNamefilter += "'" + tt + "'";
                    y++;
                }
                NetworkNamefilter += ") ";
            }

            if (INN.Length > 0)
            {
                INNfilter = " AND Code IN (";

                int y = 0;
                foreach (string tt in INN.Split('|'))
                {
                    if (y > 0)
                        INNfilter += ", ";
                    INNfilter += "'" + tt + "'";
                    y++;
                }
                INNfilter += ") ";
            }

            string IsKas = "";
            if (level < 1000 && level >= 10)
            { IsKas = " AND ID_Network IN (SELECT oo.ID_Network  FROM [dbo].[optics] oo WHERE oo.ID_KAS IN (" + UserID + ") AND oo.IsActive = 1)"; }

            if (IsActive.Length > 0)
            {
                IsActivefilter = @" AND [IsActive] = " + IsActive;
            }
            try
            {
                string sorter = " ORDER BY ID_Network ";
                sorter += order;
                if (sort != null && sort.Trim().Length > 0)
                {
                    sorter = " ORDER BY " + sort + " ";
                    if (order != null && order.Trim().Length > 0)
                        sorter += order;
                }
                string offset = "";

                string query = @"SELECT count(*)
  FROM [dbo].[networks] WHERE 1=1 " + NetworkNamefilter + IsActivefilter + INNfilter;
                DataTable dt = dbc.Make_Query(query);

                int yyy = Convert.ToInt32(dt.Rows[0][0]);

                string networks = "";
                if(UserID != null && UserID.Length> 0 && level>100)
                {
                    networks = " AND [ID_Network] IN (SELECT oo.[ID_Network]  FROM[dbo].[optics] oo WHERE oo.[ID_User] = " + UserID + ") ";
                }
                offset = "  OFFSET " + ((page - 1) * count).ToString() + " ROWS FETCH NEXT " + count.ToString() + " ROWS ONLY";
                query = @"SELECT [ID_Network]      ,[Code]      ,[NetworkName]      ,[IsActive], INN
  FROM [dbo].[networks] WHERE 1=1 " + NetworkNamefilter + IsActivefilter + INNfilter + networks + IsKas + sorter + offset;

                dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        users.Add(new NetworksModel
                        {
                            ID_Network = Convert.ToInt32(dr["ID_Network"]),
                            IsActive = Convert.ToBoolean(dr["IsActive"]),
                            NetworkName = Convert.ToString(dr["NetworkName"]),
                           // INN = Convert.ToString(dr["INN"]),
                            Code = Convert.ToString(dr["Code"]),
                            pagecount = yyy
                        }) ;
                    }
                }
            }
            catch { }
            return users;
        }

        public List<NetworksModel> GetNetworks_doc(int id)
        {
           
            List<NetworksModel> users = new List<NetworksModel>();

            try { 
                string query = @"SELECT count(*)
  FROM [dbo].[networks] nw WHERE 1=1 
  AND nw.ID_Network IN (SELECT ID_Network FROM dbo.networks_users uu WHERE uu.ID_User = " + id.ToString() + ")";
                DataTable dt = dbc.Make_Query(query);

                int yyy = Convert.ToInt32(dt.Rows[0][0]);


                
                query = @"SELECT [ID_Network]      ,[Code]      ,[NetworkName]      ,[IsActive], INN
  FROM [dbo].[networks] nw WHERE 1=1 AND nw.ID_Network IN (SELECT ID_Network FROM dbo.networks_users uu WHERE uu.ID_User = " + id.ToString() + ") ORDER BY nw.[NetworkName]";

                dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        users.Add(new NetworksModel
                        {
                            ID_Network = Convert.ToInt32(dr["ID_Network"]),
                            IsActive = Convert.ToBoolean(dr["IsActive"]),
                            NetworkName = Convert.ToString(dr["NetworkName"]),
                            Code = Convert.ToString(dr["Code"]),
                            pagecount = yyy
                        });
                    }
                }
            }
            catch { }
            return users;
        }






        public List<NetworksModel> GetNetworksExcel()
        {
            string NetworkNamefilter = "";
            string INNfilter = "";
            string IsActivefilter = "";
            List<NetworksModel> users = new List<NetworksModel>();

            if (NetworkName.Length > 0)
            {
                NetworkNamefilter = " AND NetworkName IN (";

                int y = 0;
                foreach (string tt in NetworkName.Split('|'))
                {
                    if (y > 0)
                        NetworkNamefilter += ", ";
                    NetworkNamefilter += "'" + tt + "'";
                    y++;
                }
                NetworkNamefilter += ") ";
            }

            if (INN.Length > 0)
            {
                INNfilter = " AND Code IN (";

                int y = 0;
                foreach (string tt in INN.Split('|'))
                {
                    if (y > 0)
                        INNfilter += ", ";
                    INNfilter += "'" + tt + "'";
                    y++;
                }
                INNfilter += ") ";
            }



            if (IsActive.Length > 0)
            {
                IsActivefilter = @" AND [IsActive] = " + IsActive;
            }
            try
            {
                string sorter = " ORDER BY ID_Network ";
                if (sort != null && sort.Trim().Length > 0)
                {
                    sorter = " ORDER BY " + sort + " ";
                    if (order != null && order.Trim().Length > 0)
                        sorter += order;
                }
                string offset = "";

                string query = @"SELECT count(*)
  FROM [dbo].[networks] WHERE 1=1 " + NetworkNamefilter + IsActivefilter + INNfilter;
                DataTable dt = dbc.Make_Query(query);

                int yyy = Convert.ToInt32(dt.Rows[0][0]);


                offset = "  OFFSET " + ((page - 1) * count).ToString() + " ROWS FETCH NEXT " + count.ToString() + " ROWS ONLY";
                query = @"SELECT [ID_Network]      ,[Code]      ,[NetworkName]      ,[IsActive], INN
  FROM [dbo].[networks] WHERE 1=1 " + NetworkNamefilter + IsActivefilter + INNfilter + sorter;

                dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        users.Add(new NetworksModel
                        {
                            ID_Network = Convert.ToInt32(dr["ID_Network"]),
                            IsActive = Convert.ToBoolean(dr["IsActive"]),
                            NetworkName = Convert.ToString(dr["NetworkName"]),
                            Code = Convert.ToString(dr["Code"]),
                            //INN = Convert.ToString(dr["INN"]),
                            pagecount = yyy
                        });
                    }
                }
            }
            catch { }
            return users;
        }









        public NetworksModel GetOneNetwork(string Code)
        {
            NetworksModel users = new NetworksModel();
            try
            {
                string query = @"SELECT [ID_Network]      ,[Code]      ,[NetworkName]      ,[IsActive], INN
  FROM [dbo].[networks]
WHERE [ID_Network] = " + Code + "";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        users = (new NetworksModel
                        {
                            ID_Network = Convert.ToInt32(dr["ID_Network"]),
                            IsActive = Convert.ToBoolean(dr["IsActive"]),
                            NetworkName = Convert.ToString(dr["NetworkName"]),
                            Code = Convert.ToString(dr["Code"]),
                           // INN = Convert.ToString(dr["INN"]),
                            pagecount = dt.Rows.Count
                        });
                    }
                }
            }
            catch { }
            return users;
        }


        public bool DeleteNetwork(string id)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[networks] WHERE [ID_Network] = " + id + @";";

                dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                else return false;
            }
            catch { return false; }

        }


        public bool UpdateNetwork(NetworksModel id)
        {            
            try
            {
                int y = 0;
                y = id.IsActive ? 1 : 0;
                string query = @"UPDATE [dbo].[networks]
SET [Code] = '" + id.Code + @"', [INN] = '" + id.INN + @"'      ,[NetworkName] = '" + id.NetworkName + @"'      ,[IsActive] = " + y.ToString() + @" WHERE [ID_Network] = " + id.ID_Network.ToString();

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
            }
            catch { return false; }
            return false;
        }



        public bool NewCheckDeleteNetwork(NetworksModel id)
        {
            {
                string query = @"SELECT 1  FROM [dbo].[optics] WHERE [ID_Network] = " + id.ID_Network.ToString();
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null)
                {
                    if (dt.Rows.Count > 0)
                        return false;
                    else
                        return true;
                }
            }
            return false;
        }


    }
}
