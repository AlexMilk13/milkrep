﻿using System.Collections.Generic;

namespace Valeantapi.Models
{
    public class OpticModel
    {
        public int ID_optic { get; set; }
        public int ID_Network { get; set; }
        public string NetworkName { get; set; }
        public string NetworkINN { get; set; }
        public string OpticTorgName { get; set; }
        public string OpticJuraName { get; set; }
        public string OpticINN { get; set; }
        public int ID_User { get; set; }
        public string UserName { get; set; }
        public int ID_KAS { get; set; }
        public string KASName { get; set; }
        public int ID_Region { get; set; }
        public string RegionName { get; set; }
        public int ID_Town { get; set; }
        public string TownName { get; set; }
        public int ID_TownBlock { get; set; }
        public string TownBlockName { get; set; }
        public int ID_SubTown { get; set; }
        public string SubTownName { get; set; }
        public int ID_SubSubTown { get; set; }
        public string SubSubTownName { get; set; }
        public int ID_Street { get; set; }
        public string StreetName { get; set; }
        public string House{ get; set; }
        public string Korpus { get; set; }
        public string Phone{ get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
        public string OnlineLink { get; set; }
        public bool Kids { get; set; }
        public bool Sms{ get; set; }
        public bool Digital{ get; set; }
        public int id_Doc1 { get; set; }
        public int id_Doc2 { get; set; }
        public int id_Doc3 { get; set; }
        public string Doc1 { get; set; }
        public string Doc2 { get; set; }
        public string Doc3 { get; set; }
        public float Latitude{ get; set; }
        public float Longitude{ get; set; }
        public bool IsActive{ get; set; }
        public string AccessCode { get; set; }

        public List<OpticDoctors> Doctors;
        public int pagecount { get; set; }
        public string Address { get; set; }
        public string FiasAddress { get; set; }
        public string DeliverAddress { get; set; }
        public string ClientCode { get; set; }
        public string BrickVC { get; set; }
        public bool ShowOnCard { get; set; }
        public string Email { get; set; }
        public string KASEmail { get; set; }
        public string TMEmail { get; set; }
        public string Metro { get; set; }
    }


    public class OpticModelPost
    {
        public string NetworkName { get; set; }
        public string NetworkINN { get; set; }
        public string OpticTorgName { get; set; }
        public string OpticJuraName { get; set; }
        public string OpticINN { get; set; }
        public string UserName { get; set; }
        public string KASName { get; set; }
        public string RegionName { get; set; }
        public string TownName { get; set; }
        public int ID_TownBlock { get; set; }
        public string TownBlockName { get; set; }
        public string SubTownName { get; set; }
        public int ID_SubSubTown { get; set; }
        public string SubSubTownName { get; set; }
        public string StreetName { get; set; }
        public string House { get; set; }
        public string Korpus { get; set; }
        public string Phone { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
        public string OnlineLink { get; set; }
        public string Kids { get; set; }
        public string Sms { get; set; }
        public string Digital { get; set; }
        public string Doc1 { get; set; }
        public string Doc2 { get; set; }
        public string Doc3 { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string IsActive { get; set; }
        public string AccessCode { get; set; }
        public string Address { get; set; }
        public string FiasAddress { get; set; }
        public string DeliverAddress { get; set; }
        public string ClientCode { get; set; }
        public string BrickVC { get; set; }
        public string ShowOnCard { get; set; }
        public string Email { get; set; }
        public string KASEmail { get; set; }
        public string TMEmail { get; set; }
        public string Metro { get; set; }
        public int count { get; set; }
        public int page { get; set; }
        public string sort { get; set; }
        public string order { get; set; }
        public OpticModelPost()
        {
            count = 10;
            page = 1;
            sort = "ID_Optic";
            order = "Desc";
            Kids = null;
            IsActive = null;
            Sms = null;
        }
    }
    public class OpticDoctors
    {
        public int id_Optic { get; set; }
        public int ID_Doc { get; set; }
        public string DocName { get; set; }
    }

    public class NetworksDoctors
    {
        public int id_Network { get; set; }
        public int ID_Doc { get; set; }
        public string DocName { get; set; }
    }


    public class Doctors
    {
        public int ID_Doc { get; set; }
        public string DocName { get; set; }

    }

    public class OpticDoctorsFull
    {
        public int id_Optic { get; set; }
        public string OpticName { get; set; }
        public string OpticAddress { get; set; }
        public int ID_Doc { get; set; }
        public string DocName { get; set; }
        public string DocSurname { get; set; }
        public string DocPatronymic { get; set; }
        public bool IsActive { get; set; }
        public int pagecount { get; set; }
    }


    public class Address_struct
    { }


}
