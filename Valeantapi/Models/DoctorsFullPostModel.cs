﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class DoctorsFullPostModel
    {
        public int count { get; set; } 
        public int page { get; set; }
        public string OpticName { get; set; }
        public string OpticAddress { get; set; }
        public string DocName { get; set; }
        public string DocSurname { get; set; }
        public string DocPatronymic { get; set; }
        public string IsActive { get; set; }
        public string sort { get; set; }
        public string order { get; set; }

        public DoctorsFullPostModel()
        {
            count = 10;
            page = 1;
            OpticName = "";
            OpticAddress = "";
            DocName = "";
            DocSurname = "";
            DocPatronymic = "";
            IsActive = "";
            sort = "ID_User";
            order = "DESC";
        }

    }

    public class DoctorsFullNetworkModel
    {
        public int count { get; set; }
        public int page { get; set; }
        public string NetworkName { get; set; }
        public string DocName { get; set; }
        public string DocSurname { get; set; }
        public string DocPatronymic { get; set; }
        public string IsActive { get; set; }
        public string sort { get; set; }
        public string order { get; set; }

        public DoctorsFullNetworkModel()
        {
            count = 10;
            page = 1;
            NetworkName = "";
         
            DocName = "";
            DocSurname = "";
            DocPatronymic = "";
            IsActive = "";
            sort = "ID_User";
            order = "DESC";
        }

    }
}
