﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class MatchingReportModel
    {

        public string KamKas { get; set; }
        public string Code { get; set; }
        public string NetworkName { get; set; }
        public string OpticJuraName { get; set; }
        public string RegionName { get; set; }
        public string Brick { get; set; }
        public string Town { get; set; }
        public string OpticAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Nomenklatur { get; set; }
        public string Quantity { get; set; }
        public string Reason { get; set; }
        public string EndDate { get; set; }
        public string MonthName { get; set; }
        public string CertSMS { get; set; }
        public int pagecount { get; set; }
        public List<MatchingReportSKUModel> WriteOffSKU { get;set;}

        public string DocName { get; set; }
        public int ID_Diagnostic { get; set; }

        /*KAS/KAM
       Код(Номер сертификата или SMS)
       Сеть Оптик
       Юридическое название Оптики
       Субъект РФ
       Брик VC
       Город(Оптика)
       Адрес Оптики
       Номер телефона клиента(пациента)
       Номенклатура
       Расход, pk
       Причина расхода
       Дата окончания подбора
       Месяц окончания подбора
       Сертификат или SMS(перечислимый тип)
       SKU
       ФИО врача
       */
    }

    public class MatchingReportSKUModel
    {
        public int IDSKU { get; set; }
        public string SKUName { get; set; }
    }
    }
