﻿using System;
using System.Collections.Generic;
using System.Data;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class DoctorsFullDBHandler
    {
        public DbClass dbc = new DbClass();
        public int count = 10;
        public int page;
        public string OpticName = "";
        public string OpticAddress = "";
        public string DocName = "";
        public string DocSurname = "";
        public string DocPatronymic = "";
        public string IsActive = "";
        public string sort = "ID_Optic";
        public string order = "ASC";


        public List<OpticDoctorsFull> GetItemList()
        {

            string OpticNamefilter = "";
            string OpticAddressfilter = "";
            string DocNamefilter = "";
            string DocSurnamefilter = "";
            string DocPatronymicfilter = "";
            string IsActivefilter = "";
            List<OpticDoctorsFull> iList = new List<OpticDoctorsFull>();

            if (OpticName.Length > 0)
            {
                OpticNamefilter = " AND OpticName IN (";

                int y = 0;
                foreach (string tt in OpticName.Split('|'))
                {
                    if (y > 0)
                        OpticNamefilter += ", ";
                    OpticNamefilter += "'" + tt + "'";
                    y++;
                }
                OpticNamefilter += ") ";
            }

            if (OpticAddress.Length > 0)
            {
                OpticAddressfilter = " AND Address IN (";

                int y = 0;
                foreach (string tt in OpticAddress.Split('|'))
                {
                    if (y > 0)
                        OpticAddressfilter += ", ";
                    OpticAddressfilter += "'" + tt + "'";
                    y++;
                }
                OpticAddressfilter += ") ";
            }

            if (DocName.Length > 0)
            {
                DocNamefilter = " AND Name IN (";

                int y = 0;
                foreach (string tt in DocName.Split('|'))
                {
                    if (y > 0)
                        DocNamefilter += ", ";
                    DocNamefilter += "'" + tt + "'";
                    y++;
                }
                DocNamefilter += ") ";
            }
            /////////////////////////////
            if (DocSurname.Length > 0)
            {
                DocSurnamefilter = " AND Surname IN (";

                int y = 0;
                foreach (string tt in DocSurname.Split('|'))
                {
                    if (y > 0)
                        DocSurnamefilter += ", ";
                    DocSurnamefilter += "'" + tt + "'";
                    y++;
                }
                DocSurnamefilter += ") ";
            }

            if (DocPatronymic.Length > 0)
            {
                DocPatronymicfilter = " AND Patronymic IN (";

                int y = 0;
                foreach (string tt in DocPatronymic.Split('|'))
                {
                    if (y > 0)
                        DocPatronymicfilter += ", ";
                    DocPatronymicfilter += "'" + tt + "'";
                    y++;
                }
                DocPatronymicfilter += ") ";
            }

            if (IsActive.Length > 0)
            {
                IsActivefilter = @" AND [IsActive] = " + IsActive;
            }
            if (sort.ToLower() == "docname") sort = "[Name]";
            if (sort.ToLower() == "docsurname") sort = "[Surname]";
            if (sort.ToLower() == "docpatronymic") sort = "[Patronymic]";
            if (sort.ToLower() == "opticaddress") sort = "[Address]";
            if (sort.ToLower() == "id_doc") sort = "[ID_User]";
            string sorter = " ORDER BY ID_Optic ";
            if (sort != null && sort.Trim().Length > 0)
            {
                sorter = " ORDER BY " + sort + " ";
                if (order != null && order.Trim().Length > 0)
                    sorter += order;
            }
            string offset = "";

            offset = "  OFFSET " + ((page - 1) * count).ToString() + " ROWS FETCH NEXT " + count.ToString() + " ROWS ONLY";

            string query = @"SELECT DISTINCT [ID_User] ,[Name] ,[Surname] ,[Patronymic] ,[IsActive]
  FROM [dbo].[DoctorsView]
  INNER JOIN [dbo].[fn_GetPermissions]('', 'optics') rts on rts.pmRead = 1 WHERE 1=1  " + OpticNamefilter + OpticAddressfilter + DocNamefilter + DocSurnamefilter + DocPatronymicfilter + IsActivefilter + sorter;
            
            DataTable dt = dbc.Make_Query(query);
            int yy = dt.Rows.Count;

            query = @"SELECT DISTINCT [ID_User] ,[Name] ,[Surname] ,[Patronymic] ,[IsActive]  -- ,[ID_Optic] ,[OpticName] ,[Address]
  FROM [dbo].[DoctorsView]
  INNER JOIN [dbo].[fn_GetPermissions]('', 'optics') rts on rts.pmRead = 1 WHERE 1=1 " + OpticNamefilter + OpticAddressfilter + DocNamefilter + DocSurnamefilter + DocPatronymicfilter + IsActivefilter + sorter + offset;
            dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new OpticDoctorsFull
                    {
                        //id_Optic = dr["ID_Optic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Optic"]),
                        ID_Doc = dr["ID_User"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_User"]),
                        //OpticName = dr["OpticName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticName"]),
                        DocName = dr["Name"] == Convert.DBNull ? "" : Convert.ToString(dr["Name"]),
                        DocSurname = dr["Surname"] == Convert.DBNull ? "" : Convert.ToString(dr["Surname"]),
                        DocPatronymic = dr["Patronymic"] == Convert.DBNull ? "" : Convert.ToString(dr["Patronymic"]),
                        IsActive = dr["IsActive"] == Convert.DBNull ? false : Convert.ToBoolean(dr["IsActive"]),
                        //OpticAddress = dr["Address"] == Convert.DBNull ? "" : Convert.ToString(dr["Address"]),
                        pagecount = yy
                    });


                    y++;
                }
            }
            return iList;
        }


        public List<OpticDoctorsFull> GetItemListExcel()
        {

            string OpticNamefilter = "";
            string OpticAddressfilter = "";
            string DocNamefilter = "";
            string DocSurnamefilter = "";
            string DocPatronymicfilter = "";
            string IsActivefilter = "";
            List<OpticDoctorsFull> iList = new List<OpticDoctorsFull>();

            if (OpticName.Length > 0)
            {
                OpticNamefilter = " AND OpticName IN (";

                int y = 0;
                foreach (string tt in OpticName.Split('|'))
                {
                    if (y > 0)
                        OpticNamefilter += ", ";
                    OpticNamefilter += "'" + tt + "'";
                    y++;
                }
                OpticNamefilter += ") ";
            }

            if (OpticAddress.Length > 0)
            {
                OpticAddressfilter = " AND OpticAddress IN (";

                int y = 0;
                foreach (string tt in OpticAddress.Split('|'))
                {
                    if (y > 0)
                        OpticAddressfilter += ", ";
                    OpticAddressfilter += "'" + tt + "'";
                    y++;
                }
                OpticAddressfilter += ") ";
            }

            if (DocName.Length > 0)
            {
                DocNamefilter = " AND Name IN (";

                int y = 0;
                foreach (string tt in DocName.Split('|'))
                {
                    if (y > 0)
                        DocNamefilter += ", ";
                    DocNamefilter += "'" + tt + "'";
                    y++;
                }
                DocNamefilter += ") ";
            }
            /////////////////////////////
            if (DocSurname.Length > 0)
            {
                DocSurnamefilter = " AND Surname IN (";

                int y = 0;
                foreach (string tt in DocSurname.Split('|'))
                {
                    if (y > 0)
                        DocSurnamefilter += ", ";
                    DocSurnamefilter += "'" + tt + "'";
                    y++;
                }
                DocSurnamefilter += ") ";
            }

            if (DocPatronymic.Length > 0)
            {
                DocPatronymicfilter = " AND Patronymic IN (";

                int y = 0;
                foreach (string tt in DocPatronymic.Split('|'))
                {
                    if (y > 0)
                        DocPatronymicfilter += ", ";
                    DocPatronymicfilter += "'" + tt + "'";
                    y++;
                }
                DocPatronymicfilter += ") ";
            }

            if (IsActive.Length > 0)
            {
                IsActivefilter = @" AND [IsActive] = " + IsActive;
            }
            if (sort.ToLower() == "docname") sort = "[Name]";
            if (sort.ToLower() == "docsurname") sort = "[Surname]";
            if (sort.ToLower() == "docpatronymic") sort = "[Patronymic]";
            if (sort.ToLower() == "opticaddress") sort = "[Address]";
            string sorter = " ORDER BY ID_User ";
            if (sort != null && sort.Trim().Length > 0)
            {
                sorter = " ORDER BY " + sort + " ";
                if (order != null && order.Trim().Length > 0)
                    sorter += order;
            }
            
            string query = @"SELECT DISTINCT [ID_User] ,[Name] ,[Surname] ,[Patronymic] ,[IsActive]  -- ,[ID_Optic] ,[OpticName] ,[Address]
  FROM [dbo].[DoctorsView]
  INNER JOIN [dbo].[fn_GetPermissions]('', 'optics') rts on rts.pmRead = 1 WHERE 1=1  " + OpticNamefilter + OpticAddressfilter + DocNamefilter + DocSurnamefilter + DocPatronymicfilter + IsActivefilter + sorter;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new OpticDoctorsFull
                    {
                        //id_Optic = dr["ID_Optic"] == Convert.DBNull ? 0 : Convert.ToInt32(dr["ID_Optic"]),
                        ID_Doc = dr["ID_User"] == Convert.DBNull ? 0 : Convert.ToInt32(dr["ID_User"]),
                        //OpticName = dr["OpticName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticName"]),
                        DocName = dr["Name"] == Convert.DBNull ? "" : Convert.ToString(dr["Name"]),
                        DocSurname = dr["Surname"] == Convert.DBNull ? "" : Convert.ToString(dr["Surname"]),
                        DocPatronymic = dr["Patronymic"] == Convert.DBNull ? "" : Convert.ToString(dr["Patronymic"]),
                        IsActive = dr["IsActive"] == Convert.DBNull ? false : Convert.ToBoolean(dr["IsActive"]),
                        //OpticAddress = dr["Address"] == Convert.DBNull ? "" : Convert.ToString(dr["Address"])
                    });


                    y++;
                }
            }
            return iList;
        }



        public List<string> GetDistinctList(string itemj)
        {
            List<string> iList = new List<string>();
            string sort = "";
            if (itemj.ToLower() == "docname") sort = "[Name]";
            else if (itemj.ToLower() == "docsurname") sort = "[Surname]";
            else if (itemj.ToLower() == "docpatronymic") sort = "[Patronymic]";
            else if (itemj.ToLower() == "opticaddress") sort = "[Address]";
            else sort = itemj;
            string query = @"SELECT DISTINCT " + sort + @" AS DD FROM
  [dbo].[DoctorsView]
  WHERE 1=1 ";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(
                      Convert.ToString(dr["DD"]));


                    y++;
                }
            }
            return iList;
        }




        public OpticDoctorsFull GetItem(string id)
        {

           
           OpticDoctorsFull iList = new OpticDoctorsFull();

           
            string query = @"SELECT [ID_User] ,[Name] ,[Surname] ,[Patronymic] ,[IsActive]   ,[ID_Optic] ,[OpticName] ,[Address]
  FROM [dbo].[DoctorsView]
 WHERE 1=1 AND  [ID_User] = " + id;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    iList = (new OpticDoctorsFull
                    {
                        id_Optic = Convert.ToInt32(dr["ID_Optic"]),
                        ID_Doc = Convert.ToInt32(dr["ID_User"]),
                        OpticName = Convert.ToString(dr["OpticName"]),
                        DocName = Convert.ToString(dr["Name"]),
                        DocSurname = Convert.ToString(dr["Surname"]),
                        DocPatronymic = Convert.ToString(dr["Patronymic"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        OpticAddress = Convert.ToString(dr["Address"]),
                        pagecount = dt.Rows.Count
                    });

                }
            }
            return iList;
        }

        public bool insertItem(OpticDoctorsFull id)
        {
            int did = 0;
            string idtouse = "";
            if (id.ID_Doc > 0)
                did = id.ID_Doc;
            int yy = id.IsActive ? 1 : 0;
            string query = @"INSERT INTO[dbo].[users]
        ([Login],[IsActive],[Name],[Surname],[Patronymic], isDoc,Email)
OUTPUT Inserted.ID_User
    VALUES
          ('' ," + yy.ToString() + " ,'" + id.DocName + "', '" + id.DocSurname + "', '" + id.DocPatronymic + @"',1, '" + DateTime.Now.Ticks.ToString() + "');";
            if (did == null || did <= 0)
            {
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                    idtouse = dt.Rows[0][0].ToString();
            }
            else
            {
                query = @"UPDATE [dbo].[users]
        SET [IsActive] = " + yy.ToString() + ",[Name] = '" + id.DocName + "',[Surname] = '" + id.DocSurname + "',[Patronymic] = '" + id.DocPatronymic + @"' , isDoc = 1
WHERE ID_User = " + did.ToString();
                dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                    idtouse = did.ToString();
            }



            if (dbc.last_error.Length == 0)
            {
                query = @" DELETE FROM [dbo].[user_roles] WHERE ID_User = " + idtouse + @" AND ID_Role IN (SELECT [ID_Role]  FROM[dbo].[roles]  WHERE Code = '1KGBS4VWRMB'); ";
                dbc.Make_Query(query);
                if (dbc.last_error.Trim().Length == 0 && id.id_Optic > 0)
                {
                    OpticDBHandler iHandler = new OpticDBHandler();
                    return iHandler.AddOpticDoctor(idtouse, id.id_Optic.ToString());
                }
                else if (dbc.last_error.Trim().Length == 0 && id.id_Optic == 0)
                    return true;
            }
            
            return false;
        }


        public bool UpdateItem(OpticDoctorsFull id)
        {
            int yy = id.IsActive ? 1 : 0;
            string query = @"UPDATE [dbo].[users]
   SET [IsActive] = " + yy.ToString() + @"
      ,[Name] = '" + id.DocName + @"'
      ,[Surname] = '" + id.DocSurname + @"'
      ,[Patronymic] = '" + id.DocPatronymic + @"'
,isDoc = 1
 WHERE ID_User = " + id.ID_Doc.ToString();
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Trim().Length == 0)
            {
                OpticDBHandler iHandler = new OpticDBHandler();
                return iHandler.AddOpticDoctor(id.ID_Doc.ToString(), id.id_Optic.ToString());
            }
            return false;
        }

        public bool DeleteItem(int id, int idopt)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[optic_users] WHERE [ID_Optic] = " + idopt + " AND ID_User = " + id +
                    "; DELETE FROM [dbo].[users] WHERE isDoc = 1 AND ID_User = " + id.ToString();
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                return false;
            }
            catch { return false; }
        }


        public bool DeleteItemCheck(string id )
        {
            try
            {
                OpticModel iList = new OpticModel();
                string query = @"
IF NOT EXISTS(SELECT 1
  FROM [dbo].[diagnostics]
  WHERE ID_Sertificat IN
  (  SELECT [ID_Sertificat]  FROM [dbo].[MindBox_Certificates] WHERE ID_User = " + id + @" ))
  BEGIN
  SELECT 'true';
  END
  ELSE
  BEGIN
  SELECT 'false';
  END";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr[0].ToString() == "true")
                            return true;
                        else
                            return false;
                    }
                }
                else return false;
            }
            catch { return false; }
            return false;
        }


    }

}

