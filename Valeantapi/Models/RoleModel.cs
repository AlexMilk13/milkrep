﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class RoleModel
    {
        public int ID_Role { get; set; }
        public string Code { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
    }
}
