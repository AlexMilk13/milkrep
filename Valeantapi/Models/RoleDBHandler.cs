﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class RoleDBHandler
    {

        public DbClass dbc = new DbClass();
        public int count;
        public int page;
        public string sort = "ID_Role";
        public string order = "ASC";
       


        public List<RoleModel> GetRoles()
        {
            List<RoleModel> users = new List<RoleModel>();
            try
            {
                string query = @"SELECT [ID_Role]      ,[Code]      ,[RoleName]      ,[Description]  FROM [dbo].[roles] WHERE Code <> '1KK1XNKI7QF'";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        users.Add(new RoleModel
                        {
                            ID_Role = Convert.ToInt32(dr["ID_Role"]),
                            Description = Convert.ToString(dr["Description"]),
                            RoleName = Convert.ToString(dr["RoleName"]),
                            Code = Convert.ToString(dr["Code"])
                        });
                    }
                }
            }
            catch { }
            return users;
        }

        public RoleModel GetOneRole(string Code)
        {
            RoleModel users = new RoleModel();
            try
            {
                string query = @"SELECT [ID_Role]      ,[Code]      ,[RoleName]      ,[Description]  FROM [dbo].[roles]
WHERE [ID_Role] = " + Code + "";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        users = (new RoleModel
                        {
                            ID_Role = Convert.ToInt32(dr["ID_Role"]),
                            Description = Convert.ToString(dr["Description"]),
                            RoleName = Convert.ToString(dr["RoleName"]),
                            Code = Convert.ToString(dr["Code"])
                        });
                    }
                }
            }
            catch { }
            return users;
        }


        public bool DeleteRole(string id)
        {
            try
            {
                string query = @"DELETE FROM [dbo].[roles] WHERE [ID_Role] = " + id + @";";

                dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                else return false;
            }
            catch { return false; }

        }
    }
}
