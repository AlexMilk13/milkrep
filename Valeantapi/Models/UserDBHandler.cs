﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class UserDBHandler
    {
        public DbClass dbc = new DbClass();
        public string hash = "";
        public int count;
        public int page;
        public string sort = "ID_Optic";
        public string order = "DESC";
        public int inserted = 0;
        public string UserName = "";
        public string RoleName = "";
        public string Name = "";
        public string Surname = "";
        public string Patronymic = "";
        public string Login = "";
        public string Email = "";
        public string LastDate = "";
        public string IsActive = "";



        public List<UserModel> GetItemList()
        {

            string UserNamefilter = "";
            string RoleNamefilter = "";
            string Loginfilter = "";
            string Emailfilter = "";
            string LastDatefilter = "";
            string IsActivefilter = "";
            string Namefilter = "";
            string Surnamefilter = "";
            string Patronymicfilter = "";
            List<UserModel> iList = new List<UserModel>();

            if (UserName.Length > 0)
            {
                UserNamefilter = "AND FIO IN ("; //" AND Name + ' ' + Surname IN (";

                int y = 0;
                foreach (string tt in UserName.Split('|'))
                {
                    if (y > 0)
                        UserNamefilter += ", ";
                    UserNamefilter += "'" + tt + "'";
                    y++;
                }
                UserNamefilter += ") ";
            }

            if (Name.Length > 0)
            {
                Namefilter = "AND [Name] IN ("; //" AND Name + ' ' + Surname IN (";

                int y = 0;
                foreach (string tt in Name.Split('|'))
                {
                    if (y > 0)
                        Namefilter += ", ";
                    Namefilter += "'" + tt + "'";
                    y++;
                }
                Namefilter += ") ";
            }

            if (Surname.Length > 0)
            {
                Surnamefilter = "AND Surname IN ("; //" AND Name + ' ' + Surname IN (";

                int y = 0;
                foreach (string tt in Surname.Split('|'))
                {
                    if (y > 0)
                        Surnamefilter += ", ";
                    Surnamefilter += "'" + tt + "'";
                    y++;
                }
                Surnamefilter += ") ";
            }

            if (Patronymic.Length > 0)
            {
                Patronymicfilter = "AND Patronymic IN ("; //" AND Name + ' ' + Surname IN (";

                int y = 0;
                foreach (string tt in Patronymic.Split('|'))
                {
                    if (y > 0)
                        Patronymicfilter += ", ";
                    Patronymicfilter += "'" + tt + "'";
                    y++;
                }
                Patronymicfilter += ") ";
            }

            if (RoleName.Length > 0)
            {
                RoleNamefilter = " AND RoleName IN (";

                int y = 0;
                foreach (string tt in RoleName.Split('|'))
                {
                    if (y > 0)
                        RoleNamefilter += ", ";
                    RoleNamefilter += "'" + tt + "'";
                    y++;
                }
                RoleNamefilter += ") ";
            }

            if (Login.Length > 0)
            {
                Loginfilter = " AND Login IN (";

                int y = 0;
                foreach (string tt in Login.Split('|'))
                {
                    if (y > 0)
                        Loginfilter += ", ";
                    Loginfilter += "'" + tt + "'";
                    y++;
                }
                Loginfilter += ") ";
            }
            /////////////////////////////
            if (LastDate.Length > 0)
            {
                Emailfilter = " AND LastDate IN (";

                int y = 0;
                foreach (string tt in LastDate.Split('|'))
                {
                    DateTime tmpdate;
                    if (DateTime.TryParse(tt, out tmpdate))
                    {
                        DateTime.TryParse(tt, out tmpdate);
                        if (y > 0)
                            LastDatefilter += ", ";
                        LastDatefilter += "'" + tmpdate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                        //
                        y++;
                    }
                }
                LastDatefilter += ") ";
            }

            if (Email.Length > 0)
            {
                Emailfilter = @" AND Email IN(";

                int y = 0;
                foreach (string tt in Email.Split('|'))
                {
                    if (y > 0)
                        Emailfilter += ", ";
                    Emailfilter += "'" + tt + "'";
                    y++;
                }
                Emailfilter += ") ";
            }

            if (IsActive.Length > 0)
            {
                IsActivefilter = @" AND [IsActive] = " + IsActive;
            }



            string sorter = " ORDER BY [LastDate]  ";
            if (sort != null && sort.Trim().Length > 0)
            {
                sorter = " ORDER BY " + sort.ToLower().Replace("username", "FIO") + " ";

            }
            string offset;
            if (order != null && order.Trim().Length > 0)
                sorter += order;
            offset = "  OFFSET " + ((page - 1) * count).ToString() + " ROWS FETCH NEXT " + count.ToString() + " ROWS ONLY";
            string query = @"WITH AA AS
(
SELECT [ID_User]      ,[Login]      ,[IsActive]      ,[FIO]      ,[Surname]      ,[Email]      ,[LastDate]      ,[ID_Role]      ,[RoleName]  FROM [dbo].[UsersView]  
  )
  SELECT 
  1
  FROM AA WHERE 1 = 1 " + UserNamefilter + RoleNamefilter + Loginfilter + Emailfilter + LastDatefilter + IsActivefilter + Namefilter +
  Surnamefilter + Patronymicfilter;
            DataTable dt = dbc.Make_Query(query);
            count = dt.Rows.Count;
            query = @"WITH AA AS
(
SELECT [ID_User]      ,[Login]      ,[IsActive]      ,[FIO],[Name]      ,[Surname],[Patronymic]      ,[Email]      ,[LastDate]      ,[ID_Role]      ,[RoleName]  
FROM [dbo].[UsersView]  
 )
  SELECT 
  ID_User,	Login,	IsActive,	COALESCE([FIO],'') AS FIO ,COALESCE([Surname],'') AS Surname,COALESCE([Name],'') AS Name,COALESCE([Patronymic],'')  AS Patronymic  ,COALESCE([Email],'') AS Email ,COALESCE([LastDate], CURRENT_TIMESTAMP) AS LastDate,	ID_Role,	RoleName  
  FROM AA WHERE 1 = 1 " + UserNamefilter + RoleNamefilter + Loginfilter + Emailfilter + LastDatefilter + IsActivefilter + Namefilter +
  Surnamefilter + Patronymicfilter + sorter + offset;
            dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new UserModel
                    {
                        ID_User = Convert.ToInt32(dr["ID_User"]),
                        UserName = Convert.ToString(dr["FIO"]),
                        ID_Role = Convert.ToInt32(dr["ID_Role"]),
                        RoleName = Convert.ToString(dr["RoleName"]),
                        Login = Convert.ToString(dr["Login"]),
                        Email = Convert.ToString(dr["Email"]),
                        Name = Convert.ToString(dr["Name"]),
                        Surname = Convert.ToString(dr["Surname"]),
                        Patronymic = Convert.ToString(dr["Patronymic"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        LastDate = Convert.ToDateTime(dr["LastDate"]),
                        pagecount = count
                    });


                    y++;
                }
            }
            return iList;
        }

        ///////////--------------------------
        public UserModel GetItem(string id, bool opt)
        {
            UserModel iList = new UserModel();
            string ee = " AND Login IS NOT NULL ";
            if (opt)
                ee = " AND COALESCE(Login,'') = '' ";
            string query = @"WITH AA AS
(
SELECT [ID_User]      ,[Login]      ,[IsActive]      ,[FIO],[Name]      ,[Surname],[Patronymic]      ,[Email]      ,[LastDate]      ,[ID_Role]      ,[RoleName]  
FROM [dbo].[UsersView]  
 )
  SELECT 
  ID_User,	Login,	IsActive,	COALESCE([FIO],'') AS FIO ,COALESCE([Surname],'') AS Surname,COALESCE([Name],'') AS Name,COALESCE([Patronymic],'')  AS Patronymic ,COALESCE([Email],'') AS Email ,COALESCE([LastDate], CURRENT_TIMESTAMP) AS LastDate,	ID_Role,	RoleName  
  FROM AA WHERE 1 = 1 AND  id_User = " + id + ee;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList = (new UserModel
                    {
                        ID_User = Convert.ToInt32(dr["ID_User"]),
                        UserName = Convert.ToString(dr["FIO"]),
                        ID_Role = Convert.ToInt32(dr["ID_Role"]),
                        RoleName = Convert.ToString(dr["RoleName"]),
                        Login = Convert.ToString(dr["Login"]),
                        Email = Convert.ToString(dr["Email"]),
                        Name = Convert.ToString(dr["Name"]),
                        Surname = Convert.ToString(dr["Surname"]),
                        Patronymic = Convert.ToString(dr["Patronymic"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        LastDate = Convert.ToDateTime(dr["LastDate"]),
                        pagecount = count
                    });


                    y++;
                }
            }
            return iList;
        }







        ///////////////////////////////

        public int insertItem(UserModel id /*string sortOrder, bool asc, int pagenumber, int rows, ref int count, string searcher*/)
        {
            try
            {
                //if(id.RoleName == "Оптика")

                OpticModel iList = new OpticModel();
                string query = @"IF NOT EXISTS(SELECT 1 FROM [dbo].[users] WHERE [Login] = '" + id.Login + @"')
BEGIN
INSERT INTO [dbo].[users]
           ([Code]           ,[FIO]           ,[Login]           ,[Password]           ,[PhoneNumber]           ,[IsActive]           ,[Name]           ,[Surname]           ,[Patronymic]           ,[Email]           ,[Number])
OUTPUT Inserted.ID_User
     VALUES
           (" + id.Code + @" ,'' , " + id.Login + @",'' ,'',0 ," + id.Name + @", " + id.Surname + @", " + id.Patronymic + @"," + id.Email + @",0)
END";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    query = @"DELETE FROM [dbo].[user_roles] WHERE ID_User = " + dt.Rows[0][0].ToString() + ";   INSERT INTO [dbo].[user_roles] ([ID_User], [ID_Role])   VALUES  (" + dt.Rows[0][0].ToString() + "," + id.ID_Role + ")";
                    dt = dbc.Make_Query(query);
                    if (dbc.last_error.Length == 0)
                        return 1;
                    else
                        return 0;
                }
                else if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count == 0)
                    return 100;
                else return 0;
            }
            catch { return 0; }
        }


        public List<UserModel> GetUsersOnRole(string Code)
        {
            List<UserModel> users = new List<UserModel>();
            try
            {
                string query = @"SELECT uu.[ID_User]
      ,uu.[Code]      ,[FIO]      ,[Login]      ,[Password]      ,[PhoneNumber]      ,[IsActive]
      ,[Name]      ,[Surname]      ,[Patronymic]      ,[Email]      ,[Number]	  ,rr.ID_Role	  ,RoleName
  FROM [dbo].[users] uu
  LEFT OUTER JOIN dbo.user_roles ur ON ur.ID_User = uu.ID_user
  LEFT OUTER JOIN dbo.roles rr ON rr.ID_Role = ur.ID_Role
WHERE rr.Code = '" + Code + "'";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        users.Add(new UserModel
                        {
                            ID_User = Convert.ToInt32(dr["ID_User"]),
                            ID_Role = Convert.ToInt32(dr["ID_Role"]),
                            UserName = Convert.ToString(dr["FIO"]),
                            RoleName = Convert.ToString(dr["RoleName"]),
                            Login = Convert.ToString(dr["Login"]),
                            Name = Convert.ToString(dr["Name"]),
                            Surname = Convert.ToString(dr["Surname"]),
                            Patronymic = Convert.ToString(dr["Patronymic"]),
                            Code = Convert.ToString(dr["Code"])
                        });
                    }
                }
            }
            catch { }
            return users;
        }


        public bool DeleteUser(string id)
        {
            try
            {

                OpticModel iList = new OpticModel();
                string query = @"DELETE FROM [dbo].[user_roles] WHERE ID_User = " + id + @";
                                    DELETE FROM [dbo].[users] WHERE ID_User = " + id + ";";

                dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                else return false;
            }
            catch { return false; }

        }

        public bool CanNewDeleteUser(UserModel id)
        {
            if (id.RoleName == "Оптика")
            {
                string query = @"SELECT [dbo].[dbo].[CanDeleteUser] (
   '" + hash + @"'  ," + id.ID_User + @",'" + id.Login + @"')";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr[0].ToString().Trim() == "1")
                            return true;
                    }
                }

            }

            return false;

        }












        public List<string> GetDistinctList(string itemj)
        {
            List<string> iList = new List<string>();



            string query = @"WITH AA AS
(
SELECT uu.[ID_User],[Login],[IsActive],COALESCE([FIO],'') AS FIO,[Surname], COALESCE([Email],'') AS Email, COALESCE([LastDate], CURRENT_TIMESTAMP) AS LastDate, ur.ID_Role, ro.RoleName
  FROM[dbo].[users] uu
  LEFT OUTER JOIN dbo.user_roles ur ON ur.ID_User = uu.ID_User
  LEFT OUTER JOIN dbo.roles ro ON ro.ID_Role = ur.ID_Role
WHERE ur.ID_Role IS NOT NULL
  )
  SELECT
  DISTINCT " + itemj + @" AS DD
  FROM AA";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(
                      Convert.ToString(dr["DD"]));


                    y++;
                }
            }
            return iList;
        }





        public bool NewCheckDeleteUser(UserModel id)
        {

            {
                string query = @"SELECT [dbo].[CanDeleteUser] (
   N'" + hash + @"'
  ," + id.ID_User + @"
  ,'" + id.Login + "')";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string yy = dr[0].ToString().Trim();
                        if (dr[0].ToString().Trim().Length > 0)
                            return true;
                    }
                }

            }

            return false;
        }


        public bool NewDeleteUser(UserModel id)
        {
            if (id.RoleName == "Оптика")
            {
                string query = @"DECLARE		@ErrorMessage varchar(max)
SELECT	@ErrorMessage = N''
EXEC	[dbo].[DELETE_optics] @session_hash = N'" + hash + @"',	@p_ID_Optic = " + id.ID_User + @",		@ErrorMessage = @ErrorMessage OUTPUT
SELECT	@ErrorMessage as N'@ErrorMessage'";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr[0].ToString().Trim().Length == 0)
                            return true;
                    }
                }

            }
            else
            {
                string query = @"DECLARE		@ErrorMessage varchar(max)
SELECT	@ErrorMessage = N''
EXEC	[dbo].[DELETE_users] @session_hash = N'" + hash + @"',	@p_ID_User = " + id.ID_User + @",		@ErrorMessage = @ErrorMessage OUTPUT
SELECT	@ErrorMessage as N'@ErrorMessage'";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr[0].ToString().Trim().Length == 0)
                            return true;
                    }
                }

            }
            return false;
        }

        private bool ChekUserExists(UserModel id)
        {
            try
            {
                string query = @"WITH AA AS
(
SELECT uu.[ID_User],[Login],[IsActive],COALESCE([FIO],'') AS FIO,[Surname], COALESCE([Email],'') AS Email, COALESCE([LastDate], CURRENT_TIMESTAMP) AS LastDate, ur.ID_Role, ro.RoleName
  FROM[dbo].[users] uu
  LEFT OUTER JOIN dbo.user_roles ur ON ur.ID_User = uu.ID_User
  LEFT OUTER JOIN dbo.roles ro ON ro.ID_Role = ur.ID_Role
WHERE ur.ID_Role IS NOT NULL
  UNION ALL
  SELECT[ID_Optic], '',[IsActive],[OpticName], '' as surname,[Email], LastDate, ro.ID_Role, ro.RoleName
  FROM[dbo].[optics] oo
  LEFT OUTER JOIN dbo.roles ro ON ro.Code = '1KK1XNKI7QF'
  )
  SELECT 1
  FROM AA WHERE(SELECT[dbo].[fGetPermissionForUsers] ('" + hash + @"'  , [ID_User]  , Login)) = 1 AND ID_User = " + id.ID_User.ToString() + " AND COALESCE(Login,'') = '" + id.Login + "'";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                { return true; }
            }
            catch { }
            return false;
        }

        public bool MakeInvitation(int IdUser)
        {
            try
            {
                string query = @"";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                { return true; }
            }
            catch { }
            return false;
        }



        public int NewInsertUser(UserModel id)
        {
            /* if (!ChekUserExists(id))
                 return true;*/
            if (id.RoleName != "Оптика")
            {
                string uu = "";
                int yy = id.IsActive ? 1 : 0;
                string query = @"IF NOT EXISTS(SELECT 1 FROM [dbo].[users] WHERE [Login] = '" + id.Login + @"')
BEGIN
INSERT INTO [dbo].[users]
           ([FIO], [Login], [IsActive], [Email], [LastDate])
OUTPUT Inserted.ID_User
     VALUES ('" + id.UserName + "', '" + id.Login + "', " + yy.ToString() + ", '" + id.Email + "', CURRENT_TIMESTAMP) END";
                if (id.ID_User > 0)
                {
                    query = @"IF NOT EXISTS(SELECT 1 FROM [dbo].[users] WHERE [Login] = '" + id.Login + @"' AND ID_User <> " + id.ID_User.ToString() + @")
BEGIN
UPDATE [dbo].[users]
   SET [FIO] = '" + id.UserName + @"'
      ,[Login] = '" + id.Login + @"'
      ,[IsActive] = " + yy.ToString() + @"
      ,[Email] =  '" + id.Email + @"'
      ,[LastDate] = CURRENT_TIMESTAMP
,[Name]= '" + id.Name + @"'
      ,[Surname]= '" + id.Surname + @"'
      ,[Patronymic]= '" + id.Patronymic + @"'
 WHERE ID_User = " + id.ID_User.ToString() + @" ; 
SELECT " + id.ID_User.ToString() + "; END";
                }
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr[0].ToString().Trim().Length == 0)
                            return -1;
                        uu = dr[0].ToString().Trim();
                    }
                }
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count == 0)
                { return -100; }
                if (uu.Trim().Length > 0)
                {
                    query = @"DELETE FROM [dbo].[user_roles] WHERE [ID_User] = " + uu + @"
INSERT INTO [dbo].[user_roles]
           ([ID_User],[ID_Role])
     VALUES            (" + uu + ", " + id.ID_Role.ToString() + ")";
                    dbc.Make_Query(query);
                    if (dbc.last_error.Length == 0)
                    {
                        return Convert.ToInt32(uu);
                    }
                }

            }

            return -1;
        }




        public bool CheckLogin(string login)
        {

                string query = @"IF EXISTS(SELECT 1 FROM [dbo].[users] WHERE [Login] = '" + login + @"')
BEGIN SELECT 1111; END";
               
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    return true;
                }
                else if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count == 0)
                { return false; }
               else 
                { return false; }            

        }


        public bool CheckEmail(string email)
        {

            string query = @"IF NOT EXISTS(SELECT 1 FROM [dbo].[users] WHERE [Email] =  '" + email + @"') AND NOT EXISTS(SELECT 1  FROM [dbo].[optics] WHERE [Email] = '" + email + @"'   )
BEGIN SELECT 1111; END";

            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
            {
                return true;
            }
            else if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count == 0)
            { return false; }
            else
            { return false; }

        }








    }
}
