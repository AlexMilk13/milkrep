﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class UserModel
    {

        public int ID_User { get; set; }
        public string UserName { get; set; }
        public int ID_Role { get; set; }
        public string RoleName { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
        public DateTime LastDate { get; set; }
        public int pagecount { get; set; }
        public bool IsActive { get; set; }
    }
}
