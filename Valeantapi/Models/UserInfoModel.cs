﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class UserInfoModel
    {
        public int id_User { get; set; }
        public string UserFIO { get; set; }
        public int id_Optic { get; set; }
        public string OpticName { get; set; }
        public int id_Network { get; set; }
        public string NetworkName { get; set; }
    }
}
