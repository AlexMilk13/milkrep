﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class mindboxModel
    {
        public long ID { get; set; }
        public string status { get; set; }
        public string Code { get; set; }
        public customer cust { get; set; }
        public string sms { get; set; }
    }

    public class customer
    {
        public string processingStatus { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string isEmailInvalid { get; set; }
        public string mobilePhone { get; set; }
        public string isMobilePhoneInvalid { get; set; }
        public string sex { get; set; }

        public string changeDateTimeUtc { get; set; }
        public string ianaTimeZone { get; set; }
        public string timeZoneSource { get; set; }
        public DateTime ValidTo { get; set; }
        public string sessionKey { get; set; }

        public id ids { get; set; }
        public customField customFields { get; set; }
        List<subscription> subscriptions { get; set; }
        public class id
        {
            public long mindboxId { get; set; }
        }
        public class customField
        {
            public string cityForm { get; set; }
            public string clientIDGoogleAnalytics { get; set; }
            public string clientIDYandexMetrika { get; set; }
            public string domain { get; set; }
            public string opticAdress { get; set; }
            public string opticeEmail { get; set; }
            public string opticManager { get; set; }
            public string opticName { get; set; }
            public string opticPhone { get; set; }
            public string whatYouUse { get; set; }
        }

        public class subscription
        {
            public string pointOfContact { get; set; }
            public string topic { get; set; }
            public bool isSubscribed { get; set; }
        }
    }
}

