﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class NetworksModel
    {
      public int ID_Network { get; set; }
        public string Code { get; set; }
        public string NetworkName { get; set; }
        public bool IsActive { get; set; }
        public string INN { get; set; }
        public int pagecount { get; set; }

    }
}
