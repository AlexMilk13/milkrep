﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class TownModel
    {
        public int ID_Town { get; set; }
        public string Code { get; set; }
        public string TownName { get; set; }
        public int ID_Region { get; set; }
        public string RegionName { get; set; }
    }
}
