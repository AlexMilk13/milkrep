﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class MatchingReportDBHandler
    {
        public DbClass dbc = new DbClass();
        public int count = 10;
        public int page;
        public string sort = "ID_Diagnostic";
        public string order = "DESC";
        public string DocName = "";
        public string ObjectName = "";//причина списания
        public string ID_Diagnostic = "";
        public UserInfoModel ui = new UserInfoModel();
        public int userrole = 0;
        public string restrict = "";





        public List<MatchingReportModel> GetItemList(DateTime DateBegin, DateTime DateEnd, string OpticName, string NetworkName)
        {

            string OpticNamefilter = "";
            string NetworkNamefilter = "";
            string ObjectNamefilter = "";
            string DateBeginFilter = "";//Дата начала,Время начала,
            string DateEndFilter = "";//Дата окончания,Время окончания

            List<MatchingReportModel> iList = new List<MatchingReportModel>();

            if (OpticName!=null && OpticName.Length > 0)
            {
                OpticNamefilter = " AND OpticJuraName IN (";

                int y = 0;
                foreach (string tt in OpticName.Split('|'))
                {
                    if (y > 0)
                        OpticNamefilter += ", ";
                    OpticNamefilter += "'" + tt + "'";
                    y++;
                }
                OpticNamefilter += ") ";
            }


            if (NetworkName!=null && NetworkName.Length > 0)
            {
                NetworkNamefilter = " AND NetworkName IN (";

                int y = 0;
                foreach (string tt in NetworkName.Split('|'))
                {
                    if (y > 0)
                        NetworkNamefilter += ", ";
                    NetworkNamefilter += "'" + tt + "'";
                    y++;
                }
                NetworkNamefilter += ") ";
            }

            if (ObjectName.Length > 0)
            {
                ObjectNamefilter = " AND ObjectName IN (";

                int y = 0;
                foreach (string tt in ObjectName.Split('|'))
                {
                    if (y > 0)
                        ObjectNamefilter += ", ";
                    ObjectNamefilter += "'" + tt + "'";
                    y++;
                }
                ObjectNamefilter += ") ";
            }
            ////////////////////////////////////////////
            if (DateBegin != null && DateBegin != new DateTime(1, 1, 1))
            {
                DateBeginFilter = " AND REPDATE >= ";

                DateBeginFilter += "'" + DateBegin.ToString("yyyy-MM-dd HH:mm:ss") + "'";

            }
            else
            {
                DateBeginFilter = "";
            }

            if (DateEnd != null && DateEnd != new DateTime(1, 1, 1))
            {
                DateTime gg = new DateTime(DateEnd.Year, DateEnd.Month, DateEnd.Day).AddDays(1).AddMilliseconds(-1);
                DateEndFilter = " AND REPDATE <= ";
                DateEndFilter += "'" + gg.ToString("yyyy-MM-dd HH:mm:ss") + "'";

            }
            else
                DateEndFilter = "";
            /////////////////////////
            /////////////////////////////


            //ExportDiagOrdersController


            string sorter = " ORDER BY ID_Outgoing ";
            if (sort != null && sort.Trim().Length > 0)
            {
                sorter = " ORDER BY " + sort + " ";
                if (order != null && order.Trim().Length > 0)
                    sorter += order;
            }
            string offset = "";

            // offset = "  OFFSET " + ((page - 1) * count).ToString() + " ROWS FETCH NEXT " + count.ToString() + " ROWS ONLY";
            string query = @"SELECT 1  FROM [dbo].[SelectionView] WHERE 1=1 
 " + DateBeginFilter + DateEndFilter + restrict + OpticNamefilter + NetworkNamefilter + ObjectNamefilter + sorter + offset;
            DataTable dt = dbc.Make_Query(query);
            count = dt.Rows.Count;
            query = @" SELECT [KASName], [ID_Diagnostic] ,[ID_Sertificat] ,[CodeMindBox] ,[NetworkName] ,[OpticJuraName] ,[RegionName]
 ,[BrickVC] ,[TownName] ,[Address] ,[mobilePhone] ,[Nomenkl] ,[ID_Optic] ,[AccessCode]
 ,[Certif] ,[Amount] ,[ID_Result] ,[ResultName] ,[DateEnd] ,[ID_User] ,[FIO],[MonthNamep]
  FROM [dbo].[SelectionView] WHERE 1=1 
 " + DateBeginFilter + DateEndFilter + restrict + OpticNamefilter + NetworkNamefilter + ObjectNamefilter + sorter + offset;
            dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new MatchingReportModel
                    {
                        KamKas = dr["KASName"] == Convert.DBNull ? "" : Convert.ToString(dr["KASName"]),
                        Code = dr["CodeMindBox"] == Convert.DBNull ? "" : Convert.ToString(dr["CodeMindBox"]),
                        NetworkName = dr["NetworkName"] == Convert.DBNull ? "" : Convert.ToString(dr["NetworkName"]),
                        OpticJuraName = dr["OpticJuraName"] == Convert.DBNull ? "" : Convert.ToString(dr["OpticJuraName"]),
                        RegionName = dr["RegionName"] == Convert.DBNull ? "" : Convert.ToString(dr["RegionName"]),
                        Brick = dr["BrickVC"] == Convert.DBNull ? "" : Convert.ToString(dr["BrickVC"]),
                        Town = dr["TownName"] == Convert.DBNull ? "" : Convert.ToString(dr["TownName"]),
                        OpticAddress = dr["Address"] == Convert.DBNull ? "" : Convert.ToString(dr["Address"]),
                        PhoneNumber = dr["mobilePhone"] == Convert.DBNull ? "" : Convert.ToString(dr["mobilePhone"]),
                        Nomenklatur = dr["Nomenkl"] == Convert.DBNull ? "" : Convert.ToString(dr["Nomenkl"]),
                        Quantity = dr["Amount"] == Convert.DBNull ? "" : Convert.ToString(dr["Amount"]),
                        Reason = dr["ResultName"] == Convert.DBNull ? "" : Convert.ToString(dr["ResultName"]),
                        EndDate = dr["DateEnd"] == Convert.DBNull ? "" : Convert.ToString(dr["DateEnd"]),
                        MonthName = dr["MonthNamep"] == Convert.DBNull ? "" : Convert.ToString(dr["MonthNamep"]),
                        CertSMS = dr["Certif"] == Convert.DBNull ? "" : Convert.ToString(dr["Certif"]),
                        ID_Diagnostic = dr["ID_Diagnostic"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Diagnostic"]),
                        DocName = dr["FIO"] == Convert.DBNull ? "" : Convert.ToString(dr["FIO"]),
                        pagecount = count
                    });


                }
            }
            query = "SELECT [SName],[ID_Diagnostic],[ResultName],[Nomenkl]  FROM [dbo].[OutgoingSKUView] WHERE [ID_Diagnostic] IS NOT NULL";
            dt = dbc.Make_Query(query);            
            if (dbc.last_error.Length == 0)
            {
                foreach (MatchingReportModel mod in iList)
                {
mod.WriteOffSKU = new List<MatchingReportSKUModel>();
                    foreach (DataRow dr in dt.Select("[ID_Diagnostic] = " + mod.ID_Diagnostic.ToString() + " AND [ResultName] = '" + mod.Reason + "' AND [Nomenkl] = '" + mod.Nomenklatur + "'"))
                    {
                        mod.WriteOffSKU.Add(new MatchingReportSKUModel { SKUName = dr["SName"] == Convert.DBNull ? "" : Convert.ToString(dr["SName"]) });
                    }
                }
            }
            return iList;
        }



    }
}
