﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class AddrDBHandler
    {

        public DbClass dbc = new DbClass();
        public int count;
        public int page;
        public string sort = "ID_Address";
        public string order = "ASC";


        public List<AddrModel> GetItemList(string idPar)
        {
            List<AddrModel> iList = new List<AddrModel>();
            if (idPar.Trim().Length == 0 || idPar.Trim() == "0")
                idPar = " IS NULL";
            else
                idPar = "=" + idPar;
            string query = @"SELECT [ID_Address]
      ,[Address_Name]
      ,[Fias_Code]
      ,[ID_Parent]
  FROM [dbo].[FiasAddresses] WHERE [ID_Parent] " + idPar;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                int y = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    iList.Add(new AddrModel
                    {
                        ID_Address = dr["ID_Address"] == Convert.DBNull ? -1 : Convert.ToInt32(dr["ID_Address"]),
                        Address_Name = dr["Address_Name"] == Convert.DBNull ? "" : Convert.ToString(dr["Address_Name"]),
                        Fias_Code = dr["Fias_Code"] == Convert.DBNull ? "" : Convert.ToString(dr["Fias_Code"])
                    });
                }
                return iList;
            }
            return null;
        }
    }
}
