﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class SKUModel
    {
      public int ID_Sku { get; set; }
      public string Code { get; set; }
      public string SkuName { get; set; }
      public Single Diopter { get; set; }
      public string Description { get; set; }
      public bool IsActive { get; set; }
      public string SKU_ID { get; set; }
      public int StoreCount { get; set; }
      public int id_StoringWay { get; set; }
        public string Store_Name { get; set; }
        public int pagecount { get; set; }
        public int Picture { get; set; }
        public bool IsBausch { get; set; }
    }
}
