﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Valeantapi.utils;

namespace Valeantapi.Models
{
    public class TownDBHandler
    {

        public DbClass dbc = new DbClass();
        public int count;
        public int page;
        public string sort = "ID_Town";
        public string order = "ASC";
        public bool insertItem(TownModel id /*string sortOrder, bool asc, int pagenumber, int rows, ref int count, string searcher*/)
        {
            try
            {                
                string query = @"INSERT INTO [dbo].[towns] ([Code], [TownName], [ID_Region])
OUTPUT Inserted.ID_Town
     VALUES
           ('" + id.Code + @"' , '" + id.TownName + @"'," + id.ID_Region.ToString() + ")";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 )
                {                   
                        return true;                    
                }
                else return false;
            }
            catch { return false; }
        }


        public List<TownModel> GetTowns()
        {
            List<TownModel> users = new List<TownModel>();
            try
            {
                string query = @"SELECT [ID_Town]      ,tt.[Code]      ,[TownName]      ,tt.[ID_Region]	  ,rr.RegionName
  FROM [dbo].[towns] tt LEFT OUTER JOIN dbo.regions rr ON rr.ID_Region = tt.ID_Region";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        users.Add(new TownModel
                        {
                            ID_Town = Convert.ToInt32(dr["ID_Town"]),
                            ID_Region = Convert.ToInt32(dr["ID_Region"]),
                            RegionName = Convert.ToString(dr["RegionName"]),
                            Code = Convert.ToString(dr["Code"]),
                            TownName = Convert.ToString(dr["TownName"])
                        });
                    }
                }
            }
            catch { }
            return users;
        }

        public TownModel GetOneTown(string Code)
        {
            TownModel users = new TownModel();
            try
            {
                string query = @"SELECT [ID_Town]      ,tt.[Code]      ,[TownName]      ,tt.[ID_Region]	  ,rr.RegionName
  FROM [dbo].[towns] tt LEFT OUTER JOIN dbo.regions rr ON rr.ID_Region = tt.ID_Region
WHERE [ID_Town] = " + Code + "";

                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        users = (new TownModel
                        {
                            ID_Town = Convert.ToInt32(dr["ID_Town"]),
                            ID_Region = Convert.ToInt32(dr["ID_Region"]),
                            RegionName = Convert.ToString(dr["RegionName"]),
                            Code = Convert.ToString(dr["Code"]),
                            TownName = Convert.ToString(dr["TownName"])
                        });
                    }
                }
            }
            catch { }
            return users;
        }


        public bool DeleteTown(string id)
        {
            try
            {                
                string query = @"DELETE FROM [dbo].[towns] WHERE [ID_Town] = " + id + @";";

                dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    return true;
                }
                else return false;
            }
            catch { return false; }

        }
    }
}
