﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Valeantapi.Models
{
    public class MediaFileModel
    {

        public int FileId { get; set; }        
        public String FileType { get; set; }
        public String Code { get; set; }
        public byte[] mediafile { get; set; }
    }
}
