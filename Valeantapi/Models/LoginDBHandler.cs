﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Valeantapi.utils;
using Microsoft.AspNetCore.Http;

namespace Valeantapi.Models
{
    public class LoginDBHandler
    {
        public DbClass dbc = new DbClass();
        public int count;
        public int page;
        public string sort = "ID_Optic";
        public string order = "ASC";
        public string Code = "";
        public string login = "";
        public string pass = "";
        public string idoptic = "";
        public string iduser = "";
        public HttpRequest Req;
        public HttpResponse resp;
        public DateTime expires;
        public bool rememb = false;
        public cookieclass cook = new cookieclass();
        public string hash = "";
        public bool setUpResponce()
        {
            cook = new cookieclass();
            cook.UserName = iduser;
            cook.userhash = pass;
            cook.Opt = idoptic;
            cook.expires = expires;
            
         //   cook.SetCookie();
            return true;
        }

        public bool TryToLogin()
        {
            string query = "" ;
            if (login == null) login = "";
            string rem =rememb? "1" : "0";
            if (Code.Trim().Length > 0)
                query = @"EXEC	[dbo].[sp_login_ext] @pLogin = N'" + login + @"', @pPassword = N'" + Code + @"', @pRememb = " + rem;
            else if
                (login.Trim().Length > 0 && pass.Trim().Length > 0)
                query =
                    @"EXEC	[dbo].[sp_login_ext] @pLogin = N'" + login + @"', @pPassword = N'" + pass.ToUpper() + @"', @pRememb = " + rem;

            //@"SELECT [ID_User] ,[Password]  FROM [dbo].[users] WHERE [Password] = '" + pass.ToUpper() + "' AND login = '" + login + "'";
            else return false;
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {                
                foreach (DataRow dr in dt.Rows)
                {
                    if (dt.Rows[0]["haash"].ToString().Trim().Length > 0)
                    {
                        idoptic = dt.Rows[0]["haash"].ToString();
                        try
                        {
                            query = @"
                                    UPDATE [dbo].[loginaudit]
                                       SET [Browser] = '" + Req.Headers["User-Agent"].ToString() + @"'
                                          ,[ipstring] = '" + Req.HttpContext.Connection.RemoteIpAddress.ToString() + @"', validTo = '" + expires.ToString("yyyy-MM-dd HH:mm:ss") + @"', rememb = '" + rememb.ToString() + @"'
                                     WHERE [IsActive] = 1 AND [session_hash] = '" + idoptic + @"'";
                            dbc.Make_Query(query);
                        }
                        catch { }
                        return true;
                    }
                    else
                        { return false; }


                }
               
            }
            return false;
        }



        public int TryToLoginInt()
        {
            try
            {
                string query = "";
                if (login == null) login = "";
                string rem = rememb ? "1" : "0";
                if (Code.Trim().Length > 0)
                    query = @"EXEC	[dbo].[sp_login_ext] @pLogin = N'" + login + @"', @pPassword = N'" + Code + @"', @pRememb = " + rem;
                else if
                    (login.Trim().Length > 0 && pass.Trim().Length > 0)
                    query =
                        @"EXEC	[dbo].[sp_login_ext] @pLogin = N'" + login + @"', @pPassword = N'" + pass.ToUpper() + @"', @pRememb = " + rem;


                else return 0;
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dt.Rows[0]["haash"].ToString().Trim().Length > 0)
                        {
                            idoptic = dt.Rows[0]["haash"].ToString();
                            try
                            {
                                query = @"
                                    UPDATE [dbo].[loginaudit]
                                       SET [Browser] = '" + Req.Headers["User-Agent"].ToString() + @"'
                                          ,[ipstring] = '" + Req.HttpContext.Connection.RemoteIpAddress.ToString() + @"', validTo = '" + expires.ToString("yyyy-MM-dd HH:mm:ss") + @"', rememb = '" + rememb.ToString() + @"'
                                     WHERE [IsActive] = 1 AND [session_hash] = '" + idoptic + @"'";
                                dbc.Make_Query(query);
                            }
                            catch { }
                            return 1;
                        }
                        else if (dt.Rows[0]["result_code"].ToString().Trim() == "-1000")
                        { return -1000; }
                        else
                            return 0;


                    }

                }
            }
            catch { }
            return 0;
        }


        public bool CheckLogged()

        {
            string query = @"SELECT [dbo].[GetLoginnedUser] ( N'" + login + @"', N'" + pass + @"')";
           
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0 && dt.Rows.Count>0)
            {
                if (Convert.ToInt32(dt.Rows[0][0]) == 1)
                    return true;
            }
            return false;
        }


        public bool RenewCookies()
        {
            resp.Cookies.Delete("UhashString");
            if (Req.Cookies.ContainsKey("UhashString"))
            {
                string hh = Req.Cookies["UhashString"];
                hash = hh;
                string query = @"
                                    UPDATE [dbo].[loginaudit]
                                       SET [Browser] = '" + Req.Headers["User-Agent"].ToString() + @"'
                                          ,[ipstring] = '" + Req.HttpContext.Connection.RemoteIpAddress.ToString() + @"', validTo = (CASE WHEN rememb=1 THEN DATEADD(year,1,CURRENT_TIMESTAMP) ELSE DATEADD(hour,1,CURRENT_TIMESTAMP) END)
                                     WHERE [IsActive] = 1 AND [session_hash] = '" + idoptic + @"' AND validTo>=CURRENT_TIMESTAMP";
                dbc.Make_Query(query);
                resp.Cookies.Append("UhashString", hh);
                return true;
            }
            return false;
        }

        public bool DisableCookies()
        {
            
            if (Req.Cookies.ContainsKey("UhashString"))
            {
                string hh = Req.Cookies["UhashString"];
                hash = hh;
                string query = @"
                                    UPDATE [dbo].[loginaudit]
                                       SET [IsActive] = 0
                                     WHERE [IsActive] = 1 AND [session_hash] = '" + hh + @"'";
                dbc.Make_Query(query);
                
                return true;
            }
            return false;
        }

        public bool changePass(string pass, int role)
        {
            string query = "";
            if (role == 1000 && Req.Cookies.ContainsKey("UhashString"))
            {
                string hh = Req.Cookies["UhashString"];
                query = "UPDATE [dbo].[optics] SET[AccessCode] = '" + pass + "' WHERE[ID_Optic] = (SELECT [valueint] FROM [dbo].[loginaudit] WHERE [IsActive] = 1 AND [session_hash] = '" + hh + "')";
            }
            else if (Req.Cookies.ContainsKey("UhashString"))
            {
                string hh = Req.Cookies["UhashString"];
                query = @"UPDATE [dbo].[users]
SET [Password] = (SELECT [dbo].[GeneratePassHash] ([login] ,'" + pass + @"'))
WHERE ID_User = 
(SELECT [valueint] FROM [dbo].[loginaudit] la WHERE [IsActive] = 1 AND la.[session_hash] = '" + hh + "')";
             }
            else return false;
            dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
                return true;
            else return false;
        }


        public bool changePassSimple(string User)
        {
            string query = "";

                string hh = Req.Cookies["UhashString"];
                query = @"UPDATE [dbo].[users]
SET [Password] = (SELECT [dbo].[GeneratePassHash] ([login] ,'" + pass + @"'))WHERE ID_User = " + User;


            dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
                return true;
            else return false;
        }

        public bool CheckLogin(string hash)
        {
            string query = @"SELECT 1  FROM [dbo].[loginaudit] WHERE [IsActive] = 1 AND [session_hash] = '" + hash + "' AND [validTo]>=CURRENT_TIMESTAMP";            
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0 && dt.Rows.Count>0)
            {
                return true;
            }
            return false;
        }

        public int GetRole(string hash)
        {
            string query = @"SELECT [dbo].[GetLoginnedRole] ('" + hash + @"') AS dd";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0 && dt.Rows.Count > 0)
            {
                try
                {
                    return Convert.ToInt32(dt.Rows[0]["dd"]);
                }
                catch { }
            }
            return 0;
        }

        public UserInfoModel UserInfo(string hash)
        {
            string query = @"SELECT [ID_User]      ,[UserName]      ,[ID_Optic]      ,[OpticName]      ,[ID_Network]      ,[NetworkName]  FROM [dbo].[UserInfoView] WHERE [session_hash] = '" + hash + "'";
            UserInfoModel mo = new UserInfoModel();
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0 && dt.Rows.Count > 0)
            {
                try
                {
                    mo = (new UserInfoModel
                    {
                        id_User = Convert.ToInt32(dt.Rows[0]["ID_User"]),
                        UserFIO = Convert.ToString(dt.Rows[0]["UserName"]),
                        id_Optic = Convert.ToInt32(dt.Rows[0]["ID_Optic"]),
                        OpticName = Convert.ToString(dt.Rows[0]["OpticName"]),
                        id_Network = Convert.ToInt32(dt.Rows[0]["ID_Network"]),
                        NetworkName = Convert.ToString(dt.Rows[0]["NetworkName"])
                    });
                    return mo;
                }
                catch { }
            }
            return mo;
        }


        public bool NewPass(string Code, string pass)
        {
            string query = @"UPDATE [dbo].[users] SET [Password] = (SELECT [dbo].[GeneratePassHash] ([login] ,'" + pass + @"'))
WHERE ID_User IN (SELECT [IDUser] FROM [dbo].[invitations] WHERE [ValidTo]>CURRENT_TIMESTAMP AND [InvitationCode] = '" + Code + @"');
UPDATE [dbo].[invitations] SET [ValidTo]=CURRENT_TIMESTAMP WHERE InvitationCode = '" + Code + "';";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0)
            {
                return true;
            }
            return false;
        }


        public string GetInvitation(string Code)
        {
            string query = @"SELECT [IDUser] FROM [dbo].[invitations] WHERE [ValidTo]>CURRENT_TIMESTAMP AND [InvitationCode] = '" + Code + "'";
            DataTable dt = dbc.Make_Query(query);
            if (dbc.last_error.Length == 0 && dt.Rows.Count > 0)
            {
                return Convert.ToString(dt.Rows[0]["IDUser"]);
            }
            //SELECT [IDUser] FROM [dbo].[invitations] WHERE [ValidTo]>CURRENT_TIMESTAMP AND [InvitationCode]
            return "";
        }

    }



    //////////////////////////////////////
    public class cookieclass
    {
        public bool IsAdmin = false;
        public bool IsTM = false;
        public bool IsDoc = false;
        public string hashString;
        public bool is_valid;
        public float cookie_Lasts;/// how long does cookie last
        public DateTime expires;
        public string userStatus = "";
        public string UserEmail = "";
        public HttpRequest Request;
        public HttpResponse Response;
        public string userhash;
        public string UserName;
        public string Opt = "";

        public cookieclass()
        {
            UserName = hashString = "";
            is_valid = false;
            try
            {
               cookie_Lasts = -1;
            }
            catch { cookie_Lasts = -1; }
        }

        public bool NullAllVals()
        {
            is_valid = false;
            UserName = hashString = "";
            expires = DateTime.Now.AddDays(-1d);
            try
            {

                cookie_Lasts = -1;

                userStatus = "";

                return true;
            }
            catch { return false; }
        }

        public bool GetAllVals()
        {            
            UserName = hashString = "";
            UserName = Request.Cookies["UserName"];
            hashString = Request.Cookies["UhashString"];
            string expiresstring = Request.Cookies["expires"];
            
            try
            {
               
                return false;
            }
            catch { }
            return false;
        }

        


        public bool SetCookie()
        {
            
            Response.Cookies.Delete("UserName");
            Response.Cookies.Delete("UhashString");
            Response.Cookies.Delete("expires");
            Response.Cookies.Delete("Opt");

            if (Response != null)
            {
                if (UserName.Length > 0)
                    Response.Cookies.Append("UserName", UserName);
                if (hashString.Length > 0)
                    Response.Cookies.Append("UhashString", hashString);
                if (Opt.Length > 0)
                {
                    Response.Cookies.Append("UserName", Opt);
                    Response.Cookies.Append("Opt", Opt);
                }
                if (expires != null)
                    Response.Cookies.Append("expires", expires.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            return false;
        }








    }


}
