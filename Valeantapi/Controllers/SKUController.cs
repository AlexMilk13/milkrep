﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SKUController : ControllerBase
    {
        // GET: api/SKU
        [HttpGet]
        public IEnumerable<SKUModel> Get(int count = 10, int page = 1, string sort = "ID_Sku", string order = "DESC", string Code = "", string SkuName = "", string Diopter = "", string Description = "", string StoreCount = "", 
            string Store_Name = "", string SKU_ID = "", string IsActive = "", string IsBausch = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        SKUModel[] op = new SKUModel[0];
                        SKUDBHandler iHandler = new SKUDBHandler();
                        iHandler.count = count;
                        iHandler.page = page;
                        iHandler.Code = Code;
                        iHandler.SkuName = SkuName;
                        iHandler.Diopter = Diopter;
                        iHandler.Description = Description;
                        iHandler.StoreCount = StoreCount;
                        iHandler.Store_Name = Store_Name;
                        iHandler.SKU_ID = SKU_ID;
                        iHandler.IsBausch = IsBausch;

                        iHandler.IsActive = IsActive;
                        iHandler.sort = sort;
                        iHandler.order = order;
                        List<SKUModel> bb = iHandler.GetItemList();
                        if (bb.Count > 0)
                            op = bb.ToArray();
                        return op;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return null;
        }



        // GET: api/SKU/5
        [HttpGet("{id}", Name = "GetSKU")]
        public SKUModel Get(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        SKUModel op = new SKUModel();
            SKUDBHandler iHandler = new SKUDBHandler();
            op = iHandler.GetItem(id.ToString());
            return op;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return null;
        }

        // POST: api/SKU
        [HttpPost]
        public string Post([FromBody] SKUModel value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        try
                        {
                            SKUDBHandler iHandler = new SKUDBHandler();
                            if (iHandler.insertItem(value))
                                return "ok";
                            else return "";
                        }
                        catch { }
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return "";
        }

        // PUT: api/SKU/5
        [HttpPut("{id}")]
        public bool Put([FromBody] SKUModel value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        try
                        {
                            SKUDBHandler iHandler = new SKUDBHandler();
                            return iHandler.UpdateItem(value);
                        }
                        catch { }
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return false;
        }

        // DELETE: api/SKU/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        SKUDBHandler iHandler = new SKUDBHandler();
                        if (id != null && id > 0 && iHandler.DeleteItem(id))
                            return true;
                        else
                            return false;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return false;
        }

        [HttpPatch("{id}", Name = "PatchSKU")]
        public bool Patch(string id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        SKUDBHandler iHandler = new SKUDBHandler();
                        if (id != null && id.Trim().Length > 0)
                            return iHandler.DeleteItemCheck(id);
                        else if (id != null && id.Trim().Length > 0 && iHandler.dbc.last_error.Trim().Length > 0)
                        {
                            return iHandler.dbc.last_error.Trim().Length > 0;
                        }
                        else
                            return false;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return false;
        }
    }
}
