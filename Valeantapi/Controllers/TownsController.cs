﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TownsController : ControllerBase
    {
        // GET: api/Towns
        [HttpGet]
        public IEnumerable<TownModel> Get()
        {
            //"1KGBS4VXBIO" --KAM
            TownModel[] OptDocs = new TownModel[0];
            TownDBHandler iHandler = new TownDBHandler();
            List<TownModel> bb = new List<TownModel>();            
                bb = iHandler.GetTowns();
            if (bb.Count > 0)
                OptDocs = bb.ToArray();
            return OptDocs;
        }



        [HttpGet("{id}", Name = "GetTown")]
        public TownModel Get(int id)
        {
            //"1KGBS4VXBIO" --KAM
            TownModel OptDocs = new TownModel();
            TownDBHandler iHandler = new TownDBHandler();
            
            OptDocs = iHandler.GetOneTown(id.ToString());
            
            return OptDocs;
        }



        // POST: api/Towns
        [HttpPost]
        public bool Post([FromBody] TownModel value)
        {
            try
            {
                if (value != null)
                {
                    TownDBHandler iHandler = new TownDBHandler();
                    return iHandler.insertItem(value);
                }
            }
            catch { return false; }
            return true;
        }



        // DELETE: api/Towns/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            try
            {
                TownDBHandler iHandler = new TownDBHandler();
                return iHandler.DeleteTown(id.ToString());
            }
            catch { return false; }
        }
    }
}
