﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Valeantapi.Models;


namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class ResultsController : Controller
    {
        [HttpGet]
        public IEnumerable<Results> Get()
        {
            Results[] tt = new Results[0];
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        tt = (new WriteOffDBHandler()).GetResultsList().ToArray();
                        return tt;
                    }
                }
                Response.StatusCode = 401;
            }
            return tt;
        }
    }
}
