﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/Optic")]
    [ApiController]
    public class OpticController : ControllerBase
    {

        /// <summary>
        /// get list
        /// </summary>
        /// <returns></returns>
        // GET: api/Optic
        [HttpGet]


        public IEnumerable<OpticModel> Get(int count = 10, int page = 1, string sort = "ID_Optic", string order = "Desc", string NetworkName = "",
          string OpticTorgName = "", string OpticJuraName = "", string INN = "", string UserName = "",
          string KASName = "", string TownName = "", string Phone = "", string Email1 = "",
          string Email2 = "", string Email3 = "", string OnlineLink = "", string Kids = "",
          string SMS = "", string Digital = "", string Latitude = "", string Longitude = "",
          string Doc1 = "", string IsActive = "",string Address = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    int role = log.GetRole(hh);
                    if (role > 0)
                    {
                        OpticModel[] op = new OpticModel[0];
                        OpticDBHandler iHandler = new OpticDBHandler();
                        iHandler.count = count;
                        iHandler.page = page;
                        iHandler.OpticTorgName = OpticTorgName;
                        iHandler.OpticJuraName = OpticJuraName;
                        iHandler.INN = INN;
                        iHandler.NetworkName = NetworkName;
                        iHandler.KASName = KASName;
                        iHandler.TownName = TownName;
                        iHandler.UserName = UserName;
                        iHandler.IsActive = IsActive;
                        iHandler.Doc1 = Doc1;
                        iHandler.Email1 = Email1;
                        iHandler.Email2 = Email2;
                        iHandler.Email3 = Email3;
                        iHandler.OnlineLink = OnlineLink;
                        iHandler.Kids1 = Kids;
                        iHandler.SMS = SMS;
                        iHandler.Digital = Digital;
                        iHandler.Latitude = Latitude;
                        iHandler.Longitude = Longitude;
                        iHandler.Phone = Phone;
                        iHandler.sort = sort;
                        iHandler.order = order;
                        iHandler.Address = Address;
                        if (role > 0 && role < 10)
                        {
                            UserInfoModel inf = log.UserInfo(hh);
                            iHandler.restrict = " AND ID_Optic IN (" + inf.id_Optic + ")";
                        }
                        if (role > 9 && role < 1000)
                        {
                            UserInfoModel inf = log.UserInfo(hh);
                            iHandler.restrict = " AND ID_Optic IN (SELECT [ID_Optic]  FROM [dbo].[OpticsView] WHERE ID_KAS = " + inf.id_User + ")";
                        }
                        List<OpticModel> bb = iHandler.GetItemList();
                        if (bb.Count > 0)
                            op = bb.ToArray();
                        return op;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return null;
        }

        // GET: api/Optic/5
        [HttpGet("{id}", Name = "GetOptic")]
        public OpticModel Get(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        OpticModel op = new OpticModel();
                        OpticDBHandler iHandler = new OpticDBHandler();
                        op = iHandler.GetItem(id.ToString());
                        return op;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return null;
        }






        /// <summary>
        /// inserting item
        /// </summary>
        /// <param name="ID_Network"></param>
        /// <param name="OpticName"></param>
        /// <param name="ID_User"></param>
        /// <param name="ID_Town"></param>
        /// <param name="Address"></param>
        /// <param name="Phone"></param>
        /// <param name="Sms"></param>
        /// <param name="Digital"></param>
        /// <param name="Latitude"></param>
        /// <param name="Longitude"></param>
        /// <param name="IsActive"></param>
        // POST: api/Optic
        [HttpPost]
        public bool Post([FromBody] OpticModel item)
        {
            //OpticModel op = new OpticModel();

            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1)
                    {
                        try
                        {
                            OpticDBHandler iHandler = new OpticDBHandler();

                            return iHandler.insertItem(item);
                        }
                        catch { }

                    }
                }
                Response.StatusCode = 401;
            }
            return false;
        }


        /// <summary>
        /// api/Optic/5
        /// </summary>
        /// <param name="ID_Network"></param>
        /// <param name="ID_Network"></param>
        /// <param name="ID_Network"></param>
        /// <param name="ID_Network"></param>
        /// <param name="OpticName"></param>
        /// <param name="ID_User"></param>
        /// <param name="ID_Town"></param>
        /// <param name="Address"></param>
        /// <param name="Phone"></param>
        /// <param name="Sms"></param>
        /// <param name="Digital"></param>
        /// <param name="Latitude"></param>
        /// <param name="Longitude"></param>
        /// <param name="IsActive"></param>
        // PUT: api/Optic/5
        /*    [HttpPut ("{id}", Name = "PutOptic")]//
            public bool Put([FromBody] OpticModel item)
            {
               // OpticModel op = new OpticModel();
                try
                {              
                    OpticDBHandler iHandler = new OpticDBHandler();
                    return iHandler.UpdateItem(item);
                }
                catch { }
                return false;
            }*/

        /// <summary>
        /// api/ApiWithActions/5 - DELETE
        /// </summary>
        /// <param name="id"></param>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}", Name = "DELETEOptic")]
        public bool Delete(string id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1000)
                    {
                        OpticDBHandler iHandler = new OpticDBHandler();
                        if (id != null && id.Trim().Length > 0 && iHandler.DeleteItem(id))
                            return true;
                        else if (id != null && id.Trim().Length > 0 && iHandler.dbc.last_error.Trim().Length > 0)
                        {
                            return iHandler.dbc.last_error.Length == 0;
                        }
                    }
                }
                Response.StatusCode = 401;
            }
            return false;
        }

        [HttpPatch("{id}", Name = "PatchOptic")]
        public bool Patch(string id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        OpticDBHandler iHandler = new OpticDBHandler();
                        if (id != null && id.Trim().Length > 0)
                            return iHandler.DeleteItemCheck(id);
                        else if (id != null && id.Trim().Length > 0 && iHandler.dbc.last_error.Trim().Length > 0)
                        {
                            return iHandler.dbc.last_error.Trim().Length > 0;
                        }

                    }
                }
                Response.StatusCode = 401;
            }
            return false;
        }

    }
}
