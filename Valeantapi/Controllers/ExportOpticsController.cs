﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;
using Valeantapi.utils;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class ExportOpticsController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public FileResult Get(string sort = "ID_Optic", string order = "Desc", string NetworkName = "",
          string OpticTorgName = "", string OpticJuraName = "", string INN = "", string UserName = "",
          string KASName = "", string TownName = "", string Phone = "", string Email1 = "",
          string Email2 = "", string Email3 = "", string OnlineLink = "", string Kids = "",
          string SMS = "", string Digital = "", string Latitude = "", string Longitude = "",
          string Doc1 = "", string IsActive = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1)
                    {
                        OpticModel[] op = new OpticModel[0];
                        OpticDBHandler iHandler = new OpticDBHandler();;
                        iHandler.OpticTorgName = OpticTorgName;
                        iHandler.OpticJuraName = OpticJuraName;
                        iHandler.INN = INN;
                        iHandler.NetworkName = NetworkName;
                        iHandler.KASName = KASName;
                        iHandler.TownName = TownName;
                        iHandler.UserName = UserName;
                        iHandler.IsActive = IsActive;
                        iHandler.Doc1 = Doc1;
                        iHandler.Email1 = Email1;
                        iHandler.Email2 = Email2;
                        iHandler.Email3 = Email3;
                        iHandler.OnlineLink = OnlineLink;
                        iHandler.Kids1 = Kids;
                        iHandler.SMS = SMS;
                        iHandler.Digital = Digital;
                        iHandler.Latitude = Latitude;
                        iHandler.Longitude = Longitude;
                        iHandler.Phone = Phone;
                        iHandler.sort = sort;
                        iHandler.order = order;
                        List<OpticModel> bb = iHandler.GetItemListExcel();
                        if (bb.Count > 0)
                        {
                            excelwork ew = new excelwork();
                            string pp = "";
                            string path = ew.ExportOptics(bb, ref pp);
                            FileStream fs = new FileStream(path, FileMode.Open);
                            string file_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            string file_name = "export.xls";
                            return File(fs, file_type, file_name);
                        }
                    }
                    return null;
                }
                Response.StatusCode = 401;
            }
            return null;
        }
    }
}
