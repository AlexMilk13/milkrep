﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TryNewPassController : Controller
    {
        //Get api/TryNewPass
        [HttpGet]
        public bool Index(string Code = "", string pass = "")
        {
            if (Code.Length == 0) return false;
            LoginDBHandler log = new LoginDBHandler();
            return log.NewPass(Code, pass);

        }
    }
}
