﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class MediaFileController : Controller
    {


        [HttpGet]
        public FileResult Get(string ID_SKU = "0")
        {
            byte[] respo = new byte[0];
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        MediaFileModel op;// = new MediaFileModel();
                        MediaDBHandler iHandler = new MediaDBHandler();
                        op = iHandler.GetItem_byte(ID_SKU);

                        if (op!=null && op.Code!=null)
                        {
                            MemoryStream fs = new MemoryStream(op.mediafile);
                            string file_type = "image/jpeg";
                            string file_name = "export.jpg";
                            if (op.FileType == "PNG")
                            {
                                file_type = "image/png";
                                file_name = "export.png";
                            }
                            return File(fs, file_type, file_name);
                        }
                    }
                    MemoryStream fs2 = new MemoryStream(respo);
                    string file_type2 = "image/jpeg";
                    string file_name2 = "export.jpg";

                    return File(fs2, file_type2, file_name2);

                }

Response.StatusCode = 401;
            }


                MemoryStream fs1 = new MemoryStream(respo);
                string file_type1 = "image/jpeg";
                string file_name1 = "export.jpg";
                
                return File(fs1, file_type1, file_name1);
        }

        // GET api/MediaFile/5
        [HttpGet("{id}")]
        public MediaFileModel Get(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        MediaFileModel op;// = new MediaFileModel();
                        MediaDBHandler iHandler = new MediaDBHandler();
                        op = iHandler.GetItem(id.ToString());
                        return op;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return null;
        }

       
    }
}
