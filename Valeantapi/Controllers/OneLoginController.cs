﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;



namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OneLoginController : Controller
    {
        //Get api/OneLogin
        [HttpGet]
        public string Index(string Code = "")
        {
            if (Code.Length == 0) return "-1";
            LoginDBHandler log = new LoginDBHandler();
            string ee = log.GetInvitation(Code);
            if (ee.Length > 0)
                return ee;
            return "-1";
        }
    }
}
