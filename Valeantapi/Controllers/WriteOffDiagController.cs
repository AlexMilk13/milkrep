﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Valeantapi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class WriteOffDiagController : Controller
    {
        // GET: api/WriteOffDiag
        [HttpGet]
        public IEnumerable<WriteOfDiagfModel> Get(int count = 10, int page = 1, string OpticName = "", string NetworkName = "", string ObjectName = "", string Outgoing_Date = "", string SKU = "", string Diopter = "", string Amount = "",
            string sort = "Outgoing_Date", string order = "DESC", string ID_Diagnostic = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    WriteOffDBHandler iHandler = new WriteOffDBHandler();
                    iHandler.userrole = log.GetRole(hh);
                    iHandler.ui = log.UserInfo(hh);
                    if (iHandler.userrole > 0)
                    {
                        WriteOfDiagfModel[] op = new WriteOfDiagfModel[0];

                        iHandler.count = count;
                        iHandler.page = page;
                        iHandler.OpticName = OpticName;
                        iHandler.NetworkName = NetworkName;
                        iHandler.ObjectName = ObjectName;
                        iHandler.Outgoing_Date = Outgoing_Date;
                        iHandler.SKU = SKU;
                        iHandler.Diopter = Diopter;
                        iHandler.Amount = Amount;
                        iHandler.sort = sort;
                        iHandler.order = order;
                        iHandler.ID_Diagnostic = ID_Diagnostic;
                        List<WriteOfDiagfModel> bb = iHandler.GetItemDiagList();
                        if (bb.Count > 0)
                            op = bb.ToArray();
                        return op;
                    }
                }
                Response.StatusCode = 401;
            }
            return null;
        }

        // GET api/WriteOffDiag/5
        [HttpGet("{id}", Name = "GetWriteOffDiag")]
        public WriteOfDiagfModel Get(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        WriteOfDiagfModel op = new WriteOfDiagfModel();
                        WriteOffDBHandler iHandler = new WriteOffDBHandler();
                        op = iHandler.GetItemDiag(id.ToString());
                        return op;
                    }
                }
                Response.StatusCode = 401;
            }
            return null;
        }

        // POST api/WriteOffDiag
        [HttpPost]
        public bool Post([FromBody] WriteOfDiagfModel value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        WriteOffDBHandler iHandler = new WriteOffDBHandler();
                        return iHandler.InsertItemDiag(value);
                    }
                }
                Response.StatusCode = 401;
            }
            return false;
        }

        // PUT api/WriteOffDiag/5
        [HttpPut("{id}")]
        public bool Put(int id, [FromBody] WriteOfDiagfModel value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        WriteOffDBHandler iHandler = new WriteOffDBHandler();
                        return iHandler.UpdateItemDiag(value);
                    }
                }
                Response.StatusCode = 401;
            }
            return false;
        }

        // DELETE api/WriteOffDiag/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 9)
                    {
                        WriteOffDBHandler iHandler = new WriteOffDBHandler();
                        if (id != null && id > 0 && iHandler.DeleteItem(id))
                            return true;
                        else
                            return false;
                    }
                }
                Response.StatusCode = 401;
            }
            return false;

        }
    }
}
