﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        // GET: api/Users
        [HttpGet]
        public IEnumerable<UserModel> Get(int count = 10, int page = 1, string RoleName = "", string Login = "", string Email = "", string LastDate = "", string IsActive = "", string Name = "", string Surname = "",
           string Patronymic = "" , string sort = "[LastDate]", string order = "DESC", string FIO = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        UserModel[] op = new UserModel[0];
                        UserDBHandler iHandler = new UserDBHandler();
                        iHandler.Name = Name;
                        iHandler.Surname = Surname;
                        iHandler.Patronymic = Patronymic;
                        iHandler.count = count;
                        iHandler.page = page;
                        iHandler.RoleName = RoleName;
                        iHandler.Login = Login;
                        iHandler.Email = Email;
                        iHandler.UserName = FIO;
                        iHandler.LastDate = LastDate;
                        iHandler.hash = log.hash;
                        iHandler.IsActive = IsActive;
                        iHandler.sort = sort;
                        iHandler.order = order;
                        List<UserModel> bb = iHandler.GetItemList();
                        if (bb.Count > 0)
                            op = bb.ToArray();
                        return op;
                    }
                    else
                        return null;
                }
                else
                {
                    Response.StatusCode = 401;
                    return null;

                }
            }
            else
                return null;

        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "GetUserOne")]
        public UserModel Get(int id, bool isOptic = false)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                }
                else
                {
                    Response.StatusCode = 401;
                    return null;

                }
            }
            else
                return null;
            UserModel op = new UserModel();
            UserDBHandler iHandler = new UserDBHandler();
            iHandler.hash = log.hash;
            return iHandler.GetItem(id.ToString(), isOptic);
        }

        // POST: api/Users
        [HttpPost]
        public int Post([FromBody] UserModel value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        UserDBHandler iHandler = new UserDBHandler();
                        iHandler.hash = log.hash;
                        return iHandler.NewInsertUser(value);
                    }
                }
                else
                {
                    Response.StatusCode = 401;
                    return -1;
                }
            }
            else
                return -1;
            
            
            return -1;
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public bool Put(int id, bool isOptic = false)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        UserDBHandler iHandler = new UserDBHandler();
                        iHandler.hash = log.hash;
                        string login = isOptic ? "" : "h";
                        string opic = isOptic ? "Оптика" : " ";
                        return iHandler.NewCheckDeleteUser(new UserModel { ID_User = id, Login = login, RoleName = opic });
                    }
                }
                else
                {
                    Response.StatusCode = 401;
                    return false;
                }
            }

            return false;


        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id, bool isOptic = false)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        UserDBHandler iHandler = new UserDBHandler();
                        iHandler.hash = log.hash;
                        string login = isOptic ? "" : "h";
                        string opic = isOptic ? "Оптика" : " ";
                        return iHandler.NewDeleteUser(new UserModel { ID_User = id, Login = login, RoleName = opic });
                    }
                }
                else
                {
                    Response.StatusCode = 401;
                    return false;
                }
            }

                return false;
        }
    }
}
