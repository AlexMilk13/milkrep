﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WriteOffController : ControllerBase
    {
        // GET: api/WriteOff
        [HttpGet]
        public IEnumerable<WriteOffModel> Get(int count = 10, int page = 1, string OpticName = "", string NetworkName = "", string ObjectName = "", string Outgoing_Date = "", string SKUName = "", string Diopter = "", string Amount = "", string DocName = "",
            string sort = "Outgoing_Date", string order = "DESC")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    WriteOffDBHandler iHandler = new WriteOffDBHandler();
                    iHandler.userrole = log.GetRole(hh);
                    iHandler.ui = log.UserInfo(hh);
                    if (iHandler.userrole > 0)
                    {
                        WriteOffModel[] op = new WriteOffModel[0];
                        
                        iHandler.count = count;
                        iHandler.page = page;
                        iHandler.OpticName = OpticName;
                        iHandler.NetworkName = NetworkName;
                        iHandler.ObjectName = ObjectName;
                        iHandler.Outgoing_Date = Outgoing_Date;
                        iHandler.SKU = SKUName;
                        iHandler.Diopter = Diopter;
                        iHandler.Amount = Amount;
                        iHandler.sort = sort;
                        iHandler.order = order;
                        iHandler.DocName = DocName;
                        List<WriteOffModel> bb = iHandler.GetItemList();
                        if (bb.Count > 0)
                            op = bb.ToArray();
                        return op;
                    }
                }
                Response.StatusCode = 401;
            }
            return null;
        }

        // GET: api/WriteOff/5
        [HttpGet("{id}", Name = "GetWriteOff")]
        public WriteOffModel Get(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        WriteOffModel op = new WriteOffModel();
                        WriteOffDBHandler iHandler = new WriteOffDBHandler();
                        op = iHandler.GetItem(id.ToString());
                        return op;
                    }
                }
                Response.StatusCode = 401;
            }
            return null;
        }

        // POST: api/WriteOff
        [HttpPost]
        public bool Post([FromBody] WriteOffModel value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        WriteOffDBHandler iHandler = new WriteOffDBHandler();
                        return iHandler.InsertItem(value);
                    }
                }
                Response.StatusCode = 401;
            }
            return false;
        }

        // PUT: api/WriteOff/5
        [HttpPut("{id}")]
        public bool Put(int id, [FromBody] WriteOffModel value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        WriteOffDBHandler iHandler = new WriteOffDBHandler();
                        return iHandler.UpdateItem(value);
                    }
                }
                Response.StatusCode = 401;
            }
            return false;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 9)
                    {
                        WriteOffDBHandler iHandler = new WriteOffDBHandler();
                        if (id != null && id > 0 && iHandler.DeleteItem(id))
                            return true;
                        else
                            return false;
                    }
                }
                Response.StatusCode = 401;
            }
            return false;

        }
    }
}
