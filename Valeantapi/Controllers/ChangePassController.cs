﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChangePassController : ControllerBase
    {
        //Get api/ChangePass
        [HttpGet]
        public bool Get(string pass = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            log.Req = Request;
            log.resp = Response;
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    int yy = log.GetRole(hh);
                    log.changePass(pass, yy);

                    log.RenewCookies();
                    return true;
                }
                else
                {
                    Response.StatusCode = 401;
                    
                }
            }
            return false;
        }
    }
}