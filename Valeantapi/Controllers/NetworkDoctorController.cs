﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NetworkDoctorController : ControllerBase
    {
        // GET: api/NetworkDoctor
        [HttpGet]
        public IEnumerable<NetworksDoctors> Get(int id, string IsActive = "1")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {

                        NetworksDoctors[] OptDocs = new NetworksDoctors[0];
                        OpticDBHandler iHandler = new OpticDBHandler();
                        if (IsActive.Trim().Length > 0 && (IsActive.Trim().ToLower() == "true" || IsActive.Trim().ToLower() == "false"))
                            iHandler.IsActive = IsActive;
                        List<NetworksDoctors> bb = iHandler.GetAllNetworkDoctors(id);
                        if (bb.Count > 0)
                            OptDocs = bb.ToArray();
                        return OptDocs;

                    }
                }
                Response.StatusCode = 401;
            }
            return null;
        }



        // POST: api/NetworkDoctor
        [HttpPost]
        public string Post([FromBody] NetworksDoctors item)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        OpticDBHandler iHandler = new OpticDBHandler();
                        //  return "Done";
                        if (iHandler.AddNetworkDoctor(item.ID_Doc.ToString(), item.id_Network.ToString()))
                            return "ok";
                        return "";
                    }
                }
                Response.StatusCode = 401;
            }
            return null;
        }




        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        public string DeleteNetworkDoctor(int id_Network, int ID_Doc)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        OpticDBHandler iHandler = new OpticDBHandler();

                        if (iHandler.DeleteNetworkDoctor(ID_Doc.ToString(), id_Network.ToString()))
                            return "ok";
                        else
                            return "";
                    }
                }
                Response.StatusCode = 401;
            }
            return null;
        }


        // PUT: api/Diagnostics/5
        [HttpPut]
        public IEnumerable<NetworksModel> Put(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) >= 1000)
                    {
                        NetworksModel[] OptDocs = new NetworksModel[0];
                        NetworksDBHandler iHandler = new NetworksDBHandler();
                        
                        List<NetworksModel> bb = new List<NetworksModel>();
                        bb = iHandler.GetNetworks_doc(id);
                        if (bb.Count > 0)
                            OptDocs = bb.ToArray();
                        return OptDocs;
                    }
                    return null;
                }
                Response.StatusCode = 401;
            }
            return null;
        }
    }
}
/*[19.11 2:10] Сергей Фарафонов
    

получить список Сетей врача: 

PUT: api/NetworkDoctor?id= 44444 - ID врача

​[19.11 2:11] Сергей Фарафонов
    [
    {​​​​​​​
        "iD_Network": 3862,
        "code": "_Айкрафт (франчайзи)_Филоненко В.В.",
        "networkName": "_Айкрафт (франчайзи)_Филоненко В.В.",
        "isActive": true,
        "inn": null,
        "pagecount": 1
    }​​​​​​​
] - такая вот структура придет (или несколько таких)
​[19.11 2:14] Сергей Фарафонов
    типа:
[
    {​​​​​​​
        "iD_Network": 3961,
        "code": "Народная оптика ООО_Народная оптика",
        "networkName": "Народная оптика ООО_Народная оптика",
        "isActive": true,
        "inn": null,
        "pagecount": 2
    }​​​​​​​,
    {​​​​​​​
        "iD_Network": 3986,
        "code": "",
        "networkName": "Оптик-Вижн ООО_Очкарик",
        "isActive": true,
        "inn": null,
        "pagecount": 2
    }​​​​​​​
] --это для api/NetworkDoctor?id=44993
*/
