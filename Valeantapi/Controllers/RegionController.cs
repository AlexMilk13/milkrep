﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegionController : ControllerBase
    {
        // GET: api/Region
        [HttpGet]
        public IEnumerable<RegionModel> Get()
        {
            RegionModel[] OptDocs = new RegionModel[0];
            RegionDBHandler iHandler = new RegionDBHandler();
            List<RegionModel> bb = new List<RegionModel>();
            bb = iHandler.GetRegions();
            if (bb.Count > 0)
                OptDocs = bb.ToArray();
            return OptDocs;
        }

        // GET: api/Region/5
        [HttpGet("{id}", Name = "GetRegion")]
        public RegionModel Get(int id)
        {
            //"1KGBS4VXBIO" --KAM
            RegionModel OptDocs = new RegionModel();
            RegionDBHandler iHandler = new RegionDBHandler();

            OptDocs = iHandler.GetOneRegion(id.ToString());

            return OptDocs;
        }

        // POST: api/Region
        [HttpPost]
        public bool Post([FromBody] RegionModel value)
        {
            try
            {
                if (value != null)
                {
                    RegionDBHandler iHandler = new RegionDBHandler();
                    return iHandler.insertItem(value);
                }
            }
            catch { return false; }
            return true;
        }



        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            try
            {
                RegionDBHandler iHandler = new RegionDBHandler();
                return iHandler.DeleteRegion(id.ToString());
            }
            catch { return false; }
        }
    }
}
