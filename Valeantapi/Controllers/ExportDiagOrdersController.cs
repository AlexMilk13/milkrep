﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;
using Valeantapi.utils;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class ExportDiagOrdersController : Controller
    {
        // GET: api/ExportDiagOrders
        [HttpGet]
        public FileResult Get(DateTime DateBegin, DateTime DateEnd, string Opticname)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    int role = log.GetRole(hh);
                    int level = log.GetRole(hh);
                    if (role > 0)
                    {
                        DiagnosticsDBHandler iHandler = new DiagnosticsDBHandler();
                        if (role < 10)
                        {
                            iHandler.restrict = @" AND [ID_Optic] = (SELECT [valueint] FROM [dbo].[loginaudit] WHERE [IsActive] = 1 AND [session_hash] = '" + hh + "')";
                            if(Opticname == "")
                                iHandler.restrict = @" AND [ID_Optic] IN (SELECT ou.[ID_Optic] FROM [dbo].[optics] ou WHERE ou.IsActive = 1 AND ou.ID_Network IN
(SELECT oo1.[ID_Network] FROM [dbo].[optics] oo1 WHERE oo1.[IsActive] = 1 AND oo1.ID_Optic = (SELECT [valueint] FROM [dbo].[loginaudit] WHERE [IsActive] = 1 AND [session_hash] = '" + hh + "')))";

                        }
                        else if (role < 10000)
                        {
                            int id_user = log.UserInfo(hh).id_User;
                            if (role < 1000 && role>=10)//kam/kas
                                iHandler.restrict = @" AND [ID_Optic] IN (SELECT [ID_Optic]  FROM [dbo].[OpticsView] WHERE [ID_KAS] = " + id_user.ToString() + ")";
                            else
                                iHandler.restrict = @" AND [ID_Optic] IN (SELECT [ID_Optic]  FROM [dbo].[OpticsView] ov WHERE [ID_User] = " + id_user.ToString() + ")";
                        }
                        List<DiagnosticsOrder> bb = new List<DiagnosticsOrder>();
                        bb = iHandler.GetItemListOrders(DateBegin, DateEnd, Opticname);
                        if (bb != null)
                        {
                            excelwork ew = new excelwork();
                            string pp = "";
                            string path = ew.ExportDiagnosticsOrder(bb, ref pp);
                            FileStream fs = new FileStream(path, FileMode.Open);
                            string file_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            string file_name = "export.xls";
                            return File(fs, file_type, file_name);
                        }
                    }
                    
                }
                Response.StatusCode = 401;
            }
            byte[] respo = new byte[0];
            MemoryStream fs1 = new MemoryStream(respo);
            string file_type1 = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            string file_name1 = "export.xls";
            return File(fs1, file_type1, file_name1);
        }
    }
}
