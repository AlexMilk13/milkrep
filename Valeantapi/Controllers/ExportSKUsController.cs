﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;
using Valeantapi.utils;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class ExportSKUsController : Controller
    {
        // GET: api/ExportSKUs
        [HttpGet]
        public FileResult Get(string sort = "", string order = "", string Code = "", string SkuName = "", string Diopter = "", string Description = "", string StoreCount = "", string Store_Name = "", string SKU_ID = "", string IsActive = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) >= 10)
                    {

                        SKUModel[] op = new SKUModel[0];
                        SKUDBHandler iHandler = new SKUDBHandler();
                        iHandler.Code = Code;
                        iHandler.SkuName = SkuName;
                        iHandler.Diopter = Diopter;
                        iHandler.Description = Description;
                        iHandler.StoreCount = StoreCount;
                        iHandler.Store_Name = Description;
                        iHandler.SKU_ID = SKU_ID;
                        List<SKUModel> bb = new List<SKUModel>();
                        bb = iHandler.GetItemListExcel();
                        if (bb.Count > 0)
                        {
                            excelwork ew = new excelwork();
                            string pp = "";
                            string path = ew.ExportSKUs(bb, ref pp);
                            FileStream fs = new FileStream(path, FileMode.Open);
                            string file_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            string file_name = "export.xls";
                            return File(fs, file_type, file_name);
                        }
                    }
                    return null;
                }
                Response.StatusCode = 401;
            }
            return null;
        }
    }
}
