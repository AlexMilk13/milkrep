﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/OpticDoctor")]
    [ApiController]
    public class OpticDoctorController : ControllerBase
    {

        // GET: api/OpticDoctor/5
        [HttpGet("{id}", Name = "GetDoctor")]
        public IEnumerable<OpticDoctors> Get(int id, string IsActive = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {

                        OpticDoctors[] OptDocs = new OpticDoctors[0];
                        OpticDBHandler iHandler = new OpticDBHandler();
                        if (IsActive.Trim().Length > 0 && (IsActive.Trim().ToLower() == "true" || IsActive.Trim().ToLower() == "false"))
                            iHandler.IsActive = IsActive;
                        List<OpticDoctors> bb = iHandler.GetAllOpticDoctors(id);
                        if (bb.Count > 0)
                            OptDocs = bb.ToArray();
                        return OptDocs;

                    }
                }
                Response.StatusCode = 401;
            }
            return null;
        }

        // POST: api/OpticDoctor
        [HttpPost]
        public string Post([FromBody] OpticDoctors item)
        {
            OpticDBHandler iHandler = new OpticDBHandler();
          //  return "Done";
            iHandler.AddOpticDoctor(item.ID_Doc.ToString(),item.id_Optic.ToString());
            return "";
        }


        // DELETE: api/OpticDoctor/5
        [HttpDelete]
        public string DeleteOpticDoctor([FromBody] OpticDoctors item)
        {
            OpticDBHandler iHandler = new OpticDBHandler();
            //iHandler.DeleteOpticDoctor(idd.ToString(),ido.ToString());
            if (iHandler.DeleteOpticDoctor(item.ID_Doc.ToString(), item.id_Optic.ToString())) 
                return "ok";
            else
                return "";
        }
    }
}
