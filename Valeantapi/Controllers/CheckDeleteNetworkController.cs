﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckDeleteNetworkController : Controller
    {
        //GET: CheckDeleteNetwork
        [HttpGet("{id}")]
        public bool Get(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        NetworksDBHandler iHandler = new NetworksDBHandler();
                        NetworksModel nm = new NetworksModel();
                        nm.ID_Network = id;
                        return iHandler.NewCheckDeleteNetwork(nm);
                    }
                }
                else
                {
                    Response.StatusCode = 401;
                    return false;
                }
            }
            return false;
        }
    }
}
