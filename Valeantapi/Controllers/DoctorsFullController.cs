﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorsFullController : ControllerBase
    {
        // GET: api/DoctorsFull
        [HttpGet]
        public IEnumerable<OpticDoctorsFull> Get(int count = 10, int page = 1, string OpticName = "", string OpticAddress = "", string DocName = "", string DocSurname = "", string DocPatronymic = "", string IsActive = "", string sort = "ID_User", string order = "DESC")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1)
                    {

                        OpticDoctorsFull[] op = new OpticDoctorsFull[0];
                        DoctorsFullDBHandler iHandler = new DoctorsFullDBHandler();
                        iHandler.count = count;
                        iHandler.page = page;
                        iHandler.OpticAddress = OpticAddress;
                        iHandler.OpticName = OpticName;
                        iHandler.DocName = DocName;
                        iHandler.DocSurname = DocSurname;
                        iHandler.DocPatronymic = DocPatronymic;
                        iHandler.IsActive = IsActive;
                        iHandler.sort = sort;
                        iHandler.order = order;
                        List<OpticDoctorsFull> bb = iHandler.GetItemList();
                        if (bb.Count > 0)
                            op = bb.ToArray();
                        return op;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return null;
        }

        // GET: api/DoctorsFull/5
        [HttpGet("{id}", Name = "GetDoctorsFull")]
        public OpticDoctorsFull Get(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1)
                    {
                        OpticDoctorsFull op = new OpticDoctorsFull();
                        DoctorsFullDBHandler iHandler = new DoctorsFullDBHandler();
                        op = iHandler.GetItem(id.ToString());
                        return op;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return null;
        }

        // POST: api/DoctorsFull
        [HttpPost]
        public string Post([FromBody] OpticDoctorsFull value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1)
                    {
                        try
                        {
                            DoctorsFullDBHandler iHandler = new DoctorsFullDBHandler();
                            if (iHandler.insertItem(value))
                                return "ok";
                            else return "";
                        }
                        catch { }
                        return "";
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return "";
        }

        // PUT: api/DoctorsFull/5
        [HttpPut("{id}", Name = "PutOpticDocFull")]
        public bool Put([FromBody] OpticDoctorsFull value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1)
                    {
                        try
                        {
                            DoctorsFullDBHandler iHandler = new DoctorsFullDBHandler();
                            return iHandler.UpdateItem(value);
                        }
                        catch { }
                        return false;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return false;
        }

        // DELETE: api/DoctorsFull/5
        [HttpDelete("{id}")]
        public bool Delete(int id, int idopt)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1)
                    {
                        DoctorsFullDBHandler iHandler = new DoctorsFullDBHandler();
                        if (id != null && id > 0 && iHandler.DeleteItem(id, idopt))
                            return true;
                        else
                            return false;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return false;

        }

        [HttpPatch("{id}", Name = "PatchFullDocs")]
        public bool Patch(string id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1)
                    {
                        DoctorsFullDBHandler iHandler = new DoctorsFullDBHandler();
                        if (id != null && id.Trim().Length > 0)
                            return iHandler.DeleteItemCheck(id);
                        else if (id != null && id.Trim().Length > 0 && iHandler.dbc.last_error.Trim().Length > 0)
                        {
                            return iHandler.dbc.last_error.Trim().Length > 0;
                        }
                        else
                            return false;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return false;
        }
    }
}
