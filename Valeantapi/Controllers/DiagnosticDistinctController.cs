﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiagnosticDistinctController : ControllerBase
    {


        // GET: api/DiagnosticDistinct
        [HttpGet]
        public IEnumerable<string> Get(string item = "")
        {
            string[] tt = new string[0];
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        if (item.Trim().Length > 0)
                            tt = (new DiagnosticsDBHandler()).GetDistinctList(item).ToArray();
                    }
                    return tt;
                }
                Response.StatusCode = 401;
            }
            return tt;
        }
    }
}