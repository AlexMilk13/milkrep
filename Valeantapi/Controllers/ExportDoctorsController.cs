﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using Valeantapi.Models;
using Valeantapi.utils;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class ExportDoctorsController : Controller
    {
        // GET: api/ExportDoctors
        [HttpGet]
        public FileResult Get(string OpticName = "", string OpticAddress = "", string DocName = "", string DocSurname = "", string DocPatronymic = "", string IsActive = "", string sort = "", string order = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1)
                    {                        
                        DoctorsFullDBHandler iHandler = new DoctorsFullDBHandler();
                        iHandler.OpticName = OpticName;
                        iHandler.DocName = DocName;
                        iHandler.DocSurname = DocSurname;
                        iHandler.DocPatronymic = DocPatronymic;
                        iHandler.IsActive = IsActive;
                        iHandler.sort = sort;
                        iHandler.order = order;
                        List<OpticDoctorsFull> bb = new List<OpticDoctorsFull>();
                        bb = iHandler.GetItemListExcel();
                        if (bb.Count > 0)
                        {
                            excelwork ew = new excelwork();
                            string pp = "";
                            string path = ew.ExportDoctors(bb, ref pp);
                            FileStream fs = new FileStream(path, FileMode.Open);
                            string file_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            string file_name = "export.xls";
                            return File(fs, file_type, file_name);
                        }
                    }
                    return null;
                }
                Response.StatusCode = 401;
            }
            return null;
        }
    }
}
