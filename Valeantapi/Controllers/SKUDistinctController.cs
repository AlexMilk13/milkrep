﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SKUDistinctController : ControllerBase
    {

        [HttpGet]
        public IEnumerable<string> Get(string item = "", string IsBausch = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1)
                    {
                        string[] tt = new string[0];
                        if (item.Trim().Length > 0)
                        {
                            SKUDBHandler iHandler = new SKUDBHandler();
                            iHandler.IsBausch = IsBausch;
                            tt = iHandler.GetDistinctList(item).ToArray();
                           
                        } 
                        return tt;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return null;
        }
    }
}