﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class LoginIntController : Controller
    {

        // GET: api/LoginInt
        [HttpGet]
        public int Get(string login = "", string pass = "", string Code = "", string rememb = "")
        {
            Response.Cookies.Delete("UserName");
            Response.Cookies.Delete("UhashString");
            Response.Cookies.Delete("expires");
            Response.Cookies.Delete("Opt");

            LoginDBHandler log = new LoginDBHandler();

            log.Code = Code;
            log.login = login;
            log.pass = pass;
            log.Req = Request;

            if (rememb.Trim().Length > 0)
            {
                log.expires = DateTime.Now.AddYears(1);
                log.rememb = true;
            }
            else
            {
                log.expires = DateTime.Now.AddHours(1);
                log.rememb = false;
            }
            int respo = log.TryToLoginInt();
            Response.Cookies.Append("UhashString", log.idoptic);
            return respo;

        }
    }
}
