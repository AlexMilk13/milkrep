﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class AddrController : Controller
    {
        // GET: api/Addr
        [HttpGet("{id}")]
        public IEnumerable<AddrModel> Get(int id)
        {
            List<AddrModel> iList = new List<AddrModel>();
            AddrDBHandler iHandler = new AddrDBHandler();
            return iHandler.GetItemList(id.ToString());
        }

        // GET api/<controller>/5
/*        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
    }
}
