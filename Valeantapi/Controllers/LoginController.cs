﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        // GET: api/Login
        [HttpGet]
        public bool Get(string login = "", string pass = "", string Code = "", string rememb = "")
        {
            Response.Cookies.Delete("UserName");
            Response.Cookies.Delete("UhashString");
            Response.Cookies.Delete("expires");
            Response.Cookies.Delete("Opt");
            
            LoginDBHandler log = new LoginDBHandler();
            
            log.Code = Code;
            log.login = login;
            log.pass = pass;
            log.Req = Request;

            if (rememb.Trim().Length > 0)
            {
                log.expires = DateTime.Now.AddYears(1);
                log.rememb = true;
            }
            else
            {
                log.expires = DateTime.Now.AddHours(1);
                log.rememb = false;
            }
            if (log.TryToLogin())
            {
                //log.setUpResponce();

                if (Response != null)
                {
                   // CookieOptions rr = new CookieOptions();
                    
                    if (log.idoptic.Length > 0)
                    {
                        Response.Cookies.Append("UhashString", log.idoptic);
                        return true;
                    }
                }
                
            }
            return false;
        }


        // PUT: api/Login
        [HttpPut]
        public bool Put(string login = "", string pass = "", string Code = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (login == "")
                pass = Code;
            log.login = login;
            log.pass = pass;
            log.resp = Response;
            log.Req = Request;
            log.RenewCookies();
            if (log.CheckLogged())
                    return true;
           
            return false;
        }
        


        // DELETE: api/Login
        [HttpDelete]
        public bool Delete()
        {
            LoginDBHandler log = new LoginDBHandler();
            log.Req = Request;
            log.DisableCookies();
            Response.Cookies.Delete("UserName");
            Response.Cookies.Delete("UhashString");
            Response.Cookies.Delete("expires");
            Response.Cookies.Delete("Opt");
            return true;
        }
    }
}
