﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NetworksController : ControllerBase
    {
        // GET: api/Networks
        [HttpGet]
        public IEnumerable<NetworksModel> Get(int count = 10, int page = 1, string NetworkName = "", string IsActive = "", string INN = "", string sort = "ID_Network ", string order = "DESC")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    int level = log.GetRole(hh);
                    if (level >= 10)
                    {
                        
                        NetworksModel[] OptDocs = new NetworksModel[0];
                        NetworksDBHandler iHandler = new NetworksDBHandler();
                        iHandler.IsActive = IsActive;
                        iHandler.sort = sort;
                        iHandler.order = order;
                        iHandler.count = count;
                        iHandler.page = page;
                        iHandler.INN = INN;
                        iHandler.NetworkName = NetworkName;
                        if (level >= 10/*TM*/ && level < 10000 /*not adm*/)
                        {
                            UserInfoModel ui = log.UserInfo(hh);
                            if (ui.id_User > 0)
                                iHandler.UserID = ui.id_User.ToString();
                            iHandler.level = level;
                        }
                        List<NetworksModel> bb = new List<NetworksModel>();
                        bb = iHandler.GetNetworks();
                        if (bb.Count > 0)
                            OptDocs = bb.ToArray();
                        return OptDocs;
                    }
                    return null;
                }
                Response.StatusCode = 401;
            }
            return null;
        }

        // GET: api/Networks/5
        [HttpGet("{id}", Name = "GetNetwork")]
        public NetworksModel Get(int id)
        {
            
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        NetworksModel OptDocs = new NetworksModel();
                        NetworksDBHandler iHandler = new NetworksDBHandler();

                        OptDocs = iHandler.GetOneNetwork(id.ToString());

                        return OptDocs;
                    }
                    return null;
                }
                Response.StatusCode = 401;
            }
            return null;
        }

        // POST: api/Networks
        [HttpPost]
        public bool Post([FromBody] NetworksModel item)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        try
                        {
                            NetworksDBHandler iHandler = new NetworksDBHandler();
                            if (iHandler.insertItem(item))
                                return true;
                            else return false;
                        }
                        catch { }
                        return false;
                    }
                    return false;
                }
                Response.StatusCode = 401;
            }
            return false;
        }

        // PUT: api/Networks/5
        [HttpPut("{id}", Name = "PutOpticNetw")]
        public bool Put([FromBody] NetworksModel item)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        try
                        {
                            NetworksDBHandler iHandler = new NetworksDBHandler();
                            return iHandler.UpdateNetwork(item);
                        }
                        catch { }
                        return false;
                    }
                    return false;
                }
                Response.StatusCode = 401;
            }
            return false;
        }

        // DELETE: api/Networks/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        try
                        {
                            NetworksDBHandler iHandler = new NetworksDBHandler();
                            return iHandler.DeleteNetwork(id.ToString());
                        }
                        catch { return false; }
                    }
                    return false;
                }
                Response.StatusCode = 401;
            }
            return false;
        }
    }
}
