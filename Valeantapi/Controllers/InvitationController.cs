﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;
using Valeantapi.utils;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
   
        
        public class InvitationController : Controller
    {
        // GET: api/Invitation
        [HttpGet]
        public bool Index(int iduser = 0)
        {
            if (iduser <= 0) return false;
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (hh.Length > 0 && log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if(log.GetRole(hh) == 10000)
                    {
                        Email email = new Email();
                        if (email.Send_invitation_mindbox(iduser.ToString()).Length > 0)
                            return false;
                        else
                            return true;
                    }
                }
                else if(!log.CheckLogin(hh))
                {
                    Response.StatusCode = 401;
                    return false;
                }
            }
            return false;
        }
    }
}
