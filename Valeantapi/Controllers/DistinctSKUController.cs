﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class DistinctSKUController : Controller
    {
        // GET: api/DiagnosticDistinct
        [HttpGet]
        public IEnumerable<string> Get(string item = "", string IsBausch = "")
        {
            string[] tt = new string[0];
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();

                    if (item.Trim().Length > 0)
                    {
                        SKUDBHandler iHandler = new SKUDBHandler();
                        iHandler.IsBausch = IsBausch;
                        tt = iHandler.GetDistinctList(item).ToArray();
                    }
                    return tt;
                }
                Response.StatusCode = 401;
            }
            return tt;
        }
    }
}
