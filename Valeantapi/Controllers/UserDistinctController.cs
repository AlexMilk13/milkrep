﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserDistinctController : ControllerBase
    {
        // GET: api/UserDistinct
        [HttpGet]
        public IEnumerable<string> Get(string item = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                log.resp = Response;
                log.Req = Request;
                bool res = log.CheckLogin(hh);
                if (res && log.RenewCookies())
                {
                    string[] tt = new string[0];
                    if (item.Trim().Length > 0)
                        tt = (new UserDBHandler()).GetDistinctList(item).ToArray();
                    return tt;
                }
                else if(!res)
                {
                    Response.StatusCode = 401;
                    return null;
                }
            }
                return null;
        }
    }
}