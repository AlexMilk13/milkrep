﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserRoleController : ControllerBase
    {
        // GET: api/UserRole
        public IEnumerable<UserModel> Get(string Code)
        {
            //"1KGBS4VXBIO" --KAM
            UserModel[] OptDocs = new UserModel[0];
            UserDBHandler iHandler = new UserDBHandler();
            List<UserModel> bb = new List<UserModel>();
            if(Code != null && Code.Trim().Length>0)
                bb = iHandler.GetUsersOnRole(Code);
           

            if (bb.Count > 0)
                OptDocs = bb.ToArray();
            return OptDocs;
        }



        // POST: api/UserRole
        [HttpPost]
        public int Post([FromBody] UserModel value)
        {
            try
            {
                if (value != null)
                {
                    UserDBHandler iHandler = new UserDBHandler();
                    return iHandler.insertItem(value);
                }
            }
            catch { return 0; }
            return 0;
        }

        // PUT: api/UserRole/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/AUserRole/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            try
            {
                UserDBHandler iHandler = new UserDBHandler();
                return iHandler.DeleteUser(id.ToString());
            }
            catch { return false; }
        }
    }
}
