﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using Valeantapi.Models;
using Valeantapi.utils;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class ExportNetworksController : Controller
    {
        // GET: api/ExportNetworks
        [HttpGet]
        public FileResult Get(string NetworkName = "", string IsActive = "", string INN = "", string sort = "", string order = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) == 10000)
                    {
                        
                        NetworksDBHandler iHandler = new NetworksDBHandler();
                        iHandler.sort = sort;
                        iHandler.order = order;
                        iHandler.INN = INN;
                        iHandler.NetworkName = NetworkName;
                        List<NetworksModel> bb = new List<NetworksModel>();
                        bb = iHandler.GetNetworksExcel();
                        if (bb.Count > 0)
                        {
                            excelwork ew = new excelwork();
                            string pp = "";
                            string path = ew.ExportNetworks(bb, ref pp);
                            FileStream fs = new FileStream(path, FileMode.Open);
                            string file_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            string file_name = "export.xls";
                            return File(fs, file_type, file_name);                            
                        }
                    }
                    return null;
                }
                Response.StatusCode = 401;
            }
            return null;
        }
    }
}
