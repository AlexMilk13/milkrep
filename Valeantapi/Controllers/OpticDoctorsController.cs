﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OpticDoctorsController : ControllerBase
    {
        // GET: api/OpticDoctors
        [HttpGet]
        public IEnumerable<OpticDoctors> Get(string item = "", string IsActive = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        OpticDoctors[] OptDocs = new OpticDoctors[0];
                        OpticDBHandler iHandler = new OpticDBHandler();
                        List<OpticDoctors> bb = new List<OpticDoctors>();
                        if (IsActive.Trim().Length > 0 && (IsActive.Trim().ToLower() == "true" || IsActive.Trim().ToLower() == "false"))
                            iHandler.IsActive = IsActive;
                        if (item.Trim().Length > 0)
                            bb = iHandler.GetDoctors(item);

                        if (bb.Count > 0)
                            OptDocs = bb.ToArray();
                        return OptDocs;
                    }
                }
                Response.StatusCode = 401;

            }
            return null;
        }
    }
}
