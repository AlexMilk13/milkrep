﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;
using Valeantapi.utils;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MinboxController : ControllerBase
    {
        // GET: api/Minbox
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Minbox/5
        [HttpGet("{id}", Name = "GetCheckCode")]
        public async Task<mindboxModel> GetAsync(string id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        UserInfoModel uim = log.UserInfo(hh);


                                                Mindbox mb = new Mindbox();
                        mb.iduser = uim.id_User;
                        if (await mb.OnPostCertificate(id) != null)
                        {                            
                            return mb.md1;
                        }


                    }
                    return null;
                }
                Response.StatusCode = 401;
            }
            return null;
        }

        // POST: api/Minbox
        [HttpPost]
        public async Task<int> PostAsync([FromBody] mindboxModel value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        Mindbox iHandler = new Mindbox();
                        if(await iHandler.OnPostCertificateStart(value)>0)
                        {
                            return (int)iHandler.md1.ID;
                        }
                    }
                }
               // Response.StatusCode = 401;
            }
            return 0;
        }

        // PUT: api/Minbox/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
