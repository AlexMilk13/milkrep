﻿using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetRoleController : ControllerBase
    {
        // GET: api/GetRole
        [HttpGet]
        public int Get()
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (hh.Length > 0 && log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    return log.GetRole(hh);
                }                
            }
            Response.StatusCode = 401;
            return 0;
        }
    }
}