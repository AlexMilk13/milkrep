﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiagnosticsController : ControllerBase
    {
        // GET: api/Diagnostics
        [HttpGet]
        public IEnumerable<DiagnosticsModel> Get(int count = 10, int page = 1, string sort = "ID_Diagnostic", string order = "Desc", string CodeMindBox = "", string Opt_Net_Name = "", string Opt_Name = "", string Client_FIO = "", string DoctorName = "", 
            string DateBegin = "", string DateEnd = "")
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    int role = log.GetRole(hh);
                    if (role > 0)
                    {
                        
                            DiagnosticsModel[] op = new DiagnosticsModel[0];
                        DiagnosticsDBHandler iHandler = new DiagnosticsDBHandler();
                        if (role < 10)
                            iHandler.restrict = @" AND [ID_Optic] = (SELECT [valueint] FROM [dbo].[loginaudit] WHERE [IsActive] = 1 AND [session_hash] = '" + hh + "')";
                        if (role >= 10 && role < 1000)
                        {
                            UserInfoModel inf = log.UserInfo(hh);
                            iHandler.restrict = " AND ID_Optic IN (SELECT [ID_Optic]  FROM [dbo].[OpticsView] WHERE ID_KAS = " + inf.id_User + ")";
                        }
                            iHandler.count = count;
                        iHandler.page = page;
                        iHandler.sort = sort;
                        iHandler.order = order;
                        iHandler.Opt_Net_Name = Opt_Net_Name;
                        iHandler.CodeMindBox = CodeMindBox;
                        iHandler.Opt_Name = Opt_Name;
                        iHandler.Client_FIO = Client_FIO;
                        iHandler.DoctorName = DoctorName;
                        if (DateBegin != null && DateBegin.Trim().Length > 0 && DateTime.TryParse(DateBegin, out iHandler.DateBegin))
                            DateTime.TryParse(DateBegin, out iHandler.DateBegin);
                        if (DateEnd != null && DateEnd.Trim().Length > 0 && DateTime.TryParse(DateEnd, out iHandler.DateEnd))
                            DateTime.TryParse(DateEnd, out iHandler.DateEnd);


                        List<DiagnosticsModel> bb = iHandler.GetItemList();
                        if (bb.Count > 0)
                            op = bb.ToArray();
                        return op;
                    }
                }
                Response.StatusCode = 401;
            }
            return null;
        }

        // GET: api/Diagnostics/5
        [HttpGet("{id}", Name = "GetDiagnostic")]
        public DiagnosticsModel Get(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        DiagnosticsModel op = new DiagnosticsModel();
                        DiagnosticsDBHandler iHandler = new DiagnosticsDBHandler();
                        op = iHandler.GetItem(id.ToString());
                        return op;
                    }
                }
                Response.StatusCode = 401;
            }
            return null;
        }

        // POST: api/Diagnostics
        [HttpPost]
        public string Post([FromBody] DiagnosticsModel value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                        try
                        {
                            DiagnosticsDBHandler iHandler = new DiagnosticsDBHandler();
                            if (iHandler.InsertItem(value))
                                return "ok";
                            else return "";
                        }
                        catch { }
                    return "";
                }
                Response.StatusCode = 401;
            }
            return "";
        }

        // PUT: api/Diagnostics/5
        [HttpPut]
        public string Put([FromBody] DiagnosticsModel value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        try
                        {
                            DiagnosticsDBHandler iHandler = new DiagnosticsDBHandler();
                            if (iHandler.UpdateItem(value))
                                return "ok";
                            else return "";
                        }
                        catch { }
                        return "";
                    }
                }
                Response.StatusCode = 401;
            }
            return "";
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        DiagnosticsDBHandler iHandler = new DiagnosticsDBHandler();
                        if (id != null && iHandler.DeleteItem(id.ToString()))
                            return true;
                        else if (id != null && iHandler.dbc.last_error.Trim().Length > 0)
                        {
                            return false;
                        }
                        else
                            return false;
                    }
                }
                Response.StatusCode = 401;
            }
            return false;
        }
    }
}
