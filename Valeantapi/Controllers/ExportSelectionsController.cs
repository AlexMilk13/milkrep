﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;
using Valeantapi.utils;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExportSelectionsController : ControllerBase
    {
        // GET: api/ExportSelections
        [HttpGet]
        public FileResult Get(DateTime DateBegin, DateTime DateEnd, string Opticname,string Networkname, bool fullReport = false)
        {
            
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    int role = log.GetRole(hh);

                    if (role > 0)
                    {
                        MatchingReportDBHandler iHandler = new MatchingReportDBHandler();
                        int id_user = log.UserInfo(hh).id_User;
                        if (role < 10)
                        {
                            if(fullReport /*|| (Opticname == null && Networkname == null)*/)
                            iHandler.restrict = @" AND [ID_Network] IN (SELECT oo.[ID_Network] FROM [dbo].[optics] oo WHERE oo.[ID_Optic] IN (SELECT [valueint] FROM [dbo].[loginaudit] WHERE [IsActive] = 1 AND [session_hash] = '" + hh + "'))";
                            else
                                iHandler.restrict = @" AND  [ID_Optic] IN (SELECT [valueint] FROM [dbo].[loginaudit] WHERE [IsActive] = 1 AND [session_hash] = '" + hh + "')";
                        }
                        else if (role < 1000)//kam/kas
                                iHandler.restrict = @" AND [ID_KAS] IN (" + id_user.ToString() + ")";
                        else if (role < 10000)
                        {

                            iHandler.restrict = " AND [ID_Optic] IN (SELECT opt.[ID_Optic]  FROM [dbo].[optics] opt  WHERE opt.[ID_User] = " + id_user + ")";
                        }
                        List<MatchingReportModel> bb = new List<MatchingReportModel>();
                        bb = iHandler.GetItemList(DateBegin,DateEnd,Opticname,Networkname);
                        if (bb != null)
                        {
                            excelwork ew = new excelwork();
                            string pp = "";
                            string path = "";
                            if(role < 10)
                                path = ew.ExportSelectionsShort(bb, ref pp);
                            else
                                path = ew.ExportSelections(bb, ref pp);
                            FileStream fs = new FileStream(path, FileMode.Open);
                            string file_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            string file_name = "Отчет_по_подборам_" + DateBegin.ToShortDateString() + "_" + DateEnd.ToShortDateString() + ".xls";
                            return File(fs, file_type, file_name);
                        }
                    }

                }
                Response.StatusCode = 401;
            }
            byte[] respo = new byte[0];
            MemoryStream fs1 = new MemoryStream(respo);
            string file_type1 = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            string file_name1 = "export.xls";
            return File(fs1, file_type1, file_name1);
        }
    }
}