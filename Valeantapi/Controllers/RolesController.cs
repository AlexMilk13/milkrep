﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        // GET: api/Roles
        [HttpGet]
        public IEnumerable<RoleModel> Get()
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {

                        RoleModel[] OptDocs = new RoleModel[0];
                        RoleDBHandler iHandler = new RoleDBHandler();
                        List<RoleModel> bb = new List<RoleModel>();
                        bb = iHandler.GetRoles();
                        if (bb.Count > 0)
                            OptDocs = bb.ToArray();
                        return OptDocs;
                    }
                }
                Response.StatusCode = 401;
            }
            Response.StatusCode = 401;
            return null;
        }

        // GET: api/Roles/5
        [HttpGet("{id}", Name = "GetRoles")]
        public RoleModel Get(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 0)
                    {
                        RoleModel OptDocs = new RoleModel();
                        RoleDBHandler iHandler = new RoleDBHandler();
                        OptDocs = iHandler.GetOneRole(id.ToString());
                        return OptDocs;
                    }
                }
                Response.StatusCode = 401;
            }
            Response.StatusCode = 401;
            return null;
        }



        // DELETE: api/Roles/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) >= 10000)
                    {
                        try
                        {
                            RoleDBHandler iHandler = new RoleDBHandler();
                            return iHandler.DeleteRole(id.ToString());
                        }
                        catch { return false; }
                    }
                }
                Response.StatusCode = 401;
            }
            Response.StatusCode = 401;
            return false;
        }
    }
}
