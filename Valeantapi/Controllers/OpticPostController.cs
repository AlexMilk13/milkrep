﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class OpticPostController : Controller
    {

        // POST api/OpticPost
        [HttpPost]
        public IEnumerable<OpticModel> Post([FromBody] OpticModelPost id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    int role = log.GetRole(hh);
                    if (role > 0)
                    {
                        OpticModel[] op = new OpticModel[0];
                        OpticDBHandler iHandler = new OpticDBHandler();
                        if (role > 0 && role < 10)
                        {
                            UserInfoModel inf = log.UserInfo(hh);
                            iHandler.restrict = " AND ID_Optic IN (" + inf.id_Optic + ")";
                        }
                        if (role > 9 && role < 1000)
                        {
                            UserInfoModel inf = log.UserInfo(hh);
                            iHandler.restrict = " AND ID_Optic IN (SELECT [ID_Optic]  FROM [dbo].[OpticsView] WHERE ID_KAS = " + inf.id_User + ")";
                        }
                        return iHandler.GetItemListForPost(id);
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return null;
        }


    }
}
