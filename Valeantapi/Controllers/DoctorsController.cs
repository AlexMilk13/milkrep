﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorsController : ControllerBase
    {
        // GET: api/Doctors
        [HttpGet]
        public IEnumerable<Doctors> Get()
        {
           
            Doctors[] OptDocs = new Doctors[0];
            OpticDBHandler iHandler = new OpticDBHandler();
            List<Doctors> bb = new List<Doctors>();           
                bb = iHandler.GetAllDoctors();
            if (bb.Count > 0)
                OptDocs = bb.ToArray();
            return OptDocs;
        }


        


    }
}
