﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginCheckController : ControllerBase
    {

        // GET: api/LoginCheck
        [HttpGet]
        public bool Get()
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                log.resp = Response;
                log.Req = Request;
                if (log.CheckLogin(hh))
                {
                    log.RenewCookies();
                    return true;
                }
                else
                {
                    Response.StatusCode = 401;
                }
            }
            
            return false;
        }
    }
}