﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class DoctorsFullPostController : Controller
    {
       

        // POST api/<controller>
        [HttpPost]
        public IEnumerable<OpticDoctorsFull> Post(/*[FromBody]int count = 10, [FromBody]int page = 1, [FromBody]string OpticName = "", [FromBody]string OpticAddress = "", [FromBody]string DocName = "", [FromBody] string DocSurname = "", [FromBody] string DocPatronymic = "",
            [FromBody] string IsActive = "", [FromBody] string sort = "ID_User", [FromBody] string order = "DESC"*/ [FromBody] DoctorsFullPostModel value)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) > 1)
                    {

                        OpticDoctorsFull[] op = new OpticDoctorsFull[0];
                        DoctorsFullDBHandler iHandler = new DoctorsFullDBHandler();
                        iHandler.count = value.count;
                        iHandler.page = value.page;
                        iHandler.OpticAddress = value.OpticAddress;
                        iHandler.OpticName = value.OpticName;
                        iHandler.DocName = value.DocName;
                        iHandler.DocSurname = value.DocSurname;
                        iHandler.DocPatronymic = value.DocPatronymic;
                        iHandler.IsActive = value.IsActive;
                        iHandler.sort = value.sort;
                        iHandler.order = value.order;
                        List<OpticDoctorsFull> bb = iHandler.GetItemList();
                        if (bb.Count > 0)
                            op = bb.ToArray();
                        return op;
                    }
                }
                else
                    Response.StatusCode = 401;
            }
            else
                Response.StatusCode = 401;
            return null;
        }

    }
}
