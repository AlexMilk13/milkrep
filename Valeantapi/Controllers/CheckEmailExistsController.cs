﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valeantapi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Valeantapi.Controllers
{
    [Route("api/[controller]")]
    public class CheckEmailExistsController : Controller
    {
        // GET: api/CheckEmailExists
        [HttpGet]
        public bool Get(string id)
        {
            Response.Cookies.Delete("UhashString");
            LoginDBHandler log = new LoginDBHandler();
            if (Request.Cookies.ContainsKey("UhashString"))
            {
                string hh = Request.Cookies["UhashString"];
                if (log.CheckLogin(hh))
                {
                    log.resp = Response;
                    log.Req = Request;
                    log.RenewCookies();
                    if (log.GetRole(hh) >= 10)
                    {
                        UserDBHandler iHandler = new UserDBHandler();

                        return iHandler.CheckEmail(id);
                    }
                    return false;
                }
                else
                {
                    Response.StatusCode = 401;
                    return false;
                }
            }
            return false;
        }
    }
}
