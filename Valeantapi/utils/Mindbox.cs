﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Data;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Valeantapi.Models;

namespace Valeantapi.utils
{
    public class Mindbox 
    {
        public DbClass dbc = new DbClass();
        public mindboxModel md1;
        public static IConfigurationRoot Configuration { get; set; }
        //private readonly IHttpClientFactory _clientFactory;
        public HttpResponseMessage lastresponse;
        public string lastmessage;
        public string Coder = "";
        public int iduser = 0;
        public customer cu;
        public Mindbox()
        {
            //_clientFactory = clientFactory;
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();            
        }



        public string get_Mindbox_URL()
        {
            string ret = "";
            try
            {
                ret = Configuration.GetValue(typeof(string), "Mindbox URL").ToString();
            }
            catch (Exception exc)
            {
                if (exc.Message.Length > 0)
                    return "";
            }
            return ret;
        }

        public string get_Mindbox_Start_URL()
        {
            string ret = "";
            try
            {
                ret = Configuration.GetValue(typeof(string), "MindboxStart URL").ToString();
            }
            catch (Exception exc)
            {
                if (exc.Message.Length > 0)
                    return "";
            }
            return ret;
        }

        public string get_Mindbox_Email_URL()
        {
            string ret = "";
            try
            {
                ret = Configuration.GetValue(typeof(string), "MindboxMail URL").ToString();
            }
            catch (Exception exc)
            {
                if (exc.Message.Length > 0)
                    return "";
            }
            return ret;
        }

        public string get_Mindbox_Commit_URL()
        {
            string ret = "";
            try
            {
                ret = Configuration.GetValue(typeof(string), "MindboxCommit URL").ToString();
            }
            catch (Exception exc)
            {
                if (exc.Message.Length > 0)
                    return "";
            }
            return ret;
        }

        public string get_Mindbox_secret()
        {
            string ret = "";
            try
            {
                ret = Configuration.GetValue(typeof(string), "MindboxSecret").ToString();
            }
            catch (Exception exc)
            {
                if (exc.Message.Length > 0)
                    return "";
            }
            return ret;
        }

        public async Task<int> ExampleMethodAsync()
        {
            var httpClient = new HttpClient();
            int exampleInt = (await httpClient.GetStringAsync("http://msdn.microsoft.com")).Length;
            //string hhh = "Preparing to finish ExampleMethodAsync.\n";
            // After the following return statement, any method that's awaiting  
            // ExampleMethodAsync (in this case, StartButton_Click) can get the   
            // integer result.  
            return exampleInt;
        }


        public customer DeserializeResponse()
        {
            string hhh = "";
            cu = new customer();
            try
            {

                
                cu = JsonConvert.DeserializeObject<customer>(lastmessage);
                JsonDocument document = JsonDocument.Parse(lastmessage);
                JsonElement root = document.RootElement;
                JsonElement ElementsAll = root.GetProperty("customer");
                foreach (JsonProperty student in ElementsAll.EnumerateObject())
                {
                    if (student.Name == "ids")
                    {
                        cu.ids = new customer.id();
                        JsonDocument documentids = JsonDocument.Parse(student.Value.ToString());
                        long tmp = documentids.RootElement.GetProperty("mindboxId").GetInt64();
                        cu.ids.mindboxId = tmp;
                       // hhh += student.Value;
                    }
                    else if (student.Name == "firstName")
                    {
                        cu.firstName = student.Value.ToString();
                    }
                    else if (student.Name == "lastName")
                    {
                        cu.lastName = student.Value.ToString();
                    }
                    else if (student.Name == "email")
                    {
                        cu.email = student.Value.ToString();
                    }
                    else if (student.Name == "isEmailInvalid")
                    {
                        cu.isEmailInvalid = student.Value.ToString();
                    }
                    else if (student.Name == "mobilePhone")
                    {
                        cu.mobilePhone = student.Value.ToString();
                    }
                    else if (student.Name == "isMobilePhoneInvalid")
                    {
                        cu.isMobilePhoneInvalid = student.Value.ToString();
                    }
                    else if (student.Name == "sex")
                    {
                        cu.sex = student.Value.ToString();
                    }
                    else if (student.Name == "changeDateTimeUtc")
                    {
                        cu.changeDateTimeUtc = student.Value.ToString();
                    }
                    else if (student.Name == "ianaTimeZone")
                    {
                        cu.ianaTimeZone = student.Value.ToString();
                    }
                    else if (student.Name == "timeZoneSource")
                    {
                        cu.timeZoneSource = student.Value.ToString();
                    }
                    else if (student.Name == "sessionKey")
                    {
                        cu.sessionKey = student.Value.ToString();
                    }
                    else
                        hhh += student.Value;

                }

                ElementsAll = root.GetProperty("order");//.couponsInfo.coupon.availableTillDateTimeUtc");
                ElementsAll = ElementsAll.GetProperty("couponsInfo");
                
                foreach (JsonElement student in ElementsAll.EnumerateArray())
                {
                    ElementsAll = student.GetProperty("coupon");
                    break;
                }
                    ElementsAll = ElementsAll.GetProperty("availableTillDateTimeUtc");
                cu.ValidTo = ElementsAll.GetDateTime();
                CertificateDBHandler cdb = new CertificateDBHandler();
                cdb.InsertRequest(lastmessage, 0, Coder);

            }
            catch(Exception exc) 
            {
                
            }
            return cu;
        }

        public bool ISSMS()
        {        
            try
            {


                JsonDocument document = JsonDocument.Parse(lastmessage);
                JsonElement root = document.RootElement;
                JsonElement ElementsAll = root.GetProperty("customer");
                foreach (JsonProperty student in ElementsAll.EnumerateObject())
                {
                  

                }

                ElementsAll = root.GetProperty("order");//.couponsInfo.coupon.availableTillDateTimeUtc");
                ElementsAll = ElementsAll.GetProperty("couponsInfo");

                foreach (JsonElement student in ElementsAll.EnumerateArray())
                {
                    ElementsAll = student.GetProperty("coupon");
                    break;
                }
                ElementsAll = ElementsAll.GetProperty("availableTillDateTimeUtc");
                cu.ValidTo = ElementsAll.GetDateTime();
                CertificateDBHandler cdb = new CertificateDBHandler();
                cdb.InsertRequest(lastmessage, 0, Coder);

            }
            catch (Exception exc)
            {

            }
            return false;
        }

        public async Task<mindboxModel> OnPostCertificate(string code)
        {
            lastmessage = null;
            var request = new HttpRequestMessage(HttpMethod.Post, get_Mindbox_URL());
            try
            {
               
              //  request.Headers.Add("Authorization", get_Mindbox_secret());
                request.Content = new StringContent(
                    @"{
  ""order"": { 
    ""deliveryCost"": ""0"",
    ""coupons"": [
      {
                ""ids"": {
                    ""code"": """ + code + @"""
                }
            }
    ], 
    ""lines"": [
      {
        ""basePricePerItem"": ""1"",
        ""quantity"": ""1"",
        ""product"": {
          ""ids"": {
            ""products"": ""11""
          }
},
        ""status"": {
          ""ids"": {
            ""externalId"": ""create""
          }
        }
      }
    ]
  }
}",
                                    Encoding.UTF8,
                                    "application/json");

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders
      .Accept
      .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                client.DefaultRequestHeaders.Authorization =
    new AuthenticationHeaderValue("Authorization", get_Mindbox_secret());
                request.Headers.Authorization = new AuthenticationHeaderValue("Mindbox", get_Mindbox_secret());
                
                    var response = await client.SendAsync(request);
                

                if (response.IsSuccessStatusCode)
            {
                    mindboxModel mbm = new mindboxModel();
                lastresponse = response;
                    string phonestring = "";
                    string firstName = "";
                    string lastName = "";
                    string email = "";
                    string status = "";
                    DateTime availableTillDateTimeUtc = new DateTime();
                    lastmessage = await response.Content.ReadAsStringAsync();
                    string[] stringrows = new string[0];
                    using (JsonDocument document = JsonDocument.Parse(lastmessage))
                    {
                        JsonElement root = document.RootElement;
                        JsonElement customerElement = root.GetProperty("customer");
                        //customerElement.GetProperty("processingStatus");
                        try
                        {
                            phonestring = customerElement.GetProperty("mobilePhone").GetUInt64().ToString();
                        }
                        catch { phonestring = ""; }
                        try
                        {
                            firstName = customerElement.GetProperty("firstName").GetString();
                        }
                        catch { firstName = ""; }
                        try
                            {
                                lastName = customerElement.GetProperty("lastName").GetString();
                        }
                        catch { lastName = ""; }
                        try
                                {
                                    email = customerElement.GetProperty("email").GetString();
                        }
                        catch { email = ""; }
                        status = customerElement.GetProperty("processingStatus").GetString();
                        JsonElement tmpj = root.GetProperty("order");
                        tmpj = tmpj.GetProperty("couponsInfo");
                        JsonElement lookforSMSinfo; 
                        foreach (JsonElement st in tmpj.EnumerateArray())
                        {
                            JsonElement tmpj1;
                            if(st.TryGetProperty("coupon", out tmpj1))
                            {
                                status = tmpj1.GetProperty("status").GetString();
                                if (tmpj1.TryGetProperty("pool", out lookforSMSinfo) && lookforSMSinfo.TryGetProperty("ids", out lookforSMSinfo))
                                {
                                    string SMSres = lookforSMSinfo.GetProperty("externalId").GetString();
                                        //
                                    if (SMSres == "sms-pool")
                                        mbm.sms = SMSres;
                                    else mbm.sms = "";
                                }
                                if (status == "Used")
                                    tmpj1 = tmpj1.GetProperty("usedDateTimeUtc");
                                else
                                    tmpj1 = tmpj1.GetProperty("availableTillDateTimeUtc");
                                availableTillDateTimeUtc = tmpj1.GetDateTime();
                            }
                        }
                        mbm.cust = new customer();
                        mbm.cust.email = email;
                        mbm.cust.firstName = firstName;
                        mbm.cust.lastName = lastName;
                        mbm.cust.mobilePhone = phonestring;
                        mbm.status = status;
                        mbm.cust.ValidTo = availableTillDateTimeUtc;
                        mbm.Code = code;
                        md1 = new mindboxModel();//save it
                        md1 = mbm;
                        CertificateDBHandler cdbh = new CertificateDBHandler();
                        cdbh.InsertRequest(lastmessage, iduser, code);
                    }
                return mbm;
            }
            else
            {
                lastresponse = null;
                return null;
            }
            }
            catch(Exception exc)
            {
                return null;
            }
        }



        public async Task<int> OnPostCertificateStart(mindboxModel mod)
        {
            mod.ID = 0;
            int id = InsertItem(mod);
            if (id <= 0)
                return 0;
            mod.ID = id;
            lastmessage = null;
            var request = new HttpRequestMessage(HttpMethod.Post, get_Mindbox_Start_URL());
            try
            {

                //  request.Headers.Add("Authorization", get_Mindbox_secret());
                request.Content = new StringContent(
                    @"{
  ""order"": {
    ""ids"": {
      ""externalSystemwebsite"": """ + id.ToString() + @"""
    },
    ""deliveryCost"": ""0"",
    ""totalPrice"": ""0"",
    ""coupons"": [
      {
        ""ids"": {
          ""code"": """ + mod.Code + @"""
        }
}
    ], 
    ""transaction"": {
      ""ids"": {
        ""externalId"": """ + id.ToString() + "-1" + @"""
      }
    },
    ""lines"": [
      {
        ""basePricePerItem"": ""1"",
        ""quantity"": ""1"",
        ""lineNumber"": ""1"",
        ""product"": {
          ""ids"": {
            ""products"": ""11""
          }
        },
        ""status"": {
          ""ids"": {
            ""externalId"": ""create""
          }
        }
      }
    ]
  }
}",
                                    Encoding.UTF8,
                                    "application/json");

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders
      .Accept
      .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                client.DefaultRequestHeaders.Authorization =
    new AuthenticationHeaderValue("Authorization", get_Mindbox_secret());
                request.Headers.Authorization = new AuthenticationHeaderValue("Mindbox", get_Mindbox_secret());

                var response = await client.SendAsync(request);


                if (response.IsSuccessStatusCode)
                {
                    lastresponse = response;

                    lastmessage = await response.Content.ReadAsStringAsync();

                    return await OnPostCertificateFinish(mod);
                }
                else
                {
                    lastresponse = null;
                    return 0;
                }
            }
            catch (Exception exc)
            {
                return 0;
            }
        }












        public async Task<int> OnPostCertificateFinish(mindboxModel mod)
        {


            lastmessage = null;
            var request = new HttpRequestMessage(HttpMethod.Post, get_Mindbox_Commit_URL());
            try
            {

                //  request.Headers.Add("Authorization", get_Mindbox_secret());
                request.Content = new StringContent(
                    @"{
  ""order"": {
    ""ids"": {
      ""externalSystemwebsite"": """ + mod.ID.ToString() + @"""
    },
    ""transaction"": {
                    ""ids"": {
                        ""externalId"": """ + mod.ID.ToString() + "-1" + @"""
                    }
                }
            }
}",
                                    Encoding.UTF8,
                                    "application/json");

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders
      .Accept
      .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                client.DefaultRequestHeaders.Authorization =
    new AuthenticationHeaderValue("Authorization", get_Mindbox_secret());
                request.Headers.Authorization = new AuthenticationHeaderValue("Mindbox", get_Mindbox_secret());

                var response = await client.SendAsync(request);


                if (response.IsSuccessStatusCode)
                {
                    lastresponse = response;

                    lastmessage = await response.Content.ReadAsStringAsync();
                    md1 = mod;
                    return (int)mod.ID;
                }
                else
                {
                    lastresponse = null;
                    return 0;
                }
            }
            catch (Exception exc)
            {
                return 0;
            }
        }






        public async Task<int> OnSendingEmail(InvitationModel mod)
        {

            lastmessage = null;
            var request = new HttpRequestMessage(HttpMethod.Post, get_Mindbox_Email_URL());
            try
            {

                //  request.Headers.Add("Authorization", get_Mindbox_secret());
                request.Content = new StringContent(
                    @"{
  ""customer"": {
    ""email"": """ + mod.Email + @"""
  },
  ""emailMailing"": {
                ""customParameters"": {
                    ""LOGIN"": """ + mod.LOGIN + @""",
      ""LINK"": """ + mod.LINK + @"""
                }
            }
        }",
                                    Encoding.UTF8,
                                    "application/json");

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders
      .Accept
      .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                client.DefaultRequestHeaders.Authorization =
    new AuthenticationHeaderValue("Authorization", get_Mindbox_secret());
                request.Headers.Authorization = new AuthenticationHeaderValue("Mindbox", get_Mindbox_secret());

                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    lastresponse = response;

                    lastmessage = await response.Content.ReadAsStringAsync();

                    return await OnPostCertificateFinish(new mindboxModel());
                }
                else
                {
                    lastresponse = null;
                    return 0;
                }
            }
            catch (Exception exc)
            {
                return 0;
            }
        }















        public int InsertItem(mindboxModel mod)
        {
            try
            {
               

                string query = @"INSERT INTO [dbo].[MindBox_Certificates]
           ([CodeInt]           ,[CodeMindBox]           ,[Status]           ,[DateCreate]
           ,[DateActivate]           ,[DateExpired]          ,[firstName]           ,[lastName]           ,[email]           ,[mobilePhone])
OUTPUT Inserted.ID_Sertificat
     VALUES
           ('" + mod.Code + "', '" + mod.Code + @"', '" + mod.status + @"', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP       ,'" + mod.cust.ValidTo.ToString("yyyy-MM-dd HH:mm:ss") + @"' ,'" + mod.cust.firstName + @"'
           , '" + mod.cust.lastName + @"', '" + mod.cust.email + @"','" + mod.cust.mobilePhone + @"')";
                DataTable dt = dbc.Make_Query(query);
                if (dbc.last_error.Length == 0 && dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr[0].ToString().Trim().Length == 0)
                            return -1;
                        return (int)dr[0];
                    }
                }
            }
            catch
            {
                return -1;
            }
            return -1;
        }



    }
}
