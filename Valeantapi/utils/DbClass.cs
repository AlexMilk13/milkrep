﻿using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.IO;

namespace Valeantapi.utils
{
    public class DbClass
    {
        public static IConfigurationRoot Configuration { get; set; }
        public string last_error = "";
        public SqlConnection connection = new SqlConnection();
        public DbClass()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();

            
            connection = new SqlConnection(get_conn_string());
        }


        public string get_conn_string()
        {
            string ret = "";
            try
            {
               ret = Configuration.GetValue (typeof(string), "Valeant_conn").ToString();
               
            }
            catch(Exception exc) 
            {
                if (exc.Message.Length > 0)
                    return "";
            }
            return ret;
        }

        public string get_conf_string(string conf)
        {
            string ret = "";
            try
            {
                ret = Configuration.GetValue(typeof(string), conf).ToString();

            }
            catch (Exception exc)
            {
                if (exc.Message.Length > 0)
                    return "";
            }
            return ret;
        }


        /*     public bool login_Log(HttpRequestBase baseinfo, string resp, string user)
             {
                 string query = @"INSERT INTO [dbo].[users_Loginlog]
                ([user]
                ,[lastdate]
                ,[browser]
     ,mobile
                ,[respIP]
     ,userhostname
     ,userLang
     ,userFullInfo
                ,[logrecord])
          VALUES
                ('~" + user + @"~'
                ,CURRENT_TIMESTAMP
                ,'~" + baseinfo.Browser.Browser + @"~'
     ,'~" + baseinfo.Browser.IsMobileDevice.ToString() + @"~'
     ,'~" + baseinfo.UserHostAddress + @"~'
     ,'~" + baseinfo.UserHostName + @"~'
     ,'~";
                 if (baseinfo.UserLanguages.Length > 0)
                     query += baseinfo.UserLanguages[0] + @"~'";
                 else
                     query += @" ~'";

                 query += @",'~" + baseinfo.UserAgent + @"~'
                ,'" + resp + "')";
                 Make_Query(query);
                 return true;
             }*/



        public DataTable Make_Query(string query)
        {
            DataTable dt = new DataTable();
            last_error = "";
            try
            {
                using (SqlConnection connection1 = new SqlConnection(get_conn_string()))
                {
                    SqlCommand cmd = new SqlCommand(query, connection);
                    using SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    if (connection.State != ConnectionState.Open)
                        connection.Open();
                    adapter.Fill(dt);
                    connection.Close();
                }
            }
            catch (Exception exc) { last_error = exc.Message; }
            return dt;
        }



        public DataTable insert_file(string filepath, string code)
        {
            DataTable dt = new DataTable();
            last_error = "";
            try
            {
                byte[] buffer = File.ReadAllBytes(filepath);
                //SqlCommand command = new SqlCommand();

                using (SqlConnection connection1 = new SqlConnection(get_conn_string()))
                {
                    SqlCommand cmd = new SqlCommand("", connection);
                    cmd.CommandText = @"UPDATE [dbo].[sku]
   SET[Photo] = @rrr
 WHERE Code = '" + code + "'";
                    cmd.Parameters.AddWithValue("@rrr", buffer);
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    {
                        connection.Open();
                        adapter.Fill(dt);
                        connection.Close();
                    }
                }
                File.Delete(filepath);
            }
            catch (Exception exc) { last_error = exc.Message; }
            return dt;
        }

        public DataTable update_file(byte[] buffer, string code)
        {
            DataTable dt = new DataTable();
            last_error = "";
            try
            {
                //SqlCommand command = new SqlCommand();
                using (SqlConnection connection1 = new SqlConnection(get_conn_string()))
                {
                    SqlCommand cmd = new SqlCommand("", connection);
                    cmd.CommandText = @"UPDATE [dbo].[sku]
   SET[Photo] = @rrr
 WHERE Code = '" + code + "'";
                    cmd.Parameters.AddWithValue("@rrr", buffer);
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    {
                        connection.Open();
                        adapter.Fill(dt);
                        connection.Close();
                    }
                }
            }
            catch (Exception exc) { last_error = exc.Message; }
            return dt;
        }


    }
}
