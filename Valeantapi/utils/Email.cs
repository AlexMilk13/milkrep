﻿using Microsoft.Extensions.Configuration;
using MimeKit;
using System;
using System.Data;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using System.IO;
using Valeantapi.Models;

namespace Valeantapi.utils
{
    public class Email
    {
        public DbClass dbc = new DbClass();
        public string par_smtp_server;
        public int par_smtp_port;
        public string par_smtp_user;
        public string par_smtp_pass;
        public string par_smtp_from;
        public string par_http_root;
        public string par_http_name;



        public static IConfigurationRoot Configuration { get; set; }
        public string last_error = "";
        public Email()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            par_smtp_server = smtp_server();
            par_smtp_port = smtp_port();
            par_smtp_user = smtp_user();
            par_smtp_pass = smtp_pass();
            par_smtp_from = smtp_from();
            par_http_root = http_root();
        par_http_name = http_sitename();
    }



        public static string Get_Conf_String(string conf)
        {
            string ret = "";

            try
            {
                ret = Configuration.GetValue(typeof(string), conf).ToString();

            }
            catch (Exception exc)
            {
                if (exc.Message.Length > 0)
                    return "";
            }
            return ret;
        }

        public static string get_conn_string()
        {
            string ret = "";
            try
            {
                ret = Configuration.GetValue(typeof(string), "Valeant_conn").ToString();

            }
            catch (Exception exc)
            {
                if (exc.Message.Length > 0)
                    return "";
            }
            return ret;
        }

        public static string smtp_server()
        {
            string ret = "";
            try
            {
                ret = Get_Conf_String("smtpServer");
            }
            catch { }
            return ret;
        }

        public static int smtp_port()
        {
            int ret = 25;
            try
            {
                ret = int.Parse(Get_Conf_String("smtpPort"));
            }
            catch { }
            return ret;
        }
        public static string smtp_user()
        {
            string ret = "";
            try
            {
                ret = Get_Conf_String("smtpUser");
            }
            catch { }
            return ret;
        }
        public static string smtp_pass()
        {
            string ret = "";
            try
            {
                ret = Get_Conf_String("smtpPass");
            }
            catch { }
            return ret;
        }
        public static string smtp_from()
        {
            string ret = "";
            try
            {
                ret = Get_Conf_String("smtpFrom");
            }
            catch { }
            return ret;
        }

        public static string http_root()
        {
            string ret = "";
            try
            {
                ret = Get_Conf_String("httpRoot");
            }
            catch { }
            return ret;
        }

        public static string http_sitename()
        {
            string ret = "";
            try
            {
                ret = Get_Conf_String("httpSiteName");
            }
            catch { }
            return ret;
        }



        public string Send_invitation_new(string IDUser)
        {
            string message_body, message_header, hash;
           
            string query = @"SELECT [email_header]      ,[email_body], (SELECT [Login] FROM [dbo].[users] WHERE [ID_User] = " + IDUser + ")  AS login      FROM [dbo].[TextTemplates]  WHERE [TemplateName] = 'doctor invitation';";
            DataTable dt = dbc.Make_Query(query);
           
            if (dbc.last_error.Length == 0 && dt.Rows.Count > 0)
            {

                message_header = dt.Rows[0][0].ToString();
                message_body = dt.Rows[0][1].ToString();
                string login = dt.Rows[0][2].ToString();
                query = @"EXEC	[dbo].[users_create_Invitation]		@IDUser = " + IDUser;

                dt = dbc.Make_Query(query);

                if (dbc.last_error.Length == 0 && dt.Rows.Count > 0)
                {
                    hash = dt.Rows[0][0].ToString();
                    string eaddr = dt.Rows[0][1].ToString();
                    message_body = message_body.Replace("$sitename$", par_http_name);
                    message_body = message_body.Replace("$login$", login);
                    message_body = message_body.Replace("$passrecover$", par_http_root + @"/#/auth/new-password?code=" + hash);
                    //send e-mail here
                    SendEmailAsync(eaddr, message_header, message_body);
                    return "";
                }


            }

            return dbc.last_error;

        }


        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Администрация сайта", par_smtp_from));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using var client = new SmtpClient();
            await client.ConnectAsync(par_smtp_server, par_smtp_port, false);
            await client.AuthenticateAsync(par_smtp_user, par_smtp_pass);
            await client.SendAsync(emailMessage);

            await client.DisconnectAsync(true);
        }

        public string Send_invitation_mindbox(string IDUser)
        {
            string message_body, message_header, hash;

            string query = @"SELECT [email_header]      ,[email_body], (SELECT [Login] FROM [dbo].[users] WHERE [ID_User] = " + IDUser + ")  AS login      FROM [dbo].[TextTemplates]  WHERE [TemplateName] = 'doctor invitation';";
            DataTable dt = dbc.Make_Query(query);

            if (dbc.last_error.Length == 0 && dt.Rows.Count > 0)
            {

                message_header = dt.Rows[0][0].ToString();
                message_body = dt.Rows[0][1].ToString();
                string login = dt.Rows[0][2].ToString();
                query = @"EXEC	[dbo].[users_create_Invitation]		@IDUser = " + IDUser;

                dt = dbc.Make_Query(query);

                if (dbc.last_error.Length == 0 && dt.Rows.Count > 0)
                {
                    hash = dt.Rows[0][0].ToString();
                    string eaddr = dt.Rows[0][1].ToString();
                    //message_body = message_body.Replace("$sitename$", par_http_name);
                    message_body = message_body.Replace("$login$", login);
                    message_body = message_body.Replace("$passrecover$", par_http_root + @"/#/auth/new-password?code=" + hash);
                    //send e-mail here
                    Mindbox mb = new Mindbox();
                    InvitationModel inv = new InvitationModel();
                    inv.Email = eaddr;
                    inv.LINK = par_http_root + @"/#/auth/new-password?code=" + hash;
                    inv.LOGIN = login;
                    mb.OnSendingEmail(inv);
                   // SendEmailAsync(eaddr, message_header, message_body);
                    return "";
                }


            }

            return dbc.last_error;

        }


    }
}
