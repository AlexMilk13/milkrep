﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF;

using System;
using System.Collections.Generic;
using System.IO;
using Valeantapi.Models;

namespace Valeantapi.utils
{
    public class excelwork
    {
        


        public string ExportNetworks(List<NetworksModel> UsersList, ref string OutFile)
        {
            XSSFWorkbook xssfwb;
            string result = Path.GetTempPath();
            string filename = "ExportVal" + DateTime.Now.Ticks.ToString() + ".xlsx";
            result += filename;
            OutFile = filename;
            int rownumb = 0;
            //Открываем файл
            using (FileStream file = new FileStream(result, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                xssfwb = new XSSFWorkbook();
                if (xssfwb.NumberOfSheets == 0)
                {
                    xssfwb.CreateSheet();
                }
                ISheet sheet = xssfwb.GetSheetAt(0);
                while (sheet.LastRowNum > 0)
                {
                    sheet.RemoveRow(sheet.GetRow(sheet.LastRowNum));
                }
                IRow row = sheet.CreateRow(0);
                for (int column = 0; column < 2; column++)
                {
                    

                    row.CreateCell(column);
                    switch (column)
                    {
                        
                        case 0:
                            try
                            {
                                row.Cells[column].SetCellValue("Сеть Оптик (Юридическое торговое название)");
                            }
                            catch
                            {
                            }
                            break;
                        case 1:
                            try
                            {
                                row.Cells[column].SetCellValue("Активность");
                            }
                            catch
                            {
                            }
                            break;
                        
                    }

                }
                foreach (NetworksModel um in UsersList)
                {
                    rownumb++;
                    IRow rowtmp = sheet.CreateRow(rownumb);
                    for (int column = 0; column < 2; column++)
                    {
                        rowtmp.CreateCell(column);
                        switch (column)
                        {
                            
                            case 0:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.NetworkName);
                                }
                                catch
                                {
                                }
                                break;
                            case 1:
                                try
                                {
                                    if (um.IsActive)
                                    { rowtmp.Cells[column].SetCellValue("True"); }
                                    else
                                        rowtmp.Cells[column].SetCellValue("False");
                                  //  rowtmp.Cells[column].SetCellValue(um.IsActive);
                                }
                                catch
                                {
                                }
                                break;
                           
                        }
                    }
                }
                sheet.AutoSizeColumn(0, true);
                sheet.AutoSizeColumn(1, true);
                // sheet.SetColumnWidth()
                try
                {
                    xssfwb.Write(file);
                    if (xssfwb != null)
                        xssfwb.Close();
                }
                catch { }
            }
            return result;
        }





        public string ExportDiagnosticsOrder(List<DiagnosticsOrder> DiagnosticsOrderList, ref string OutFile)
        {
            XSSFWorkbook xssfwb;
            string result = Path.GetTempPath();
            string filename = "ExportVal" + DateTime.Now.Ticks.ToString() + ".xls";
            result += filename;
            OutFile = filename;
            int rownumb = 0;
            //Открываем файл
            using (FileStream file = new FileStream(result, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                xssfwb = new XSSFWorkbook();
                if (xssfwb.NumberOfSheets == 0)
                {
                    xssfwb.CreateSheet();
                }
                ISheet sheet = xssfwb.GetSheetAt(0);
                while (sheet.LastRowNum > 0)
                {
                    sheet.RemoveRow(sheet.GetRow(sheet.LastRowNum));
                }
                IRow row = sheet.CreateRow(0);
                for (int column = 0; column < 22; column++)
                {


                    row.CreateCell(column);
                    switch (column)
                    {

                        case 0:
                            try
                            {
                                row.Cells[column].SetCellValue("Субъект РФ");
                            }
                            catch
                            {
                            }
                            break;
                        case 1:
                            try
                            {
                                row.Cells[column].SetCellValue("ТМ");
                            }
                            catch
                            {
                            }
                            break;
                        case 2:
                            try
                            {
                                row.Cells[column].SetCellValue("KAS/KAM");
                            }
                            catch
                            {
                            }
                            break;
                        case 3:
                            try
                            {
                                row.Cells[column].SetCellValue("ИНН Оптики");
                            }
                            catch
                            {
                            }
                            break;
                        case 4:
                            try
                            {
                                row.Cells[column].SetCellValue("Юридическое название Оптики");
                            }
                            catch
                            {
                            }
                            break;
                        case 5:
                            try
                            {
                                row.Cells[column].SetCellValue("Название оптики");
                            }
                            catch
                            {
                            }
                            break;
                        case 6:
                            try
                            {
                                row.Cells[column].SetCellValue("Город (Оптика)");
                            }
                            catch
                            {
                            }
                            break;
                        case 7:
                            try
                            {
                                row.Cells[column].SetCellValue("Адрес Оптики");
                            }
                            catch
                            {
                            }
                            break;
                        case 8:
                            try
                            {
                                row.Cells[column].SetCellValue("Адрес доставки");
                            }
                            catch
                            {
                            }
                            break;
                        case 9:
                            try
                            {
                                row.Cells[column].SetCellValue("Код (Номер сертификата или SMS)");
                            }
                            catch
                            {
                            }
                            break;
                        case 10:
                            try
                            {
                                row.Cells[column].SetCellValue("Сертификат или SMS (перечислимый тип)");
                            }
                            catch
                            {
                            }
                            break;
                        case 11:
                            try
                            {
                                row.Cells[column].SetCellValue("SKU");
                            }
                            catch
                            {
                            }
                            break;
                        case 12:
                            try
                            {
                                row.Cells[column].SetCellValue("SKUID");
                            }
                            catch
                            {
                            }
                            break;
                        case 13:
                            try
                            {
                                row.Cells[column].SetCellValue("Номенклатура");
                            }
                            catch
                            {
                            }
                            break;
                        case 14:
                            try
                            {
                                row.Cells[column].SetCellValue("Расход, pk");
                            }
                            catch
                            {
                            }
                            break;
                       /* case 15:
                            try
                            {
                                row.Cells[column].SetCellValue("Подбор к оплате");
                            }
                            catch
                            {
                            }
                            break;*/
                        case 15:
                            try
                            {
                                row.Cells[column].SetCellValue("Причина расхода");
                            }
                            catch
                            {
                            }
                            break;
                        case 16:
                            try
                            {
                                row.Cells[column].SetCellValue("Дата окончания подбора");
                            }
                            catch
                            {
                            }
                            break;
                        case 17:
                            try
                            {
                                row.Cells[column].SetCellValue("Месяц окончания подбора");
                            }
                            catch
                            {
                            }
                            break;
                        case 18:
                            try
                            {
                                row.Cells[column].SetCellValue("Квартал окончания подбора");
                            }
                            catch
                            {
                            }
                            break;
                        case 19:
                            try
                            {
                                row.Cells[column].SetCellValue("Год окончания подбора");
                            }
                            catch
                            {
                            }
                            break;
                        case 20:
                            try
                            {
                                row.Cells[column].SetCellValue("Номер телефона клиента (пациента)");
                            }
                            catch
                            {
                            }
                            break;
                       /* case 22:
                            try
                            {
                                row.Cells[column].SetCellValue("Код доступа");
                            }
                            catch
                            {
                            }
                            break;*/
                    }
                    if (column != 7 && column != 8)
                        sheet.AutoSizeColumn(column, true);
                    else
                    {
                        sheet.SetColumnWidth(column, 6820);
                      // sheet.SetColumnWidth
                    }
                }
                ICellStyle _doubleCellStyle = null;
                _doubleCellStyle = xssfwb.CreateCellStyle();
                _doubleCellStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("0.00");
                ICellStyle _dateCellStyle = null;
                _dateCellStyle = xssfwb.CreateCellStyle();
                _dateCellStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("yyyy.mm.dd");
                foreach (DiagnosticsOrder um in DiagnosticsOrderList)
                {
                    rownumb++;
                    IRow rowtmp = sheet.CreateRow(rownumb);
                    for (int column = 0; column < 22; column++)
                    {
                        rowtmp.CreateCell(column);
                        switch (column)
                        {

                            case 0:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.RegionName);
                                }
                                catch
                                {
                                }
                                break;
                            case 1:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.TMNAME);
                                }
                                catch
                                {
                                }
                                break;
                            case 2:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.KASName);
                                }
                                catch
                                {
                                }
                                break;
                            case 3:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Client_INN);
                                }
                                catch
                                {
                                }
                                break;
                            case 4:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Client_FIO);
                                }
                                catch
                                {
                                }
                                break;
                            case 5:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.OpticName);
                                }
                                catch
                                {
                                }
                                break;
                            case 6:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.TownName);
                                }
                                catch
                                {
                                }
                                break;
                            case 7:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.FiasAddress);
                                }
                                catch
                                {
                                }
                                break;
                            case 8:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.DeliverAddress);
                                }
                                catch
                                {
                                }
                                break;
                            case 9:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.CodeMindBox);
                                }
                                catch
                                {
                                }
                                break;
                            case 10:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Certif);
                                }
                                catch
                                {
                                }
                                break;
                            case 11:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.SKUName);
                                }
                                catch
                                {
                                }
                                break;
                            case 12:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.SKU_ID);
                                }
                                catch
                                {
                                }
                                break;
                            case 13:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Nomenkl);
                                }
                                catch
                                {
                                }
                                break;
                            case 14:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.SKUAmount);
                                    rowtmp.Cells[column].SetCellType(CellType.Numeric);
                                    ICellStyle _doubleCellStyle1 = null;
                                    _doubleCellStyle1 = rowtmp.Cells[column].CellStyle;
                                    rowtmp.Cells[column].CellStyle = _doubleCellStyle;                                
                                }
                                catch
                                {
                                }
                                break;
                            /*case 15:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.ResultName);
                                }
                                catch
                                {
                                }
                                break;*/
                            case 15:
                                try
                                {
                                    if (um.Reason == null || um.Reason.Length == 0)
                                        rowtmp.Cells[column].SetCellValue("Подбор");
                                    else
                                        rowtmp.Cells[column].SetCellValue(um.Reason);
                                }
                                catch
                                {
                                }
                                break;
                            case 16:
                                try
                                {
                                    if(um.DateEnd.Year!= 1)
                                    rowtmp.Cells[column].SetCellValue(um.DateEnd.Day.ToString());
                                }
                                catch
                                {
                                }
                                break;
                            case 17:
                                try
                                {
                                    if(um.monthpart == 1)
                                    rowtmp.Cells[column].SetCellValue("январь");
                                    else if (um.monthpart == 2)
                                        rowtmp.Cells[column].SetCellValue("февраль");
                                    else if (um.monthpart == 3)
                                        rowtmp.Cells[column].SetCellValue("март");
                                    else if (um.monthpart == 4)
                                        rowtmp.Cells[column].SetCellValue("апрель");
                                    else if (um.monthpart == 5)
                                        rowtmp.Cells[column].SetCellValue("май");
                                    else if (um.monthpart == 6)
                                        rowtmp.Cells[column].SetCellValue("июнь");
                                    else if (um.monthpart == 7)
                                        rowtmp.Cells[column].SetCellValue("июль");
                                    else if (um.monthpart == 8)
                                        rowtmp.Cells[column].SetCellValue("август");
                                    else if (um.monthpart == 9)
                                        rowtmp.Cells[column].SetCellValue("сентябрь");
                                    else if (um.monthpart == 10)
                                        rowtmp.Cells[column].SetCellValue("октябрь");
                                    else if (um.monthpart == 11)
                                        rowtmp.Cells[column].SetCellValue("ноябрь");
                                    else if (um.monthpart == 12)
                                        rowtmp.Cells[column].SetCellValue("декабрь");
                                    else 
                                        rowtmp.Cells[column].SetCellValue("");
                                }
                                catch
                                {
                                }
                                break;
                            case 18:
                                try
                                {
                                    if (um.quoterpart > 0)
                                        rowtmp.Cells[column].SetCellValue(um.quoterpart);
                                    else
                                        rowtmp.Cells[column].SetCellValue("");
                                }
                                catch
                                {
                                }
                                break;
                            case 19:
                                try
                                {
                                    if (um.yearpart > 0)
                                        rowtmp.Cells[column].SetCellValue(um.yearpart);
                                    else
                                        rowtmp.Cells[column].SetCellValue("");
                                }
                                catch
                                {
                                }
                                break;
                            case 20:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.mobilePhone);

                                }
                                catch
                                {
                                }
                                break;
                            /*case 22:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.AccessCode);

                                }
                                catch
                                {
                                }
                                break;*/
                        }
                    }
                }
                sheet.AutoSizeColumn(0, true);
                sheet.AutoSizeColumn(1, true);
                // sheet.SetColumnWidth()
                try
                {
                    xssfwb.Write(file);
                    if (xssfwb != null)
                        xssfwb.Close();
                }
                catch { }
            }
            return result;
        }




        public string ExportSKUs(List<SKUModel> SKUList, ref string OutFile)
        {
            XSSFWorkbook xssfwb;
            string result = Path.GetTempPath();
            string filename = "ExportVal" + DateTime.Now.Ticks.ToString() + ".xlsx";
            result += filename;
            OutFile = filename;
            int rownumb = 0;
            //Открываем файл
            using (FileStream file = new FileStream(result, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                xssfwb = new XSSFWorkbook();
                if (xssfwb.NumberOfSheets == 0)
                {
                    xssfwb.CreateSheet();
                }
                ISheet sheet = xssfwb.GetSheetAt(0);
                while (sheet.LastRowNum > 0)
                {
                    sheet.RemoveRow(sheet.GetRow(sheet.LastRowNum));
                }
                IRow row = sheet.CreateRow(0);
                for (int column = 0; column < 9; column++)
                {
                    row.CreateCell(column);
                    switch (column)
                    {
                        case 0:
                            try
                            {
                                row.Cells[column].SetCellValue("ID");
                            }
                            catch
                            {
                            }
                            break;
                        case 1:
                            try
                            {
                                row.Cells[column].SetCellValue("Наименование");
                            }
                            catch
                            {
                            }
                            break;
                        case 2:
                            try
                            {
                                row.Cells[column].SetCellValue("Наименование для Оптик");
                            }
                            catch
                            {
                            }
                            break;
                        case 3:
                            try
                            {
                                row.Cells[column].SetCellValue("SKU");
                            }
                            catch
                            {
                            }
                            break;
                        case 4:
                            try
                            {
                                row.Cells[column].SetCellValue(@"SKUID");
                            }
                            catch
                            {
                            }
                            break;
                        case 5:
                            try
                            {
                                row.Cells[column].SetCellValue("Диоптрия");
                            }
                            catch
                            {
                            }
                            break;
                        case 6:
                            try
                            {
                                row.Cells[column].SetCellValue("Способ учёта");
                            }
                            catch
                            {
                            }
                            break;
                        case 7:
                            try
                            {
                                row.Cells[column].SetCellValue("Количество линз в упаковке");
                            }
                            catch
                            {
                            }
                            break;
                        case 8:
                            try
                            {
                                row.Cells[column].SetCellValue("Активность");
                            }
                            catch
                            {
                            }
                            break;

                    }

                }
                foreach (SKUModel um in SKUList)
                {
                    rownumb++;
                    IRow rowtmp = sheet.CreateRow(rownumb);
                    for (int column = 0; column < 9; column++)
                    {
                        rowtmp.CreateCell(column);
                        switch (column)
                        {
                            case 0:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.ID_Sku);
                                }
                                catch
                                {
                                }
                                break;
                            case 1:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Code);
                                }
                                catch
                                {
                                }
                                break;
                            case 2:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Description);
                                }
                                catch
                                {
                                }
                                break;
                            case 3:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.SkuName);
                                }
                                catch
                                {
                                }
                                break;
                            case 4:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.SKU_ID);
                                }
                                catch
                                {
                                }
                                break;
                            case 5:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Diopter.ToString());
                                }
                                catch
                                {
                                }
                                break;
                            case 6:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Store_Name);
                                }
                                catch
                                {
                                }
                                break;
                            case 7:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.StoreCount.ToString());
                                }
                                catch
                                {
                                }
                                break;
                            case 8:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.IsActive.ToString());
                                }
                                catch
                                {
                                }
                                break;

                        }
                    }
                }
                try
                {
                    xssfwb.Write(file);
                    if (xssfwb != null)
                        xssfwb.Close();
                }
                catch { }
            }
            return result;
        }






        public string ExportDoctors(List<OpticDoctorsFull> SKUList, ref string OutFile)
        {
            XSSFWorkbook xssfwb;
            string result = Path.GetTempPath();
            string filename = "ExportVal" + DateTime.Now.Ticks.ToString() + ".xlsx";
            result += filename;
            OutFile = filename;
            int rownumb = 0;
            //Открываем файл
            using (FileStream file = new FileStream(result, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                xssfwb = new XSSFWorkbook();
                if (xssfwb.NumberOfSheets == 0)
                {
                    xssfwb.CreateSheet();
                }
                ISheet sheet = xssfwb.GetSheetAt(0);
                while (sheet.LastRowNum > 0)
                {
                    sheet.RemoveRow(sheet.GetRow(sheet.LastRowNum));
                }
                IRow row = sheet.CreateRow(0);
                for (int column = 0; column < 5; column++)
                {
                    row.CreateCell(column);
                    switch (column)
                    {
                        case 0:
                            try
                            {
                                row.Cells[column].SetCellValue("ID");
                            }
                            catch
                            {
                            }
                            break;
                        case 1:
                            try
                            {
                                row.Cells[column].SetCellValue("Имя");
                            }
                            catch
                            {
                            }
                            break;
                        case 2:
                            try
                            {
                                row.Cells[column].SetCellValue("Фамилия");
                            }
                            catch
                            {
                            }
                            break;
                        case 3:
                            try
                            {
                                row.Cells[column].SetCellValue("Отчество");
                            }
                            catch
                            {
                            }
                            break;

                        /*case 4:
                            try
                            {
                                row.Cells[column].SetCellValue(@"Оптика");
                            }
                            catch
                            {
                            }
                            break;
                        case 5:
                            try
                            {
                                row.Cells[column].SetCellValue("Адрес оптики");
                            }
                            catch
                            {
                            }
                            break;*/

                        case 4:
                            try
                            {
                                row.Cells[column].SetCellValue("Активность");
                            }
                            catch
                            {
                            }
                            break;
                        

                    }

                }
                foreach (OpticDoctorsFull um in SKUList)
                {
                    rownumb++;
                    IRow rowtmp = sheet.CreateRow(rownumb);
                    for (int column = 0; column < 5; column++)
                    {
                        rowtmp.CreateCell(column);
                        switch (column)
                        {
                            case 0:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.ID_Doc.ToString());
                                }
                                catch
                                {
                                }
                                break;
                            case 1:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.DocName);
                                }
                                catch
                                {
                                }
                                break;
                            case 2:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.DocSurname);
                                }
                                catch
                                {
                                }
                                break;
                            case 3:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.DocPatronymic);
                                }
                                catch
                                {
                                }
                                break;
                            /*case 4:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.OpticName);
                                }
                                catch
                                {
                                }
                                break;
                            case 5:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.OpticAddress);
                                }
                                catch
                                {
                                }
                                break;*/
                            case 4:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.IsActive.ToString());
                                }
                                catch
                                {
                                }
                                break;

                        }
                    }
                }
                try
                {
                    xssfwb.Write(file);
                    if (xssfwb != null)
                        xssfwb.Close();
                }
                catch { }
            }
            return result;
        }







        public string ExportOptics(List<OpticModel> SKUList, ref string OutFile)
        {
            XSSFWorkbook xssfwb;
            string result = Path.GetTempPath();
            string filename = "ExportVal" + DateTime.Now.Ticks.ToString() + ".xlsx";
            result += filename;
            OutFile = filename;
            int rownumb = 0;
            //Открываем файл
            /* */
            using (FileStream file = new FileStream(result, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                xssfwb = new XSSFWorkbook();
                if (xssfwb.NumberOfSheets == 0)
                {
                    xssfwb.CreateSheet();
                }
                ISheet sheet = xssfwb.GetSheetAt(0);
                while (sheet.LastRowNum > 0)
                {
                    sheet.RemoveRow(sheet.GetRow(sheet.LastRowNum));
                }
                IRow row = sheet.CreateRow(0);
                for (int column = 0; column < 27; column++)
                {
                    row.CreateCell(column);
                    switch (column)
                    {
                        case 0:
                            try
                            {
                                row.Cells[column].SetCellValue("ID");
                            }
                            catch
                            {
                            }
                            break;
                        case 1:
                            try
                            {
                                row.Cells[column].SetCellValue("Сеть Оптик");
                            }
                            catch
                            {
                            }
                            break;
                        case 2:
                            try
                            {
                                row.Cells[column].SetCellValue("Торговое название Оптики");
                            }
                            catch
                            {
                            }
                            break;
                        case 3:
                            try
                            {
                                row.Cells[column].SetCellValue("Юридическое название Оптики");
                            }
                            catch
                            {
                            }
                            break;
                        case 4:
                            try
                            {
                                row.Cells[column].SetCellValue("ИНН Оптики");
                            }
                            catch
                            {
                            }
                            break;

                        case 5:
                            try
                            {
                                row.Cells[column].SetCellValue(@"Ответственный ТМ");
                            }
                            catch
                            {
                            }
                            break;
                        case 6:
                            try
                            {
                                row.Cells[column].SetCellValue(@"Электронная почта ответственного ТМ");
                            }
                            catch
                            {
                            }
                            break;
                        case 7:
                            try
                            {
                                row.Cells[column].SetCellValue("Ответственный KAM/KAS");
                            }
                            catch
                            {
                            }
                            break;
                        case 8:
                            try
                            {
                                row.Cells[column].SetCellValue("Электронная почта ответственного КАS/КАМ");
                            }
                            catch
                            {
                            }
                            break;
                        case 9:
                            try
                            {
                                row.Cells[column].SetCellValue("Адрес");
                            }
                            catch
                            {
                            }
                            break;
                        case 10:
                            try
                            {
                                row.Cells[column].SetCellValue("Станция метро");
                            }
                            catch
                            {
                            }
                            break;
                        case 11:
                            try
                            {
                                row.Cells[column].SetCellValue("Телефон");
                            }
                            catch
                            {
                            }
                            break;
                        case 12:
                            try
                            {
                                row.Cells[column].SetCellValue("E-mail ответственного лица в Оптике");
                            }
                            catch
                            {
                            }
                            break;
                        case 13:
                            try
                            {
                                row.Cells[column].SetCellValue("E-mail контактного лица в Оптике (дополнительный)");
                            }
                            catch
                            {
                            }
                            break;                        
                        case 14:
                            try
                            {
                                row.Cells[column].SetCellValue("Отображение на карте оптик (Да/Нет)");
                            }
                            catch
                            {
                            }
                            break;
                        case 15:
                            try
                            {
                                row.Cells[column].SetCellValue("Ссылка на online запись");
                            }
                            catch
                            {
                            }
                            break;
                        case 16:
                            try
                            {
                                row.Cells[column].SetCellValue("Ведется приём детей до 18 лет?");
                            }
                            catch
                            {
                            }
                            break;
                        case 17:
                            try
                            {
                                row.Cells[column].SetCellValue("Digital");
                            }
                            catch
                            {
                            }
                            break;
                        case 18:
                            try
                            {
                                row.Cells[column].SetCellValue("SMS");
                            }
                            catch
                            {
                            }
                            break;
                        case 19:
                            try
                            {
                                row.Cells[column].SetCellValue("Широта");
                            }
                            catch
                            {
                            }
                            break;
                        case 20:
                            try
                            {
                                row.Cells[column].SetCellValue("Долгота");
                            }
                            catch
                            {
                            }
                            break;/*
                        case 21:
                            try
                            {
                                row.Cells[column].SetCellValue("Врач основной");
                            }
                            catch
                            {
                            }
                            break;
                        case 22:
                            try
                            {
                                row.Cells[column].SetCellValue("Врач дополнительный");
                            }
                            catch
                            {
                            }
                            break;
                        case 23:
                            try
                            {
                                row.Cells[column].SetCellValue("Врач дополнительный");
                            }
                            catch
                            {
                            }
                            break;*/
                        case 21:
                            try
                            {
                                row.Cells[column].SetCellValue("Основной e-mail оптики для логина");
                            }
                            catch
                            {
                            }
                            break;
                        case 22:
                            try
                            {
                                row.Cells[column].SetCellValue("Адрес по ФИАС");
                            }
                            catch
                            {
                            }
                            break;
                        case 23:
                            try
                            {
                                row.Cells[column].SetCellValue("Код клиента");
                            }
                            catch
                            {
                            }
                            break;
                        case 24:
                            try
                            {
                                row.Cells[column].SetCellValue("Брик VC");
                            }
                            catch
                            {
                            }
                            break;
                        case 25:
                            try
                            {
                                row.Cells[column].SetCellValue("Адрес доставки");
                            }
                            catch
                            {
                            }
                            break;
                        case 26:
                            try
                            {
                                row.Cells[column].SetCellValue("Код доступа");
                            }
                            catch
                            {
                            }
                            break;
                    }

                }
                foreach (OpticModel um in SKUList)
                {
                    rownumb++;
                    IRow rowtmp = sheet.CreateRow(rownumb);
                    for (int column = 0; column < 27; column++)
                    {
                        rowtmp.CreateCell(column);
                        switch (column)
                        {
                            case 0:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.ID_optic);
                                }
                                catch
                                {
                                }
                                break;
                            case 1:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.NetworkName);
                                }
                                catch
                                {
                                }
                                break;
                            case 2:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.OpticTorgName);
                                }
                                catch
                                {
                                }
                                break;
                            case 3:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.OpticJuraName);
                                }
                                catch
                                {
                                }
                                break;
                            case 4:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.OpticINN);
                                }
                                catch
                                {
                                }
                                break;
                            case 5:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.UserName);
                                }
                                catch
                                {
                                }
                                break;
                            case 6:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.TMEmail);
                                }
                                catch
                                {
                                }
                                break;
                            case 7:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.KASName);
                                }
                                catch
                                {
                                }
                                break;
                            case 8:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.KASEmail);
                                }
                                catch
                                {
                                }
                                break;
                            case 9:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Address);
                                }
                                catch
                                {
                                }
                                break;
                            case 10:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Metro);
                                }
                                catch
                                {
                                }
                                break;
                            case 11:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Phone);
                                }
                                catch
                                {
                                }
                                break;
                            case 12:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Email1);
                                }
                                catch
                                {
                                }
                                break;
                            case 13:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Email2);
                                }
                                catch
                                {
                                }
                                break;
                            case 14:
                                try
                                {
                                    if (um.ShowOnCard)
                                        rowtmp.Cells[column].SetCellValue("Да");
                                    else
                                        rowtmp.Cells[column].SetCellValue("Нет");
                                }
                                catch
                                {
                                }
                                break;
                            case 15:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.OnlineLink);
                                }
                                catch
                                {
                                }
                                break;
                            case 16:
                                try
                                {
                                    if (um.Kids)
                                        rowtmp.Cells[column].SetCellValue("Да");
                                    else
                                        rowtmp.Cells[column].SetCellValue("Нет");
                                  //  rowtmp.Cells[column].SetCellValue(um.Kids);
                                }
                                catch
                                {
                                }
                                break;
                            case 17:
                                try
                                {
                                    if (um.Digital)
                                        rowtmp.Cells[column].SetCellValue("Да");
                                    else
                                        rowtmp.Cells[column].SetCellValue("Нет");
                                    //rowtmp.Cells[column].SetCellValue(um.Digital);
                                }
                                catch
                                {
                                }
                                break;
                            case 18:
                                try
                                {
                                    if (um.Sms)
                                        rowtmp.Cells[column].SetCellValue("Да");
                                    else
                                        rowtmp.Cells[column].SetCellValue("Нет");
                                    //rowtmp.Cells[column].SetCellValue(um.Sms);
                                }
                                catch
                                {
                                }
                                break;
                            case 19:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Latitude);
                                }
                                catch
                                {
                                }
                                break;
                            case 20:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Longitude);
                                }
                                catch
                                {
                                }
                                break;
                            /*case 21:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Doc1);
                                }
                                catch
                                {
                                }
                                break;
                            case 22:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Doc2);
                                }
                                catch
                                {
                                }
                                break;
                            case 23:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Doc3);
                                }
                                catch
                                {
                                }
                                break;*/
                            case 21:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Email);
                                }
                                catch
                                {
                                }
                                break;
                            case 22:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Address);
                                }
                                catch
                                {
                                }
                                break;
                            case 23:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.ClientCode);
                                }
                                catch
                                {
                                }
                                break;
                            case 24:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.BrickVC);
                                }
                                catch
                                {
                                }
                                break;
                            case 25:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.DeliverAddress);
                                }
                                catch
                                {
                                }
                                break;
                            case 26:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.AccessCode);
                                }
                                catch
                                {
                                }
                                break;

                        }
                    }
                }
                try
                {
                    xssfwb.Write(file);
                    if (xssfwb != null)
                        xssfwb.Close();
                }
                catch { }
            }
            return result;
        }


        ///////////////////***********************************************
        public string ExportSelections(List<MatchingReportModel> DiagnosticsOrderList, ref string OutFile)
        {
            XSSFWorkbook xssfwb;
            string result = Path.GetTempPath();
            string filename = "ExportVal" + DateTime.Now.Ticks.ToString() + ".xls";
            result += filename;
            OutFile = filename;
            int rownumb = 0;
            int maxrow = 0;
            foreach(MatchingReportModel b in DiagnosticsOrderList)
            {
                if (b.WriteOffSKU.Count > 0 && maxrow < b.WriteOffSKU.Count)
                    maxrow = b.WriteOffSKU.Count;
            }
            //Открываем файл
            using (FileStream file = new FileStream(result, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                xssfwb = new XSSFWorkbook();
                if (xssfwb.NumberOfSheets == 0)
                {
                    xssfwb.CreateSheet();
                }
                ISheet sheet = xssfwb.GetSheetAt(0);
                while (sheet.LastRowNum > 0)
                {
                    sheet.RemoveRow(sheet.GetRow(sheet.LastRowNum));
                }
                IRow row = sheet.CreateRow(0);
                for (int column = 0; column < 16+maxrow; column++)
                {
                    row.CreateCell(column);
                    switch (column)
                    {

                        case 0:
                            try
                            {
                                row.Cells[column].SetCellValue("KAS/KAM");
                            }
                            catch
                            {                            }
                            break;
                        case 1:
                            try
                            {
                                row.Cells[column].SetCellValue("Код (Номер сертификата или SMS)");
                            }
                            catch
                            {                            }
                            break;
                        case 2:
                            try
                            {
                                row.Cells[column].SetCellValue("Сеть Оптик");
                            }
                            catch
                            {                            }
                            break;
                        case 3:
                            try
                            {
                                row.Cells[column].SetCellValue("Юридическое название Оптики");
                            }
                            catch
                            {                            }
                            break;
                        case 4:
                            try
                            {
                                row.Cells[column].SetCellValue("Субъект РФ");
                            }
                            catch
                            {                            }
                            break;
                        case 5:
                            try
                            {
                                row.Cells[column].SetCellValue("Брик VC");
                            }
                            catch                            {
                            }
                            break;
                        case 6:
                            try
                            {
                                row.Cells[column].SetCellValue("Город (Оптика)");
                            }
                            catch
                            {                            }
                            break;
                        case 7:
                            try
                            {
                                row.Cells[column].SetCellValue("Адрес Оптики");
                            }
                            catch
                            {                            }
                            break;
                        case 8:
                            try
                            {
                                row.Cells[column].SetCellValue("Номер телефона клиента (пациента)");
                            }
                            catch
                            {                            }
                            break;
                       
                        case 9:
                            try
                            {
                                row.Cells[column].SetCellValue("Номенклатура");
                            }
                            catch
                            {                            }
                            break;
                        case 10:
                            try
                            {
                                row.Cells[column].SetCellValue("Расход, pk");
                            }
                            catch
                            {                            }
                            break;
                        case 11:
                            try
                            {
                                row.Cells[column].SetCellValue("Причина расхода");
                            }
                            catch
                            {                            }
                            break;
                        case 12:
                            try
                            {
                                row.Cells[column].SetCellValue("Дата окончания подбора");
                            }
                            catch
                            {                            }
                            break;
                        case 13:
                            try
                            {
                                row.Cells[column].SetCellValue("Месяц окончания подбора");
                            }
                            catch
                            {                            }
                            break;
                        case 14:
                            try
                            {
                                row.Cells[column].SetCellValue("Сертификат или SMS (перечислимый тип)");
                            }
                            catch
                            {                            }
                            break;
                        
                        default:
                            if (column == (14 + maxrow + 1))
                            {
                                try
                                {
                                    row.Cells[column].SetCellValue("ФИО врача");
                                }
                                catch
                                {                                }                                
                            }
                            else
                                try
                                {
                                    row.Cells[column].SetCellValue("SKU");
                                }
                                catch
                                {                                }
                            break;
                        
                    }
                    
                }
                ICellStyle _doubleCellStyle = null;
                _doubleCellStyle = xssfwb.CreateCellStyle();
                _doubleCellStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("0.00");
                foreach (MatchingReportModel um in DiagnosticsOrderList)
                {
                    rownumb++;
                    IRow rowtmp = sheet.CreateRow(rownumb);
                    for (int column = 0; column < (16+maxrow); column++)
                    {
                        rowtmp.CreateCell(column);
                        switch (column)
                        {
                            /*KAS/KAM
Код (Номер сертификата или SMS)
Сеть Оптик 
*/
                            case 0:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.KamKas);
                                }
                                catch
                                {                                }
                                break;
                            case 1:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Code);
                                }
                                catch
                                {                                }
                                break;
                            case 2:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.NetworkName);
                                }
                                catch
                                {                                }
                                break;
                            /*Юридическое название Оптики
Субъект РФ
Брик VC
Город (Оптика)
Адрес Оптики
*/
                            case 3:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.OpticJuraName);
                                }
                                catch
                                {                                }
                                break;
                            case 4:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.RegionName);
                                }
                                catch
                                {                                }
                                break;
                            case 5:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Brick);
                                }
                                catch
                                {                                }
                                break;
                            case 6:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Town);
                                }
                                catch
                                {                                }
                                break;
                            case 7:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.OpticAddress);
                                }
                                catch
                                {                                }
                                break;
                            /*Номер телефона клиента (пациента)
                            Номенклатура
                            Расход, pk
                            Причина расхода
                            Дата окончания подбора
                            Месяц окончания подбора
                            Сертификат или SMS (перечислимый тип)
                            SKU
                            ФИО врача
                            */

                            case 8:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.PhoneNumber);
                                }
                                catch
                                {                                }
                                break;
                            case 9:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Nomenklatur);
                                }
                                catch
                                {                                }
                                break;
                            case 10:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Quantity);
                                }
                                catch
                                {                                }
                                break;
                            case 11:
                                try
                                {
                                    if (um.Reason == null || um.Reason.Length == 0)
                                        rowtmp.Cells[column].SetCellValue("Подбор");
                                    else
                                        rowtmp.Cells[column].SetCellValue(um.Reason);
                                    //rowtmp.Cells[column].SetCellValue(um.Reason);
                                }
                                catch
                                {                                }
                                break;
                            case 12:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.EndDate);
                                }
                                catch
                                {                                }
                                break;
                            case 13:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.MonthName);
                                }
                                catch
                                {                                }
                                break;
                            case 14:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.CertSMS);
                                    
                                }
                                catch
                                {                                }
                                break;
                            default:
                                if (column == (14 + maxrow + 1))
                                {
                                    try
                                    {
                                        rowtmp.Cells[column].SetCellValue(um.DocName);
                                    }
                                    catch
                                    { }
                                }
                                else
                                    try
                                    {
                                        if(um.WriteOffSKU.Count >= column-14 )
                                            rowtmp.Cells[column].SetCellValue(um.WriteOffSKU[column-15].SKUName);
                                    }
                                    catch
                                    { }
                                break;

                        }
                    }
                }
                sheet.AutoSizeColumn(0, true);
                sheet.AutoSizeColumn(1, true);
                // sheet.SetColumnWidth()
                try
                {
                    xssfwb.Write(file);
                    if (xssfwb != null)
                        xssfwb.Close();
                }
                catch { }
            }
            return result;
        }



        //////////////--------------------------
        public string ExportSelectionsShort(List<MatchingReportModel> DiagnosticsOrderList, ref string OutFile)
        {
            XSSFWorkbook xssfwb;
            string result = Path.GetTempPath();
            string filename = "ExportVal" + DateTime.Now.Ticks.ToString() + ".xls";

                       
            result += filename;
            OutFile = filename;
            int rownumb = 0;
            int maxrow = 0;
            foreach (MatchingReportModel b in DiagnosticsOrderList)
            {
                if (b.WriteOffSKU.Count > 0 && maxrow < b.WriteOffSKU.Count)
                    maxrow = b.WriteOffSKU.Count;
            }
            //Открываем файл
            using (FileStream file = new FileStream(result, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                xssfwb = new XSSFWorkbook();
                if (xssfwb.NumberOfSheets == 0)
                {
                    xssfwb.CreateSheet();
                }
                ISheet sheet = xssfwb.GetSheetAt(0);
                ICellStyle _dateCellStyle = null;
                _dateCellStyle = xssfwb.CreateCellStyle();
                _dateCellStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("yyyy.mm.dd");
                while (sheet.LastRowNum > 0)
                {
                    sheet.RemoveRow(sheet.GetRow(sheet.LastRowNum));
                }
                IRow row = sheet.CreateRow(0);
                for (int column = 0; column < 12 + maxrow; column++)
                {
                    row.CreateCell(column);
                    switch (column)
                    {

                       
                        case 0:
                            try
                            {
                                row.Cells[column].SetCellValue("Код (Номер сертификата или SMS)");
                            }
                            catch
                            { }
                            break;
                        
                        case 1:
                            try
                            {
                                row.Cells[column].SetCellValue("Юридическое название Оптики");
                            }
                            catch
                            { }
                            break;
                        case 2:
                            try
                            {
                                row.Cells[column].SetCellValue("Субъект РФ");
                            }
                            catch
                            { }
                            break;                        
                        case 3:
                            try
                            {
                                row.Cells[column].SetCellValue("Город (Оптика)");
                            }
                            catch
                            { }
                            break;
                        case 4:
                            try
                            {
                                row.Cells[column].SetCellValue("Адрес Оптики");
                            }
                            catch
                            { }
                            break;
                        

                        case 5:
                            try
                            {
                                row.Cells[column].SetCellValue("Номенклатура");
                            }
                            catch
                            { }
                            break;
                        case 6:
                            try
                            {
                                row.Cells[column].SetCellValue("Расход, pk");
                            }
                            catch
                            { }
                            break;
                        case 7:
                            try
                            {
                                row.Cells[column].SetCellValue("Причина расхода");
                            }
                            catch
                            { }
                            break;
                        case 8:
                            try
                            {
                                row.Cells[column].SetCellValue("Дата окончания подбора");
                            }
                            catch
                            { }
                            break;
                        case 9:
                            try
                            {
                                row.Cells[column].SetCellValue("Месяц окончания подбора");
                            }
                            catch
                            { }
                            break;
                        case 10:
                            try
                            {
                                row.Cells[column].SetCellValue("Сертификат или SMS (перечислимый тип)");
                            }
                            catch
                            { }
                            break;

                        default:
                            if (column == (10 + maxrow + 1))
                            {
                                try
                                {
                                    row.Cells[column].SetCellValue("ФИО врача");
                                }
                                catch
                                { }
                            }
                            else
                                try
                                {
                                    row.Cells[column].SetCellValue("SKU");
                                }
                                catch
                                { }
                            break;

                    }

                }
                ICellStyle _doubleCellStyle = null;
                _doubleCellStyle = xssfwb.CreateCellStyle();
                _doubleCellStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("0.00");
                foreach (MatchingReportModel um in DiagnosticsOrderList)
                {
                    rownumb++;
                    IRow rowtmp = sheet.CreateRow(rownumb);
                    for (int column = 0; column < (12 + maxrow); column++)
                    {
                        rowtmp.CreateCell(column);
                        switch (column)
                        {
                            /*KAS/KAM
Код (Номер сертификата или SMS)
Сеть Оптик 
*/                           
                            case 0:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Code);
                                }
                                catch
                                { }
                                break;
                            
                            case 1:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.OpticJuraName);
                                }
                                catch
                                { }
                                break;
                            case 2:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.RegionName);
                                }
                                catch
                                { }
                                break;                            
                            case 3:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Town);
                                }
                                catch
                                { }
                                break;
                            case 4:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.OpticAddress);
                                }
                                catch
                                { }
                                break;   
                            case 5:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Nomenklatur);
                                }
                                catch
                                { }
                                break;
                            case 6:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.Quantity);
                                }
                                catch
                                { }
                                break;
                            case 7:
                                try
                                {
                                    if (um.Reason == null || um.Reason.Length == 0)
                                        rowtmp.Cells[column].SetCellValue("Подбор");
                                    else
                                        rowtmp.Cells[column].SetCellValue(um.Reason);
                                    //rowtmp.Cells[column].SetCellValue(um.Reason);
                                }
                                catch
                                { }
                                break;
                            case 8:
                                try
                                {
                                    //rowtmp.Cells[column].CellStyle.DataFormat.
                                    rowtmp.Cells[column].SetCellValue(um.EndDate);
                                    rowtmp.Cells[column].SetCellType(CellType.String);
                                    //rowtmp.Cells[column].CellStyle = _dateCellStyle;
                                }
                                catch
                                { }
                                break;
                            case 9:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.MonthName);
                                }
                                catch
                                { }
                                break;
                            case 10:
                                try
                                {
                                    rowtmp.Cells[column].SetCellValue(um.CertSMS);

                                }
                                catch
                                { }
                                break;
                            default:
                                if (column == (10 + maxrow + 1))
                                {
                                    try
                                    {
                                        rowtmp.Cells[column].SetCellValue(um.DocName);
                                    }
                                    catch
                                    { }
                                }
                                else
                                    try
                                    {
                                        if (um.WriteOffSKU.Count >= column - 10)
                                            rowtmp.Cells[column].SetCellValue(um.WriteOffSKU[column - 11].SKUName);
                                    }
                                    catch
                                    { }
                                break;

                        }
                    }
                }
                sheet.AutoSizeColumn(0, true);
                sheet.AutoSizeColumn(1, true);
                // sheet.SetColumnWidth()
                try
                {
                    xssfwb.Write(file);
                    if (xssfwb != null)
                        xssfwb.Close();
                }
                catch { }
            }
            return result;
        }









    }
}
